package com.getjavajob.training.matiivv.socialnetwork;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("development")
public class SocialNetworkWebApplicationTest {

    @SuppressWarnings("EmptyMethod")
    @Test
    public void contextLoads() {
    }

}
