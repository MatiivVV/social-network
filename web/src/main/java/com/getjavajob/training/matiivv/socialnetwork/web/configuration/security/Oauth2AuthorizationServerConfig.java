package com.getjavajob.training.matiivv.socialnetwork.web.configuration.security;

import com.getjavajob.training.matiivv.socialnetwork.service.security.account.AccountUserDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsByNameServiceWrapper;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;

import java.util.List;

@Slf4j
@Configuration
@EnableAuthorizationServer
public class Oauth2AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    private Oauth2Properties.Client clientProperties;
    private Oauth2Properties.Resource resourceProperties;
    private AuthenticationManager authenticationManager;
    private UserDetailsService userDetailsService;
    private PasswordEncoder passwordEncoder;
    private UserAuthenticationConverter userAuthenticationConverter;
    private ClientDetailsService clientDetailsService;

    @Autowired
    public void setSecurityConfig(WebSecurityConfig webSecurityConfig) throws Exception {
        authenticationManager = webSecurityConfig.authenticationManagerBean();
        userDetailsService = webSecurityConfig.userDetailsServiceBean();
        passwordEncoder = webSecurityConfig.passwordEncoder();
    }

    @Autowired
    public void setProperties(Oauth2Properties properties) {
        clientProperties = properties.getClient();
        resourceProperties = properties.getResource();
    }

    @Autowired
    public void setUserAuthenticationConverter(UserAuthenticationConverter userAuthenticationConverter) {
        this.userAuthenticationConverter = userAuthenticationConverter;
    }

    @Autowired
    public void setClientDetailsService(ClientDetailsService clientDetailsService) {
        this.clientDetailsService = clientDetailsService;
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        var converter = new JwtAccessTokenConverter();
        converter.setSigningKey(resourceProperties.getJwt().getKeyValue());
        var accessTokenConverter = (DefaultAccessTokenConverter) converter.getAccessTokenConverter();
        accessTokenConverter.setUserTokenConverter(userAuthenticationConverter);
        return converter;
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());
    }

    @Bean
    public DefaultTokenServices tokenServices() {
        var tokenServices = new DefaultTokenServices();
        tokenServices.setTokenStore(tokenStore());
        tokenServices.setSupportRefreshToken(true);
        tokenServices.setClientDetailsService(clientDetailsService);
        var provider = new PreAuthenticatedAuthenticationProvider() {
            @Override
            public Authentication authenticate(Authentication authentication) {
                Authentication result = super.authenticate(authentication);
                var tokenUser = (AccountUserDetails) ((Authentication) authentication.getPrincipal()).getPrincipal();
                var user = (AccountUserDetails) result.getPrincipal();
                if (!user.getSession().equals(tokenUser.getSession())) {
                    log.info("User {} session {} is already closed", user.getId(), tokenUser.getSession());
                    throw new SessionAuthenticationException("Session is already closed");
                }
                return result;
            }
        };
        provider.setPreAuthenticatedUserDetailsService(new UserDetailsByNameServiceWrapper<>(userDetailsService));
        tokenServices.setAuthenticationManager(new ProviderManager(List.of(provider)));
        tokenServices.setTokenEnhancer(accessTokenConverter());
        return tokenServices;
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints.authenticationManager(authenticationManager)
                .userDetailsService(userDetailsService)
                .tokenServices(tokenServices())
                .accessTokenConverter(accessTokenConverter())
                .tokenStore(tokenStore());
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer configurer) throws Exception {
        configurer.inMemory()
                .withClient(clientProperties.getClientId())
                .secret(passwordEncoder.encode(clientProperties.getClientSecret()))
                .authorizedGrantTypes(clientProperties.getAuthorizedGrantTypes())
                .scopes(clientProperties.getScope())
                .accessTokenValiditySeconds(clientProperties.getAccessTokenValiditySeconds())
                .refreshTokenValiditySeconds(clientProperties.getRefreshTokenValiditySeconds());
    }

}
