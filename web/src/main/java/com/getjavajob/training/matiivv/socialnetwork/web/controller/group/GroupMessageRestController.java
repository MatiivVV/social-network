package com.getjavajob.training.matiivv.socialnetwork.web.controller.group;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.GroupMessage;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.MessageSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.group.GroupMessageService;
import com.getjavajob.training.matiivv.socialnetwork.web.controller.MultipartFileResourceAdapter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;

import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.fromController;

@Slf4j
@RestController
@RequestMapping(path = "/groups/{groupId}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
public class GroupMessageRestController {

    private GroupMessageService groupMessageService;

    public GroupMessageService getGroupMessageService() {
        return groupMessageService;
    }

    @Autowired
    public void setGroupMessageService(GroupMessageService groupMessageService) {
        this.groupMessageService = groupMessageService;
    }

    @GetMapping("/authors/{groupId}/messages/{messageId}")
    public GroupMessage getGroupMessage(@PathVariable long groupId, @PathVariable long authorId, @PathVariable long messageId) {
        log.debug("Going to get group message {} for group {} from author {}", messageId, groupId, authorId);
        return groupMessageService.getGroupMessage(groupId, authorId, messageId);
    }

    @GetMapping("/messages")
    public Page<MessageSummary> getGroupMessages(@PathVariable long groupId, Pageable pageRequest) {
        log.debug("Going to get group messages for group {} / {}", groupId, pageRequest);
        return groupMessageService.getGroupMessages(groupId, pageRequest);
    }

    @PostMapping(path = "/authors/{authorId}/messages", consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<MessageSummary> sendGroupMessage(@PathVariable long groupId, @PathVariable long authorId,
                                                           @RequestPart GroupMessage message,
                                                           @RequestPart(required = false) MultipartFile image) {
        log.debug("Going to send group message for group {} from author {} {}", groupId, authorId, message);
        GroupMessage result = groupMessageService.sendGroupMessage(groupId, authorId, message,
                image == null ? null : new MultipartFileResourceAdapter(image));
        log.debug("Group message for group {} from author {} has been successfully sent {}", groupId, authorId, result);
        URI uri = fromController(getClass())
                .path("/authors/{authorId}/messages/{messageId}")
                .buildAndExpand(groupId, authorId, result.getId())
                .toUri();
        return created(uri).body(new MessageSummary(
                result.getId(),
                result.getAuthor().getId(),
                result.getAuthor().getFirstName(),
                result.getAuthor().getLastName(),
                result.getAuthor().getImageId(),
                result.getAuthor().isDeleted(),
                result.getMessage(),
                result.getImageId(),
                result.getPostedAt()
        ));
    }

    @DeleteMapping("/authors/{authorId}/messages/{messageId}")
    @ResponseStatus(NO_CONTENT)
    public void deleteGroupMessage(@PathVariable long groupId, @PathVariable long authorId, @PathVariable long messageId) {
        log.debug("Going to delete group message {} for group {} from author {}", messageId, groupId, authorId);
        groupMessageService.deleteGroupMessage(groupId, authorId, messageId);
        log.debug("Group message {} for group {} from author {} has been successfully deleted", messageId, groupId, authorId);
    }

}
