package com.getjavajob.training.matiivv.socialnetwork.web.controller.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Chat;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.ChatSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.account.ChatService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.fromController;

@Slf4j
@RestController
@RequestMapping(path = "/accounts/{accountId}/opponents", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
public class ChatRestController {

    private ChatService chatService;

    public ChatService getChatService() {
        return chatService;
    }

    @Autowired
    public void setChatService(ChatService chatService) {
        this.chatService = chatService;
    }

    @GetMapping("/{opponentId}")
    public Chat getChat(@PathVariable long accountId, @PathVariable long opponentId) {
        log.debug("Going to get account {} chat {}", accountId, opponentId);
        return chatService.getChat(accountId, opponentId);
    }

    @GetMapping
    public Page<ChatSummary> getChats(@PathVariable long accountId, Pageable pageRequest) {
        log.debug("Going to get account {} chats / {}", accountId, pageRequest);
        return chatService.getChats(accountId, pageRequest);
    }

    @PostMapping("/{opponentId}")
    public ResponseEntity<Chat> createChat(@PathVariable long accountId, @PathVariable long opponentId) {
        log.debug("Going to create account {} chat {}", accountId, opponentId);
        Chat result = chatService.createChat(accountId, opponentId);
        log.debug("Account {} chat {} has been successfully created {}", accountId, opponentId, result);
        URI uri = fromController(getClass())
                .path("/{opponentId}")
                .buildAndExpand(accountId, opponentId)
                .toUri();
        return created(uri).body(result);
    }

    @PatchMapping(path = "/{opponentId}", params = "part=read")
    public ResponseEntity<?> setChatRead(@PathVariable long accountId, @PathVariable long opponentId) {
        log.debug("Going to set account {} chat {} status as read", accountId, opponentId);
        long result = chatService.setChatRead(accountId, opponentId);
        log.debug("Account {} chat {} read status has been successfully set {}", accountId, opponentId, result);
        return noContent().header("Read-Message-Count", Long.toString(result)).build();
    }

    @DeleteMapping("/{opponentId}")
    @ResponseStatus(NO_CONTENT)
    public void deleteChat(@PathVariable long accountId, @PathVariable long opponentId) {
        log.debug("Going to delete account {} chat {}", accountId, opponentId);
        chatService.deleteChat(accountId, opponentId);
        log.debug("Account {} chat {} has been successfully deleted", accountId, opponentId);
    }

}
