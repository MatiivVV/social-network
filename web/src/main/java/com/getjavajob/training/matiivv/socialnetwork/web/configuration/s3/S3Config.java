package com.getjavajob.training.matiivv.socialnetwork.web.configuration.s3;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.amazonaws.regions.Regions.fromName;

@Configuration
public class S3Config {

    private S3Properties properties;

    @Autowired
    public void setProperties(S3Properties properties) {
        this.properties = properties;
    }

    @Bean
    public AmazonS3 s3Client() {
        var credentials = new BasicAWSCredentials(properties.getCredentials().getAccessKey(),
                properties.getCredentials().getSecretKey());
        return AmazonS3ClientBuilder.standard()
                .withRegion(fromName(properties.getRegion()))
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .build();
    }

}
