package com.getjavajob.training.matiivv.socialnetwork.web.controller;

import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.Resource;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

import static org.springframework.http.MediaType.*;

@Slf4j
@RestController
@RequestMapping(value = "/images", produces = {IMAGE_JPEG_VALUE, IMAGE_GIF_VALUE, IMAGE_PNG_VALUE})
public class ImageRestController {

    private ResourceService resourceService;

    public ResourceService getResourceService() {
        return resourceService;
    }

    @Autowired
    public void setResourceService(ResourceService resourceService) {
        this.resourceService = resourceService;
    }

    @GetMapping("/{id}")
    public void getImage(@PathVariable String id, HttpServletResponse response) throws Exception {
        log.debug("Going to get image {}", id);
        try (Resource resource = resourceService.getResource(id)) {
            response.setContentType(resource.getContentType());
            response.setContentLengthLong(resource.getContentLength());
            resource.getInputStream().transferTo(response.getOutputStream());
        }
    }

}
