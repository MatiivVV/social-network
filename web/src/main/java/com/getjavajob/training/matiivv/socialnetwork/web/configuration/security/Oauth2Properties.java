package com.getjavajob.training.matiivv.socialnetwork.web.configuration.security;

import com.getjavajob.training.matiivv.socialnetwork.domain.stereotype.ConfigurationComponent;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationComponent
@ConfigurationProperties("security.oauth2")
public class Oauth2Properties {

    private Client client = new Client();
    private Resource resource = new Resource();

    @Data
    public static class Client {

        private String clientId;
        private String clientSecret;
        private String[] authorizedGrantTypes;
        private String[] scope;
        private int accessTokenValiditySeconds;
        private int refreshTokenValiditySeconds;

    }

    @Data
    public static class Resource {

        private String id;
        private Jwt jwt = new Jwt();

        @Data
        public static class Jwt {

            private String keyValue;

        }

    }

}
