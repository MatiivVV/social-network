package com.getjavajob.training.matiivv.socialnetwork.web.configuration;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.getjavajob.training.matiivv.socialnetwork.service.validation.AutowireConstraintValidatorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.validation.ConstraintValidatorFactory;

import static com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module.Feature.USE_TRANSIENT_ANNOTATION;
import static org.springframework.web.method.HandlerTypePredicate.forAnnotation;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    private MessageSource messageSource;

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Bean
    public Module hibernate5Module() {
        var hibernate5Module = new Hibernate5Module();
        hibernate5Module.disable(USE_TRANSIENT_ANNOTATION);
        return hibernate5Module;
    }

    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        configurer.addPathPrefix("/api", forAnnotation(RestController.class));
    }

    @SuppressWarnings("SpringMVCViewInspection")
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/")
                .setViewName("forward:index.html");
        registry.addViewController("/{route:^(?!api|oauth|notifications|image|static)\\w+$}/**")
                .setViewName("forward:/index.html");
    }

    @Bean
    public ConstraintValidatorFactory constraintValidatorFactory() {
        return new AutowireConstraintValidatorFactory();
    }

    @Bean
    @Override
    public LocalValidatorFactoryBean getValidator() {
        var validator = new LocalValidatorFactoryBean();
        validator.setValidationMessageSource(messageSource);
        validator.setConstraintValidatorFactory(constraintValidatorFactory());
        return validator;
    }

}
