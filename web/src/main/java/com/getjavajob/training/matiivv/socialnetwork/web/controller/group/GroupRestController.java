package com.getjavajob.training.matiivv.socialnetwork.web.controller.group;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Group;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.group.GroupSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.group.GroupService;
import com.getjavajob.training.matiivv.socialnetwork.web.controller.MultipartFileResourceAdapter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;

import static java.net.URI.create;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.fromController;

@Slf4j
@RestController
@RequestMapping(path = "/groups", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
public class GroupRestController {

    private GroupService groupService;

    public GroupService getGroupService() {
        return groupService;
    }

    @Autowired
    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    @GetMapping("/{id}")
    public Group getGroup(@PathVariable long id) {
        log.debug("Going to get group {}", id);
        return groupService.getGroup(id);
    }

    @GetMapping
    public Page<GroupSummary> getGroups(@RequestParam String pattern, Pageable pageRequest) {
        log.debug("Going to get groups for pattern {} / {}", pattern, pageRequest);
        return groupService.getGroups(pattern, pageRequest);
    }

    @PostMapping(consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Group> createGroup(@RequestPart Group group,
                                             @RequestPart(required = false) MultipartFile groupImage) {
        log.debug("Going to register group {}", group);
        Group result = groupService.createGroup(group,
                groupImage == null ? null : new MultipartFileResourceAdapter(groupImage));
        log.debug("Group has been successfully created {}", result);
        URI uri = fromController(getClass())
                .path("/{id}")
                .buildAndExpand(result.getId())
                .toUri();
        return created(uri).body(result);
    }

    @PatchMapping(value = "/{id}", params = "part=info")
    @ResponseStatus(NO_CONTENT)
    public void editGroupInfo(@PathVariable long id, @RequestBody Group groupInfo) {
        groupInfo.setId(id);
        log.debug("Going to edit group info {}", groupInfo);
        Group result = groupService.editGroupInfo(id, groupInfo);
        log.debug("Group info has been successfully edited {}", result);
    }

    @PatchMapping(value = "/{id}", params = "part=image",
            consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> editGroupImage(@PathVariable long id, @RequestPart(required = false) MultipartFile groupImage) {
        log.debug("Going to edit group {} image", id);
        Group result = groupService.editGroupImage(id,
                groupImage == null ? null : new MultipartFileResourceAdapter(groupImage));
        log.debug("Group {} image has been successfully edited", id);
        if (result.getImageId() == null) {
            return noContent().build();
        } else {
            return noContent().location(create(result.getImageId())).build();
        }
    }

    @PatchMapping(value = "/{id}", params = "part=deleted")
    @ResponseStatus(NO_CONTENT)
    public void editGroupDeleted(@PathVariable long id, @RequestBody Group groupDeleted) {
        groupDeleted.setId(id);
        log.debug("Going to edit group deleted flag {}", groupDeleted);
        Group result = groupService.editGroupDeleted(id, groupDeleted);
        log.debug("Group deleted flag has been successfully edited {}", result);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public void deleteGroup(@PathVariable long id) {
        log.debug("Going to delete group {}", id);
        var groupDeleted = new Group(id);
        groupDeleted.setDeleted(true);
        groupService.editGroupDeleted(id, groupDeleted);
        log.debug("Group {} has been successfully deleted", id);
    }

}
