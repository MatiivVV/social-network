package com.getjavajob.training.matiivv.socialnetwork.web.configuration.s3;

import com.getjavajob.training.matiivv.socialnetwork.domain.stereotype.ConfigurationComponent;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationComponent
@ConfigurationProperties("spring.s3")
public class S3Properties {

    private String region;
    private String bucket;
    private Credentials credentials = new Credentials();

    @Data
    public static class Credentials {

        private String accessKey;
        private String secretKey;

    }

}
