package com.getjavajob.training.matiivv.socialnetwork.web.controller;

import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

public class MultipartFileResourceAdapter implements Resource {

    private final MultipartFile file;

    public MultipartFileResourceAdapter(MultipartFile file) {
        this.file = file;
    }

    @Override
    public String getId() {
        return file.getOriginalFilename();
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return file.getInputStream();
    }

    @Override
    public String getContentType() {
        return file.getContentType();
    }

    @Override
    public long getContentLength() {
        return file.getSize();
    }

    @Override
    public void close() {
    }

}
