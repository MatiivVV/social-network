package com.getjavajob.training.matiivv.socialnetwork.web.controller.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Friendship;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Friendship.Id;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.FriendshipSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.account.FriendshipService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Set;

import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.fromController;

@Slf4j
@RestController
@RequestMapping(path = "/accounts/{accountId}/friendships", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
public class FriendshipRestController {

    private FriendshipService friendshipService;

    public FriendshipService getFriendshipService() {
        return friendshipService;
    }

    @Autowired
    public void setFriendshipService(FriendshipService friendshipService) {
        this.friendshipService = friendshipService;
    }

    @GetMapping("/{friendId}")
    public Friendship getFriendship(@PathVariable long accountId, @PathVariable long friendId) {
        log.debug("Going to get account {} friendship {}", accountId, friendId);
        return friendshipService.getFriendship(accountId, friendId);
    }

    @GetMapping
    public Page<FriendshipSummary> getFriendships(@PathVariable long accountId,
                                                  @RequestParam(required = false) Set<Friendship.Type> types,
                                                  @RequestParam(required = false) Set<Status> statuses,
                                                  Pageable pageRequest) {
        log.debug("Going to get account {} friendships for types {} and statuses {} / {}", accountId, types, statuses, pageRequest);
        return friendshipService.getFriendships(accountId, types, statuses, pageRequest);
    }

    @GetMapping(params = "friends")
    public Page<FriendshipSummary> getFriends(@PathVariable long accountId, Pageable pageRequest) {
        log.debug("Going to get account {} friends / {}", accountId, pageRequest);
        return friendshipService.getFriends(accountId, pageRequest);
    }

    @PostMapping("/{friendId}")
    public ResponseEntity<Friendship> createFriendship(@PathVariable long accountId, @PathVariable long friendId) {
        log.debug("Going to create account {} friendship {}", accountId, friendId);
        Friendship result = friendshipService.createFriendship(accountId, friendId);
        log.debug("Account {} friendship {} has been successfully created {}", accountId, friendId, result);
        URI uri = fromController(getClass())
                .path("/{friendId}")
                .buildAndExpand(accountId, friendId)
                .toUri();
        return created(uri).body(result);
    }

    @PatchMapping(value = "/{friendId}", params = "part=status")
    @ResponseStatus(NO_CONTENT)
    public void editFriendshipStatus(@PathVariable long accountId, @PathVariable long friendId,
                                     @RequestBody Friendship friendshipStatus) {
        friendshipStatus.setId(new Id(accountId, friendId));
        log.debug("Going to edit account {} friendship {} status {}", accountId, friendId, friendshipStatus);
        Friendship result = friendshipService.editFriendshipStatus(accountId, friendId, friendshipStatus);
        log.debug("Account {} friendship {} status has been successfully edited {}", accountId, friendId, result);
    }

    @DeleteMapping("/{friendId}")
    @ResponseStatus(NO_CONTENT)
    public void deleteFriendship(@PathVariable long accountId, @PathVariable long friendId) {
        log.debug("Going to delete account {} friendship {}", accountId, friendId);
        friendshipService.deleteFriendship(accountId, friendId);
        log.debug("Account {} friendship {} has been successfully deleted", accountId, friendId);
    }

}
