package com.getjavajob.training.matiivv.socialnetwork.web.controller.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.AccountMessage;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.MessageSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.account.AccountMessageService;
import com.getjavajob.training.matiivv.socialnetwork.web.controller.MultipartFileResourceAdapter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;

import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.fromController;

@Slf4j
@RestController
@RequestMapping(path = "/accounts/{accountId}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
public class AccountMessageRestController {

    private AccountMessageService accountMessageService;

    public AccountMessageService getAccountMessageService() {
        return accountMessageService;
    }

    @Autowired
    public void setAccountMessageService(AccountMessageService accountMessageService) {
        this.accountMessageService = accountMessageService;
    }

    @GetMapping("/authors/{authorId}/messages/{messageId}")
    public AccountMessage getAccountMessage(@PathVariable long accountId, @PathVariable long authorId, @PathVariable long messageId) {
        log.debug("Going to get account message {} for account {} from author {}", messageId, accountId, authorId);
        return accountMessageService.getAccountMessage(accountId, authorId, messageId);
    }

    @GetMapping("/messages")
    public Page<MessageSummary> getAccountMessages(@PathVariable long accountId, Pageable pageRequest) {
        log.debug("Going to get account messages for account {} / {}", accountId, pageRequest);
        return accountMessageService.getAccountMessages(accountId, pageRequest);
    }

    @PostMapping(path = "/authors/{authorId}/messages", consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<MessageSummary> sendAccountMessage(@PathVariable long accountId, @PathVariable long authorId,
                                                             @RequestPart AccountMessage message,
                                                             @RequestPart(required = false) MultipartFile image) {
        log.debug("Going to send account message for account {} from author {} {}", accountId, authorId, message);
        AccountMessage result = accountMessageService.sendAccountMessage(accountId, authorId, message,
                image == null ? null : new MultipartFileResourceAdapter(image));
        log.debug("Account message for account {} from author {} has been successfully sent {}", accountId, authorId, result);
        URI uri = fromController(getClass())
                .path("/authors/{authorId}/messages/{messageId}")
                .buildAndExpand(accountId, authorId, result.getId())
                .toUri();
        return created(uri).body(new MessageSummary(
                result.getId(),
                result.getAuthor().getId(),
                result.getAuthor().getFirstName(),
                result.getAuthor().getLastName(),
                result.getAuthor().getImageId(),
                result.getAuthor().isDeleted(),
                result.getMessage(),
                result.getImageId(),
                result.getPostedAt()
        ));
    }

    @DeleteMapping("/authors/{authorId}/messages/{messageId}")
    @ResponseStatus(NO_CONTENT)
    public void deleteAccountMessage(@PathVariable long accountId, @PathVariable long authorId, @PathVariable long messageId) {
        log.debug("Going to delete account message {} for account {} from author {}", messageId, accountId, authorId);
        accountMessageService.deleteAccountMessage(accountId, authorId, messageId);
        log.debug("Account message {} for account {} from author {} has been successfully deleted", messageId, accountId, authorId);

    }

}
