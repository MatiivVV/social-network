package com.getjavajob.training.matiivv.socialnetwork.web.configuration.security;

import com.getjavajob.training.matiivv.socialnetwork.service.security.EntityPermissionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import org.springframework.security.oauth2.provider.expression.OAuth2MethodSecurityExpressionHandler;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class MethodSecurityConfig extends GlobalMethodSecurityConfiguration {

    private EntityPermissionManager entityPermissionManager;

    @Autowired
    public void setEntityPermissionManager(EntityPermissionManager entityPermissionManager) {
        this.entityPermissionManager = entityPermissionManager;
    }

    @Override
    protected MethodSecurityExpressionHandler createExpressionHandler() {
        var expressionHandler = new OAuth2MethodSecurityExpressionHandler();
        expressionHandler.setPermissionEvaluator(entityPermissionManager);
        return expressionHandler;
    }

}
