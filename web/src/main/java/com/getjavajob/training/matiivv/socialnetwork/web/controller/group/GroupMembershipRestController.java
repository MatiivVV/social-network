package com.getjavajob.training.matiivv.socialnetwork.web.controller.group;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Membership;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Membership.Id;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.group.GroupMembershipSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.group.MembershipService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Set;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.Role;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.fromController;

@Slf4j
@RestController
@RequestMapping(path = "/groups/{groupId}/memberships", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
public class GroupMembershipRestController {

    private MembershipService membershipService;

    public MembershipService getMembershipService() {
        return membershipService;
    }

    @Autowired
    public void setMembershipService(MembershipService membershipService) {
        this.membershipService = membershipService;
    }

    @GetMapping("/{memberId}")
    public Membership getMembership(@PathVariable long groupId, @PathVariable long memberId) {
        log.debug("Going to get group {} membership {}", groupId, memberId);
        return membershipService.getMembership(groupId, memberId);
    }

    @GetMapping
    public Page<GroupMembershipSummary> getMemberships(@PathVariable long groupId,
                                                       @RequestParam(required = false) Set<Role> roles,
                                                       @RequestParam(required = false) Set<Status> statuses,
                                                       Pageable pageRequest) {
        log.debug("Going to get group {} memberships for roles {} and statuses {} / {}", groupId, roles, statuses, pageRequest);
        return membershipService.getGroupMemberships(groupId, roles, statuses, pageRequest);
    }

    @GetMapping(params = "members")
    public Page<GroupMembershipSummary> getMembers(@PathVariable long groupId,
                                                   @RequestParam(required = false) Set<Role> roles,
                                                   Pageable pageRequest) {
        log.debug("Going to get group {} members for roles {} / {}", groupId, roles, pageRequest);
        return membershipService.getGroupMembers(groupId, roles, pageRequest);
    }

    @PostMapping("/{memberId}")
    public ResponseEntity<Membership> createMembership(@PathVariable long groupId, @PathVariable long memberId) {
        log.debug("Going to create group {} membership {}", groupId, memberId);
        Membership result = membershipService.createMembership(groupId, memberId);
        log.debug("Group {} membership {} has been successfully created {}", groupId, memberId, result);
        URI uri = fromController(getClass())
                .path("/{memberId}")
                .buildAndExpand(groupId, memberId)
                .toUri();
        return created(uri).body(result);
    }

    @PatchMapping(value = "/{memberId}", params = "part=role")
    @ResponseStatus(NO_CONTENT)
    public void editMembershipRole(@PathVariable long groupId, @PathVariable long memberId,
                                   @RequestBody Membership membershipRole) {
        membershipRole.setId(new Id(groupId, memberId));
        log.debug("Going to edit group {} membership {} role {}", groupId, memberId, membershipRole);
        Membership result = membershipService.editMembershipRole(groupId, memberId, membershipRole);
        log.debug("Group {} membership {} role has been successfully edited {}", groupId, memberId, result);

    }

    @PatchMapping(value = "/{memberId}", params = "part=status")
    @ResponseStatus(NO_CONTENT)
    public void editMembershipStatus(@PathVariable long groupId, @PathVariable long memberId,
                                     @RequestBody Membership membershipStatus) {
        membershipStatus.setId(new Id(groupId, memberId));
        log.debug("Going to edit group {} membership {} status {}", groupId, memberId, membershipStatus);
        Membership result = membershipService.editMembershipStatus(groupId, memberId, membershipStatus);
        log.debug("Group {} membership {} status has been successfully edited {}", groupId, memberId, result);
    }

    @DeleteMapping("/{memberId}")
    @ResponseStatus(NO_CONTENT)
    public void deleteMembership(@PathVariable long groupId, @PathVariable long memberId) {
        log.debug("Going to delete group {} membership {}", groupId, memberId);
        membershipService.deleteMembership(groupId, memberId);
        log.debug("Group {} membership {} has been successfully deleted", groupId, memberId);
    }

}
