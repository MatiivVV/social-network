package com.getjavajob.training.matiivv.socialnetwork.web.controller.group;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Membership;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.group.AccountMembershipSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.group.MembershipService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Set;

import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.fromController;

@Slf4j
@RestController
@RequestMapping(path = "/accounts/{accountId}/memberships", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
public class AccountMembershipRestController {

    private MembershipService membershipService;

    public MembershipService getMembershipService() {
        return membershipService;
    }

    @Autowired
    public void setMembershipService(MembershipService membershipService) {
        this.membershipService = membershipService;
    }

    @GetMapping("/{groupId}")
    public Membership getMembership(@PathVariable long accountId, @PathVariable long groupId) {
        log.debug("Going to get account {} membership {}", accountId, groupId);
        return membershipService.getMembership(groupId, accountId);
    }

    @GetMapping
    public Page<AccountMembershipSummary> getMemberships(@PathVariable long accountId,
                                                         @RequestParam(required = false) Set<Account.Role> roles,
                                                         @RequestParam(required = false) Set<Status> statuses,
                                                         Pageable pageRequest) {
        log.debug("Going to get account {} memberships for roles {} and statuses {} / {}", accountId, roles, statuses, pageRequest);
        return membershipService.getAccountMemberships(accountId, roles, statuses, pageRequest);
    }

    @GetMapping(params = "groups")
    public Page<AccountMembershipSummary> getGroups(@PathVariable long accountId,
                                                    @RequestParam(required = false) Set<Account.Role> roles,
                                                    Pageable pageRequest) {
        log.debug("Going to get account {} groups for roles {} / {}", accountId, roles, pageRequest);
        return membershipService.getAccountGroups(accountId, roles, pageRequest);
    }

    @PostMapping("/{groupId}")
    public ResponseEntity<Membership> createMembership(@PathVariable long accountId, @PathVariable long groupId) {
        log.debug("Going to create account {} membership {}", accountId, groupId);
        Membership result = membershipService.createMembership(groupId, accountId);
        log.debug("Account {} membership {} has been successfully created {}", accountId, groupId, result);
        URI uri = fromController(getClass())
                .path("/{groupId}")
                .buildAndExpand(accountId, groupId)
                .toUri();
        return created(uri).body(result);
    }

    @PatchMapping(value = "/{groupId}", params = "part=role")
    @ResponseStatus(NO_CONTENT)
    public void editMembershipRole(@PathVariable long accountId, @PathVariable long groupId,
                                   @RequestBody Membership membershipRole) {
        membershipRole.setId(new Membership.Id(groupId, accountId));
        log.debug("Going to edit account {} membership {} role {}", accountId, groupId, membershipRole);
        Membership result = membershipService.editMembershipRole(groupId, accountId, membershipRole);
        log.debug("Account {} membership {} role has been successfully edited {}", accountId, groupId, result);
    }

    @PatchMapping(value = "/{groupId}", params = "part=status")
    @ResponseStatus(NO_CONTENT)
    public void editMembershipStatus(@PathVariable long accountId, @PathVariable long groupId,
                                     @RequestBody Membership membershipStatus) {
        membershipStatus.setId(new Membership.Id(groupId, accountId));
        log.debug("Going to edit account {} membership {} status {}", accountId, groupId, membershipStatus);
        Membership result = membershipService.editMembershipStatus(groupId, accountId, membershipStatus);
        log.debug("Account {} membership {} status has been successfully edited {}", accountId, groupId, result);
    }

    @DeleteMapping("/{groupId}")
    @ResponseStatus(NO_CONTENT)
    public void deleteMembership(@PathVariable long accountId, @PathVariable long groupId) {
        log.debug("Going to delete account {} membership {}", accountId, groupId);
        membershipService.deleteMembership(groupId, accountId);
        log.debug("Account {} membership {} has been successfully deleted", accountId, groupId);
    }

}
