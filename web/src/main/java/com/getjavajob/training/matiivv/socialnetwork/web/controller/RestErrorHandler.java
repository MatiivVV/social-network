package com.getjavajob.training.matiivv.socialnetwork.web.controller;

import com.getjavajob.training.matiivv.socialnetwork.domain.projection.ConstraintViolation;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.Error;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.NotFoundEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.util.unit.DataSize;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Locale;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

@Slf4j
@RestControllerAdvice(annotations = RestController.class)
public class RestErrorHandler {

    private static final String CONSTRAINT_VIOLATION_CODE = "exception.constraintViolation";
    private static final String MAX_UPLOAD_SIZE_EXCEEDED_CODE = "exception.maxUploadSizeExceeded";

    private MessageSource messageSource;

    public MessageSource getMessageSource() {
        return messageSource;
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ResponseStatus(NOT_FOUND)
    @ExceptionHandler(NotFoundEntityException.class)
    public Error handleNotFoundException(NotFoundEntityException e, Locale locale) {
        log.debug("Going to handle {} {}", e.getClass().getSimpleName(), e);
        var result = new Error(e.getDefaultMessage(), e.getLocalizedMessage(messageSource, locale), null);
        log.info("{} has been successfully handled {}", e.getClass().getSimpleName(), result);
        return result;
    }

    @ResponseStatus(UNPROCESSABLE_ENTITY)
    @ExceptionHandler(ServiceException.class)
    public Error handleServiceException(ServiceException e, Locale locale) {
        log.debug("Going to handle {} {}", e.getClass().getSimpleName(), e);
        var result = new Error(e.getDefaultMessage(), e.getLocalizedMessage(messageSource, locale), null);
        log.info("{} has been successfully handled {}", e.getClass().getSimpleName(), result);
        return result;
    }

    @ResponseStatus(UNPROCESSABLE_ENTITY)
    @ExceptionHandler(ConstraintViolationException.class)
    public Error handleValidationException(ConstraintViolationException e, Locale locale) {
        log.debug("Going to handle {} {}", e.getClass().getSimpleName(), e);
        List<ConstraintViolation> constraintViolations = e.getConstraintViolations().stream()
                .map(v -> new ConstraintViolation(
                        v.getPropertyPath().toString(),
                        v.getMessageTemplate().substring(1, v.getMessageTemplate().length() - 1),
                        v.getMessage())
                )
                .collect(toList());
        String message = messageSource.getMessage(CONSTRAINT_VIOLATION_CODE, new Object[]{}, locale);
        var result = new Error(CONSTRAINT_VIOLATION_CODE, message, constraintViolations);
        log.info("{} has been successfully handled {}", e.getClass().getSimpleName(), result);
        return result;
    }

    @ResponseStatus(UNPROCESSABLE_ENTITY)
    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public Error handleMaxUploadSizeExceededException(MaxUploadSizeExceededException e, Locale locale) {
        log.debug("Going to handle {} {}", e.getClass().getSimpleName(), e);
        long limit = DataSize.ofBytes(e.getMaxUploadSize()).toMegabytes();
        String message = messageSource.getMessage(MAX_UPLOAD_SIZE_EXCEEDED_CODE, new Object[]{limit}, locale);
        var result = new Error(MAX_UPLOAD_SIZE_EXCEEDED_CODE, message, null);
        log.info("{} has been successfully handled {}", e.getClass().getSimpleName(), result);
        return result;
    }

}
