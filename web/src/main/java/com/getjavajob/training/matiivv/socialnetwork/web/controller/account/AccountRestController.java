package com.getjavajob.training.matiivv.socialnetwork.web.controller.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.AccountSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.account.AccountService;
import com.getjavajob.training.matiivv.socialnetwork.web.controller.MultipartFileResourceAdapter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;

import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.MediaType.*;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.fromController;

@Slf4j
@RestController
@RequestMapping(value = "/accounts", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
public class AccountRestController {

    private AccountService accountService;

    public AccountService getAccountService() {
        return accountService;
    }

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping(value = "/{id}", produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    public Account getAccount(@PathVariable long id) {
        log.debug("Going to get account {}", id);
        return accountService.getAccount(id);
    }

    @GetMapping
    public Page<AccountSummary> getAccounts(@RequestParam String pattern, Pageable pageRequest) {
        log.debug("Going to get accounts for pattern {} / {}", pattern, pageRequest);
        return accountService.getAccounts(pattern, pageRequest);
    }

    @PostMapping(consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Account> registerAccount(@RequestPart Account account,
                                                   @RequestPart(required = false) MultipartFile accountImage) {
        log.debug("Going to register account {}", account);
        Account result = accountService.registerAccount(account,
                accountImage == null ? null : new MultipartFileResourceAdapter(accountImage));
        log.debug("Account has been successfully registered {}", result);
        URI uri = fromController(getClass())
                .path("/{id}")
                .buildAndExpand(result.getId())
                .toUri();
        return created(uri).body(result);
    }

    @PatchMapping(value = "/{id}", params = "part=info", consumes = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    @ResponseStatus(NO_CONTENT)
    public void editAccountInfo(@PathVariable long id, @RequestBody Account accountInfo) {
        accountInfo.setId(id);
        log.debug("Going to edit account info {}", accountInfo);
        Account result = accountService.editAccountInfo(id, accountInfo);
        log.debug("Account info has been successfully edited {}", result);
    }

    @PatchMapping(value = "/{id}", params = "part=password")
    @ResponseStatus(NO_CONTENT)
    public void editAccountPassword(@PathVariable long id, @RequestBody Account accountPassword) {
        accountPassword.setId(id);
        log.debug("Going to edit account {} password", id);
        accountService.editAccountPassword(id, accountPassword);
        log.debug("Account {} password has been successfully edited", id);
    }

    @PatchMapping(value = "/{id}", params = "part=role")
    @ResponseStatus(NO_CONTENT)
    public void editAccountRole(@PathVariable long id, @RequestBody Account accountRole) {
        accountRole.setId(id);
        log.debug("Going to edit account role {}", accountRole);
        Account result = accountService.editAccountRole(id, accountRole);
        log.debug("Account role has been successfully edited {}", result);
    }

    @PatchMapping(value = "/{id}", params = "part=image",
            consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> editAccountImage(@PathVariable long id, @RequestPart(required = false) MultipartFile accountImage) {
        log.debug("Going to edit account {} image", id);
        Account result = accountService.editAccountImage(id,
                accountImage == null ? null : new MultipartFileResourceAdapter(accountImage));
        log.debug("Account {} image has been successfully edited", id);
        if (result.getImageId() == null) {
            return noContent().build();
        } else {
            return noContent().location(URI.create(result.getImageId())).build();
        }
    }

    @PatchMapping(value = "/{id}", params = "part=session")
    @ResponseStatus(NO_CONTENT)
    public void resetAccountSession(@PathVariable long id) {
        log.debug("Going to reset account {} session", id);
        accountService.resetAccountSession(id);
        log.debug("Account {} session has been successfully reset", id);
    }

    @PatchMapping(value = "/{id}", params = "part=deleted")
    @ResponseStatus(NO_CONTENT)
    public void editAccountDeleted(@PathVariable long id, @RequestBody Account accountDeleted) {
        accountDeleted.setId(id);
        log.debug("Going to edit account deleted flag {}", accountDeleted);
        accountService.editAccountDeleted(id, accountDeleted);
        log.debug("Account deleted flag has been successfully edited {}", id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public void deleteAccount(@PathVariable long id) {
        log.debug("Going to delete account {}", id);
        var accountDeleted = new Account(id);
        accountDeleted.setDeleted(true);
        accountService.editAccountDeleted(id, accountDeleted);
        log.debug("Account {} has been successfully deleted", id);
    }

}
