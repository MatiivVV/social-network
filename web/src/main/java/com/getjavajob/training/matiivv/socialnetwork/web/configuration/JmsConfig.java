package com.getjavajob.training.matiivv.socialnetwork.web.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.annotation.EnableJms;

@Configuration
@EnableJms
@Profile("development")
public class JmsConfig {
}
