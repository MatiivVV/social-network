package com.getjavajob.training.matiivv.socialnetwork.web.controller.account;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.ChatMessage;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.ChatMessageSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.account.ChatMessageService;
import com.getjavajob.training.matiivv.socialnetwork.web.controller.MultipartFileResourceAdapter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.fromController;

@Slf4j
@RestController
@RequestMapping(path = "/accounts/{accountId}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
public class ChatMessageRestController {

    private ChatMessageService chatMessageService;
    private SimpMessagingTemplate template;
    private ObjectMapper objectMapper;

    public ChatMessageService getChatMessageService() {
        return chatMessageService;
    }

    @Autowired
    public void setChatMessageService(ChatMessageService chatMessageService) {
        this.chatMessageService = chatMessageService;
    }

    @Autowired
    public void setTemplate(SimpMessagingTemplate template) {
        this.template = template;
    }

    @Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @GetMapping("/authors/{authorId}/chatMessages/{messageId}")
    public ChatMessage getChatMessage(@PathVariable long accountId, @PathVariable long authorId, @PathVariable long messageId) {
        log.debug("Going to get chat message {} for account {} from author {}", messageId, accountId, authorId);
        return chatMessageService.getChatMessage(accountId, authorId, messageId);
    }

    @GetMapping("/opponents/{opponentId}/chatMessages")
    public Page<ChatMessageSummary> getChatMessages(@PathVariable long accountId, @PathVariable long opponentId, Pageable pageRequest) {
        log.debug("Going to get chat messages for account {} and opponent {} / {}", accountId, opponentId, pageRequest);
        return chatMessageService.getChatMessages(accountId, opponentId, pageRequest);
    }

    @PostMapping(path = "/authors/{authorId}/chatMessages", consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ChatMessageSummary> sendChatMessage(@PathVariable long accountId, @PathVariable long authorId,
                                                              @RequestPart ChatMessage message,
                                                              @RequestPart(required = false) MultipartFile image) throws JsonProcessingException {
        log.debug("Going to create chat message for account {} from author {} {}", accountId, authorId, message);
        ChatMessage result = chatMessageService.sendChatMessage(accountId, authorId, message,
                image == null ? null : new MultipartFileResourceAdapter(image));
        log.debug("Chat message for account {} from author {} has been successfully created {}", accountId, authorId, result);
        var chatMessageSummary = new ChatMessageSummary(
                result.getId(),
                authorId,
                accountId,
                result.getMessage(),
                result.getImageId(),
                result.getPostedAt(),
                result.isRead()
        );
        log.debug("Going to send chat message for account {} from author {} {}", accountId, authorId, message);
        template.convertAndSendToUser(result.getRecipient().getEmailAddress(), "/topic/notifications/",
                objectMapper.writeValueAsString(chatMessageSummary));
        log.debug("Chat message for account {} from author {} has been successfully sent {}", accountId, authorId, result);
        URI uri = fromController(getClass())
                .path("/authors/{authorId}/chatMessages/{messageId}")
                .buildAndExpand(accountId, authorId, result.getId())
                .toUri();
        return created(uri).body(chatMessageSummary);
    }

}
