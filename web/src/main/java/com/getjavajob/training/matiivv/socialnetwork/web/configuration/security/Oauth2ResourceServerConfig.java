package com.getjavajob.training.matiivv.socialnetwork.web.configuration.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Configuration
@EnableResourceServer
public class Oauth2ResourceServerConfig extends ResourceServerConfigurerAdapter {

    private Oauth2Properties.Resource resourceProperties;

    @Autowired
    public void setProperties(Oauth2Properties properties) {
        resourceProperties = properties.getResource();
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId(resourceProperties.getId())
                .stateless(true);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(POST, "/api/accounts").permitAll()
                .antMatchers(GET, "/api/images/**").permitAll()
                .antMatchers("/api/**").authenticated()
                .antMatchers("/notifications/**").authenticated()
                .anyRequest().permitAll();
        http.exceptionHandling()
                .accessDeniedHandler(new OAuth2AccessDeniedHandler());
        http.sessionManagement()
                .sessionCreationPolicy(STATELESS);
        http.csrf()
                .disable();
        http.requiresChannel()
                .requestMatchers(r -> r.getHeader("X-Forwarded-Proto") != null).requiresSecure();
    }

}
