package com.getjavajob.training.matiivv.socialnetwork.service.service.resource;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.context.ApplicationEvent;

@Value
@EqualsAndHashCode(callSuper = true)
public class ResourceCreatedEvent extends ApplicationEvent {

    private String id;

    public ResourceCreatedEvent(Object source, String id) {
        super(source);
        this.id = id;
    }

}
