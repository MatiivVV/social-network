package com.getjavajob.training.matiivv.socialnetwork.service.service.exception;

public class NotFoundEntityException extends ServiceException {

    public NotFoundEntityException(String errorCode, Object... arguments) {
        super(errorCode, arguments);
    }

    public NotFoundEntityException(Throwable cause, String errorCode, Object... arguments) {
        super(cause, errorCode, arguments);
    }

}
