package com.getjavajob.training.matiivv.socialnetwork.service.service.account;

import com.getjavajob.training.matiivv.socialnetwork.dao.account.AccountRepository;
import com.getjavajob.training.matiivv.socialnetwork.dao.account.FriendshipRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Friendship;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.FriendshipSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.event.BusinessEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.AlreadyExistsEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.IllegalStateEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.NotFoundEntityException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.EnumSet;
import java.util.Set;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status.*;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Friendship.Id;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Friendship.Type;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Friendship.Type.REQUEST;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Friendship.Type.RESPONSE;
import static java.lang.String.format;

@Slf4j
@Service
@Validated
@Transactional(readOnly = true)
public class FriendshipService {

    private AccountRepository accountRepository;
    private FriendshipRepository friendshipRepository;
    private ApplicationEventPublisher eventPublisher;

    public AccountRepository getAccountRepository() {
        return accountRepository;
    }

    @Autowired
    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public FriendshipRepository getFriendshipRepository() {
        return friendshipRepository;
    }

    @Autowired
    public void setFriendshipRepository(FriendshipRepository friendshipRepository) {
        this.friendshipRepository = friendshipRepository;
    }

    public ApplicationEventPublisher getEventPublisher() {
        return eventPublisher;
    }

    @Autowired
    public void setEventPublisher(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @PreAuthorize("hasPermission(#accountId, 'Account', 'EDIT')")
    public Friendship getFriendship(long accountId, long friendId) {
        log.debug("Going to find account {} friendship {}", accountId, friendId);
        return friendshipRepository.findById(new Id(accountId, friendId)).orElseThrow(() -> {
            log.warn("Not found account {} friendship {}", accountId, friendId);
            return new NotFoundEntityException("exception.notFound.friendship.id");
        });
    }

    @PreAuthorize("hasPermission(#accountId, 'Account', 'EDIT')")
    public Page<FriendshipSummary> getFriendships(long accountId, Set<Type> types,
                                                  Set<Status> statuses, Pageable pageRequest) {
        log.debug("Going to find account {} friendships for types {} and statuses {} / {}", accountId, types, statuses, pageRequest);
        return friendshipRepository.search(accountId, types, statuses, pageRequest);
    }

    @PreAuthorize("hasPermission(#accountId, 'Account', 'READ')")
    public Page<FriendshipSummary> getFriends(long accountId, Pageable pageRequest) {
        log.debug("Going to find account {} friends / {}", accountId, pageRequest);
        return friendshipRepository.search(accountId, null, EnumSet.of(ACCEPTED), pageRequest);
    }

    @Transactional
    @PreAuthorize("hasPermission(#accountId, 'Account', 'EDIT')")
    public Friendship createFriendship(long accountId, long friendId) {
        if (accountId == friendId) {
            throw new IllegalStateEntityException("exception.illegalState.friendship.id.selfReference");
        }
        var request = new Friendship(new Id());
        var response = new Friendship(new Id());
        if (accountRepository.existsById(accountId)) {
            Account account = accountRepository.getOne(accountId);
            request.setAccount(account);
            response.setFriend(account);
        } else {
            log.warn("Not found friendship account {}", accountId);
            throw new NotFoundEntityException("exception.notFound.friendship.account.id");
        }
        if (accountRepository.existsById(friendId)) {
            Account friend = accountRepository.getOne(friendId);
            request.setFriend(friend);
            response.setAccount(friend);
        } else {
            log.warn("Not found friendship friend {}", friendId);
            throw new NotFoundEntityException("exception.notFound.friendship.friend.id");
        }
        request.setType(REQUEST);
        response.setType(RESPONSE);
        request.setStatus(PENDING);
        response.setStatus(PENDING);
        try {
            log.debug("Going to create account {} friendship {}", accountId, friendId);
            friendshipRepository.save(response);
            Friendship result = friendshipRepository.saveAndFlush(request);
            eventPublisher.publishEvent(new BusinessEvent(this, format("Friend request from '%s %s' / id '%d' to '%s %s' / id '%d' was created.",
                    result.getAccount().getFirstName(),
                    result.getAccount().getLastName(),
                    result.getAccount().getId(),
                    result.getFriend().getFirstName(),
                    result.getFriend().getLastName(),
                    result.getFriend().getId()
            )));
            log.info("Account {} friendship {} has been successfully created {}", accountId, friendId, result);
            return result;
        } catch (DataIntegrityViolationException e) {
            log.warn("Already exists account {} friendship {}", accountId, friendId);
            throw new AlreadyExistsEntityException(e, "exception.alreadyExists.friendship.id");
        }
    }

    @Transactional
    @PreAuthorize("hasPermission(#accountId, 'Account', 'EDIT')")
    public Friendship editFriendshipStatus(long accountId, long friendId, @Valid Friendship friendshipStatus) {
        Friendship friendship = getFriendship(accountId, friendId);
        if (friendship.getType() == REQUEST &&
                !(friendship.getStatus() == DECLINED && friendshipStatus.getStatus() == PENDING)) {
            log.warn("Friendship status {} can not be edited to {}", friendship, friendshipStatus);
            throw new IllegalStateEntityException("exception.illegalState.friendship.status");
        }
        if (friendship.getType() == RESPONSE && friendship.getStatus() != PENDING) {
            log.warn("Friendship status {} can not be edited to {}", friendship, friendshipStatus);
            throw new IllegalStateEntityException("exception.illegalState.friendship.status");
        }
        friendship.setStatus(friendshipStatus.getStatus());
        Friendship inverseFriendship = getFriendship(friendId, accountId);
        inverseFriendship.setStatus(friendshipStatus.getStatus());
        log.debug("Going to edit account {} friendship {} status {}", accountId, friendId, friendshipStatus);
        friendshipRepository.save(inverseFriendship);
        Friendship result = friendshipRepository.saveAndFlush(friendship);
        eventPublisher.publishEvent(new BusinessEvent(this, format("Friend request (from '%s %s' / id '%d' to '%s %s' / id '%d') status was changed to '%s'.",
                result.getAccount().getFirstName(),
                result.getAccount().getLastName(),
                result.getAccount().getId(),
                result.getFriend().getFirstName(),
                result.getFriend().getLastName(),
                result.getFriend().getId(),
                result.getStatus().toString().toLowerCase()
        )));
        log.info("Account {} friendship {} status has been successfully edited {}", accountId, friendId, result);
        return result;
    }

    @Transactional
    @PreAuthorize("hasPermission(#accountId, 'Account', 'EDIT')")
    public void deleteFriendship(long accountId, long friendId) {
        try {
            log.debug("Going to delete account {} friendship {}", accountId, friendId);
            Friendship friendship = getFriendship(accountId, friendId);
            if (friendship.getStatus() == ACCEPTED) {
                eventPublisher.publishEvent(new BusinessEvent(this, format("Account '%s %s' / id '%d' friend '%s %s' / id '%d' was deleted.",
                        friendship.getAccount().getFirstName(),
                        friendship.getAccount().getLastName(),
                        friendship.getAccount().getId(),
                        friendship.getFriend().getFirstName(),
                        friendship.getFriend().getLastName(),
                        friendship.getFriend().getId()
                )));
            } else {
                eventPublisher.publishEvent(new BusinessEvent(this, format("Friend request from '%s %s' / id '%d' to '%s %s' / id '%d' was deleted.",
                        friendship.getAccount().getFirstName(),
                        friendship.getAccount().getLastName(),
                        friendship.getAccount().getId(),
                        friendship.getFriend().getFirstName(),
                        friendship.getFriend().getLastName(),
                        friendship.getFriend().getId()
                )));
            }
            friendshipRepository.deleteById(new Id(accountId, friendId));
            friendshipRepository.deleteById(new Id(friendId, accountId));
            friendshipRepository.flush();
            log.info("Account {} friendship {} has been successfully deleted", accountId, friendId);
        } catch (EmptyResultDataAccessException e) {
            log.warn("Not found account {} friendship {} {}", accountId, friendId, e);
            throw new NotFoundEntityException(e, "exception.notFound.friendship.id");
        }
    }

}
