package com.getjavajob.training.matiivv.socialnetwork.service.service.account;

import com.getjavajob.training.matiivv.socialnetwork.dao.account.AccountRepository;
import com.getjavajob.training.matiivv.socialnetwork.dao.account.ChatMessageRepository;
import com.getjavajob.training.matiivv.socialnetwork.dao.account.ChatRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Chat;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.ChatSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.event.BusinessEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.AlreadyExistsEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.NotFoundEntityException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Chat.Id;
import static java.lang.String.format;

@Slf4j
@Service
@Validated
@Transactional(readOnly = true)
public class ChatService {

    private AccountRepository accountRepository;
    private ChatRepository chatRepository;
    private ChatMessageRepository chatMessageRepository;
    private ApplicationEventPublisher eventPublisher;

    public AccountRepository getAccountRepository() {
        return accountRepository;
    }

    @Autowired
    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public ChatRepository getChatRepository() {
        return chatRepository;
    }

    @Autowired
    public void setChatRepository(ChatRepository chatRepository) {
        this.chatRepository = chatRepository;
    }

    public ChatMessageRepository getChatMessageRepository() {
        return chatMessageRepository;
    }

    @Autowired
    public void setChatMessageRepository(ChatMessageRepository chatMessageRepository) {
        this.chatMessageRepository = chatMessageRepository;
    }

    public ApplicationEventPublisher getEventPublisher() {
        return eventPublisher;
    }

    @Autowired
    public void setEventPublisher(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @PreAuthorize("hasPermission(#accountId, 'Account', 'USE_CHAT') OR hasPermission(#opponentId, 'Account', 'USE_CHAT')")
    public boolean chatExists(long accountId, long opponentId) {
        return chatRepository.existsById(new Id(accountId, opponentId));
    }

    @PreAuthorize("hasPermission(#accountId, 'Account', 'USE_CHAT')")
    public Chat getChat(long accountId, long opponentId) {
        log.debug("Going to find account {} chat {}", accountId, opponentId);
        return chatRepository.findById(new Id(accountId, opponentId)).orElseThrow(() -> {
            log.warn("Not found account {} chat {}", accountId, opponentId);
            return new NotFoundEntityException("exception.notFound.chat.id");
        });
    }

    @PreAuthorize("hasPermission(#accountId, 'Account', 'USE_CHAT')")
    public Page<ChatSummary> getChats(long accountId, Pageable pageRequest) {
        log.debug("Going to find account {} chats / {}", accountId, pageRequest);
        return chatRepository.findByAccountId(accountId, pageRequest);
    }

    @Transactional
    @PreAuthorize("hasPermission(#accountId, 'Account', 'USE_CHAT') OR hasPermission(#opponentId, 'Account', 'USE_CHAT')")
    public Chat createChat(long accountId, long opponentId) {
        var chat = new Chat(new Id(accountId, opponentId));
        if (accountRepository.existsById(accountId)) {
            chat.setAccount(accountRepository.getOne(accountId));
        } else {
            log.warn("Not found chat account {}", accountId);
            throw new NotFoundEntityException("exception.notFound.chat.account.id");
        }
        if (accountRepository.existsById(opponentId)) {
            chat.setOpponent(accountRepository.getOne(opponentId));
        } else {
            log.warn("Not found chat opponent {}", opponentId);
            throw new NotFoundEntityException("exception.notFound.chat.opponent.id");
        }

        try {
            log.debug("Going to create account {} chat {}", accountId, opponentId);
            Chat result = chatRepository.saveAndFlush(chat);
            eventPublisher.publishEvent(new BusinessEvent(this, format("Account '%s %s' / id '%d' started chat with '%s %s' / id '%d'.",
                    result.getAccount().getFirstName(),
                    result.getAccount().getLastName(),
                    result.getAccount().getId(),
                    result.getOpponent().getFirstName(),
                    result.getOpponent().getLastName(),
                    result.getOpponent().getId()
            )));
            log.info("Account {} chat {} has been successfully created {}", accountId, opponentId, result);
            return result;
        } catch (DataIntegrityViolationException e) {
            log.warn("Already exists account {} chat {}", accountId, opponentId);
            throw new AlreadyExistsEntityException(e, "exception.alreadyExists.chat.id");
        }
    }

    @Transactional
    @PreAuthorize("hasPermission(#accountId, 'Account', 'USE_CHAT')")
    public long setChatRead(long accountId, long opponentId) {
        log.debug("Going to set account {} chat {} read", accountId, opponentId);
        int result = chatMessageRepository.setReadByRecipientIdAndAuthorId(accountId, opponentId);
        log.info("Account {} chat {} has been successfully set read - {} messages", accountId, opponentId, result);
        return result;
    }

    @Transactional
    @PreAuthorize("hasPermission(#accountId, 'Account', 'USE_CHAT')")
    public void deleteChat(long accountId, long opponentId) {
        try {
            log.debug("Going to delete account {} chat {}", accountId, opponentId);
            Chat chat = getChat(accountId, opponentId);
            eventPublisher.publishEvent(new BusinessEvent(this, format("Account '%s %s' / id '%d' deleted chat with '%s %s' / id '%d'.",
                    chat.getAccount().getFirstName(),
                    chat.getAccount().getLastName(),
                    chat.getAccount().getId(),
                    chat.getOpponent().getFirstName(),
                    chat.getOpponent().getLastName(),
                    chat.getOpponent().getId()
            )));
            chatRepository.deleteById(new Id(accountId, opponentId));
            chatRepository.flush();
            log.info("Account {} chat {} has been successfully deleted", accountId, opponentId);
        } catch (EmptyResultDataAccessException e) {
            log.warn("Not found account {} chat {} {}", accountId, opponentId, e);
            throw new NotFoundEntityException(e, "exception.notFound.chat.id");
        }
    }

}
