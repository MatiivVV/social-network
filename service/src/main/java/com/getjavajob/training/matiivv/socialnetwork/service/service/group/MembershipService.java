package com.getjavajob.training.matiivv.socialnetwork.service.service.group;

import com.getjavajob.training.matiivv.socialnetwork.dao.account.AccountRepository;
import com.getjavajob.training.matiivv.socialnetwork.dao.group.GroupRepository;
import com.getjavajob.training.matiivv.socialnetwork.dao.group.MembershipRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Membership;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Membership.Id;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Membership.ValidRole;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Membership.ValidStatus;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.group.AccountMembershipSummary;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.group.GroupMembershipSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.event.BusinessEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.AlreadyExistsEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.IllegalStateEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.NotFoundEntityException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.EnumSet;
import java.util.Set;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status.*;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.Role;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.Role.USER;
import static java.lang.String.format;

@Slf4j
@Service
@Validated
@Transactional(readOnly = true)
public class MembershipService {

    private AccountRepository accountRepository;
    private GroupRepository groupRepository;
    private MembershipRepository membershipRepository;
    private ApplicationEventPublisher eventPublisher;

    public AccountRepository getAccountRepository() {
        return accountRepository;
    }

    @Autowired
    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public GroupRepository getGroupRepository() {
        return groupRepository;
    }

    @Autowired
    public void setGroupRepository(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    public MembershipRepository getMembershipRepository() {
        return membershipRepository;
    }

    @Autowired
    public void setMembershipRepository(MembershipRepository membershipRepository) {
        this.membershipRepository = membershipRepository;
    }

    public ApplicationEventPublisher getEventPublisher() {
        return eventPublisher;
    }

    @Autowired
    public void setEventPublisher(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @PreAuthorize("hasPermission(#groupId, 'Group', 'EDIT') OR hasPermission(#memberId, 'Account', 'EDIT')")
    public Membership getMembership(long groupId, long memberId) {
        log.debug("Going to find group {} membership {}", groupId, memberId);
        return membershipRepository.findById(new Id(groupId, memberId)).orElseThrow(() -> {
            log.warn("Not found group {} membership {}", groupId, memberId);
            return new NotFoundEntityException("exception.notFound.membership.id");
        });
    }

    @PreAuthorize("hasPermission(#groupId, 'Group', 'EDIT')")
    public Page<GroupMembershipSummary> getGroupMemberships(long groupId, Set<Role> roles,
                                                            Set<Status> statuses, Pageable pageRequest) {
        log.debug("Going to find group {} memberships for roles {} and statuses {} / {}", groupId, roles, statuses, pageRequest);
        return membershipRepository.searchForGroup(groupId, roles, statuses, pageRequest);
    }

    @PreAuthorize("hasPermission(#groupId, 'Group', 'READ')")
    public Page<GroupMembershipSummary> getGroupMembers(long groupId, Set<Role> roles, Pageable pageRequest) {
        log.debug("Going to find group {} members for roles {} / {}", groupId, roles, pageRequest);
        return membershipRepository.searchForGroup(groupId, roles, EnumSet.of(ACCEPTED), pageRequest);
    }

    @PreAuthorize("hasPermission(#accountId, 'Account', 'EDIT')")
    public Page<AccountMembershipSummary> getAccountMemberships(long accountId, Set<Role> roles,
                                                                Set<Status> statuses, Pageable pageRequest) {
        log.debug("Going to find account {} memberships for roles {} and statuses {} / {}", accountId, roles, statuses, pageRequest);
        return membershipRepository.searchForAccount(accountId, roles, statuses, pageRequest);
    }

    @PreAuthorize("hasPermission(#accountId, 'Account', 'READ')")
    public Page<AccountMembershipSummary> getAccountGroups(long accountId, Set<Role> roles, Pageable pageRequest) {
        log.debug("Going to find account {} groups for roles {} / {}", accountId, roles, pageRequest);
        return membershipRepository.searchForAccount(accountId, roles, EnumSet.of(ACCEPTED), pageRequest);
    }

    @Transactional
    @PreAuthorize("hasPermission(#memberId, 'Account', 'EDIT')")
    public Membership createMembership(long groupId, long memberId) {
        var membership = new Membership(new Id());
        if (groupRepository.existsById(groupId)) {
            membership.setGroup(groupRepository.getOne(groupId));
        } else {
            log.warn("Not found membership group {}", groupId);
            throw new NotFoundEntityException("exception.notFound.membership.group.id");
        }
        if (accountRepository.existsById(memberId)) {
            membership.setMember(accountRepository.getOne(memberId));
        } else {
            log.warn("Not found membership member {}", memberId);
            throw new NotFoundEntityException("exception.notFound.membership.member.id");
        }
        membership.setRole(USER);
        membership.setStatus(PENDING);
        try {
            log.debug("Going to create group {} membership {}", groupId, memberId);
            Membership result = membershipRepository.saveAndFlush(membership);
            eventPublisher.publishEvent(new BusinessEvent(this, format("Membership request from '%s %s' / id '%d' to group '%s' / id '%d' was created.",
                    result.getMember().getFirstName(),
                    result.getMember().getLastName(),
                    result.getMember().getId(),
                    result.getGroup().getName(),
                    result.getGroup().getId()
            )));
            log.info("Group {} membership {} has been successfully created {}", groupId, memberId, result);
            return result;
        } catch (DataIntegrityViolationException e) {
            log.warn("Already exists group {} membership {}", groupId, memberId);
            throw new AlreadyExistsEntityException(e, "exception.alreadyExists.membership.id");
        }
    }

    @Transactional
    @Validated(ValidRole.class)
    @PreAuthorize("hasPermission(#groupId, 'Group', 'EDIT')")
    public Membership editMembershipRole(long groupId, long memberId, @Valid Membership membershipRole) {
        Membership membership = getMembership(groupId, memberId);
        if (membership.getStatus() != ACCEPTED) {
            log.warn("Not found group {} member {}", groupId, memberId);
            throw new NotFoundEntityException("exception.notFound.groupMember.id");
        }
        membership.setRole(membershipRole.getRole());
        log.debug("Going to edit group {} member {} role {}", groupId, memberId, membershipRole);
        Membership result = membershipRepository.save(membership);
        eventPublisher.publishEvent(new BusinessEvent(this, format("Group '%s' / id '%d' member '%s %s' / id '%d' role was changed to '%s'.",
                result.getGroup().getName(),
                result.getGroup().getId(),
                result.getMember().getFirstName(),
                result.getMember().getLastName(),
                result.getMember().getId(),
                result.getRole().toString().toLowerCase()
        )));
        log.info("Group {} member {} role has been successfully edited {}", groupId, memberId, result);
        return result;
    }

    @Transactional
    @Validated(ValidStatus.class)
    @PreAuthorize("hasPermission(#groupId, 'Group', 'EDIT') OR hasPermission(#memberId, 'Account', 'EDIT')")
    public Membership editMembershipStatus(long groupId, long memberId, @Valid Membership membershipStatus) {
        Membership membership = getMembership(groupId, memberId);
        if (!(membership.getStatus() == PENDING ||
                membership.getStatus() == DECLINED && membershipStatus.getStatus() == PENDING)) {
            log.warn("Membership status {} can not be edited to {}", membership, membershipStatus);
            throw new IllegalStateEntityException("exception.illegalState.membership.status");
        }
        membership.setStatus(membershipStatus.getStatus());
        log.debug("Going to edit group {} membership {} status {}", groupId, memberId, membershipStatus);
        Membership result = membershipRepository.save(membership);
        eventPublisher.publishEvent(new BusinessEvent(this, format("Membership request (from '%s %s' / id '%d' to group '%s' / id '%d') status was changed to '%s'.",
                result.getMember().getFirstName(),
                result.getMember().getLastName(),
                result.getMember().getId(),
                result.getGroup().getName(),
                result.getGroup().getId(),
                result.getStatus().toString().toLowerCase()
        )));
        log.info("Group {} membership {} status has been successfully edited {}", groupId, memberId, result);
        return result;
    }

    @Transactional
    @PreAuthorize("hasPermission(#groupId, 'Group', 'EDIT') OR hasPermission(#memberId, 'Account', 'EDIT')")
    public void deleteMembership(long groupId, long memberId) {
        try {
            log.debug("Going to delete group {} membership {}", groupId, memberId);
            Membership membership = getMembership(groupId, memberId);
            if (membership.getStatus() == ACCEPTED) {
                eventPublisher.publishEvent(new BusinessEvent(this, format("Group '%s' / id '%d' member '%s %s' / id '%d' was deleted.",
                        membership.getGroup().getName(),
                        membership.getGroup().getId(),
                        membership.getMember().getFirstName(),
                        membership.getMember().getLastName(),
                        membership.getMember().getId()
                )));
            } else {
                eventPublisher.publishEvent(new BusinessEvent(this, format("Membership request from '%s %s' / id '%d' to group '%s' / id '%d' was deleted.",
                        membership.getMember().getFirstName(),
                        membership.getMember().getLastName(),
                        membership.getMember().getId(),
                        membership.getGroup().getName(),
                        membership.getGroup().getId()
                )));
            }
            membershipRepository.deleteById(new Id(groupId, memberId));
            membershipRepository.flush();
            log.info("Group {} membership {} has been successfully deleted", groupId, memberId);
        } catch (EmptyResultDataAccessException e) {
            log.warn("Not found group {} membership {} {}", groupId, memberId, e);
            throw new NotFoundEntityException(e, "exception.notFound.membership.id", memberId);
        }
    }

}
