package com.getjavajob.training.matiivv.socialnetwork.service.service.group;

import com.getjavajob.training.matiivv.socialnetwork.dao.account.AccountRepository;
import com.getjavajob.training.matiivv.socialnetwork.dao.group.GroupMessageRepository;
import com.getjavajob.training.matiivv.socialnetwork.dao.group.GroupRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.GroupMessage;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.MessageSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.event.BusinessEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.NotFoundEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.Resource;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceCreatedEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceDeletedEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

import static java.lang.String.format;

@Slf4j
@Service
@Validated
@Transactional(readOnly = true)
public class GroupMessageService {

    private AccountRepository accountRepository;
    private GroupRepository groupRepository;
    private GroupMessageRepository groupMessageRepository;
    private ResourceService resourceService;
    private ApplicationEventPublisher eventPublisher;

    public AccountRepository getAccountRepository() {
        return accountRepository;
    }

    @Autowired
    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public GroupRepository getGroupRepository() {
        return groupRepository;
    }

    @Autowired
    public void setGroupRepository(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    public GroupMessageRepository getGroupMessageRepository() {
        return groupMessageRepository;
    }

    @Autowired
    public void setGroupMessageRepository(GroupMessageRepository groupMessageRepository) {
        this.groupMessageRepository = groupMessageRepository;
    }

    public ResourceService getResourceService() {
        return resourceService;
    }

    @Autowired
    public void setResourceService(ResourceService resourceService) {
        this.resourceService = resourceService;
    }

    public ApplicationEventPublisher getEventPublisher() {
        return eventPublisher;
    }

    @Autowired
    public void setEventPublisher(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @PreAuthorize("hasPermission(#groupId, 'Group', 'READ_SEND_MESSAGES')")
    public GroupMessage getGroupMessage(long groupId, long authorId, long messageId) {
        log.debug("Going to find group message {} for group {} from author {}", messageId, groupId, authorId);
        return groupMessageRepository.findByIdAndGroupIdAndAuthorId(messageId, groupId, authorId).orElseThrow(() -> {
            log.warn("Not found group message {} for group {} from author {}", messageId, groupId, authorId);
            return new NotFoundEntityException("exception.notFound.groupMessage.id");
        });
    }

    @PreAuthorize("hasPermission(#groupId, 'Group', 'READ_SEND_MESSAGES')")
    public Page<MessageSummary> getGroupMessages(long groupId, Pageable pageRequest) {
        log.debug("Going to get group messages for group {} / {}", groupId, pageRequest);
        return groupMessageRepository.findByGroupId(groupId, pageRequest);
    }

    @Transactional
    @PreAuthorize("hasPermission(#groupId, 'Group', 'READ_SEND_MESSAGES') AND hasPermission(#authorId, 'Account', 'EDIT')")
    public GroupMessage sendGroupMessage(long groupId, long authorId, @Valid GroupMessage message, Resource image) {
        message.setId(null);
        if (groupRepository.existsById(groupId)) {
            message.setGroup(groupRepository.getOne(groupId));
        } else {
            log.warn("Not found group message recipient {}", groupId);
            throw new NotFoundEntityException("exception.notFound.groupMessage.group.id");
        }
        if (accountRepository.existsById(authorId)) {
            message.setAuthor(accountRepository.getOne(authorId));
        } else {
            log.warn("Not found group message author {}", authorId);
            throw new NotFoundEntityException("exception.notFound.groupMessage.author.id");
        }
        message.setImageId(null);
        if (image != null) {
            log.debug("Going to upload group message image");
            String imageId = resourceService.createResource(image);
            eventPublisher.publishEvent(new ResourceCreatedEvent(this, imageId));
            log.info("Group message image has been successfully uploaded {}", imageId);
            message.setImageId(imageId);
        }
        log.debug("Going to save group message for group {} from author {} {}", groupId, authorId, message);
        GroupMessage result = groupMessageRepository.saveAndFlush(message);
        eventPublisher.publishEvent(new BusinessEvent(this, format("Group '%s' / id '%d' public message id '%d' was sent by '%s %s' / id '%d'.",
                result.getGroup().getName(),
                result.getGroup().getId(),
                result.getId(),
                result.getAuthor().getFirstName(),
                result.getAuthor().getLastName(),
                result.getAuthor().getId()
        )));
        log.info("Group message for group {} from author {} has been successfully saved {}", groupId, authorId, result);
        return result;
    }

    @Transactional
    @PreAuthorize("hasPermission(#groupId, 'Group', 'EDIT') OR hasPermission(#authorId, 'Account', 'EDIT')")
    public void deleteGroupMessage(long groupId, long authorId, long messageId) {
        GroupMessage message = getGroupMessage(groupId, authorId, messageId);
        if (message.getImageId() != null) {
            eventPublisher.publishEvent(new ResourceDeletedEvent(this, message.getImageId()));
        }
        log.debug("Going to delete group message {} for group {} from author {}", messageId, groupId, authorId);
        eventPublisher.publishEvent(new BusinessEvent(this, format("Group '%s' / id '%d' public message id '%d' was deleted.",
                message.getGroup().getName(),
                message.getGroup().getId(),
                message.getId()
        )));
        groupMessageRepository.delete(message);
        groupMessageRepository.flush();
        log.info("Group message {} for group {} from author {} has been successfully deleted", messageId, groupId, authorId);
    }

}
