package com.getjavajob.training.matiivv.socialnetwork.service.service.resource;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.NotFoundEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

import java.io.IOException;
import java.io.InputStream;

import static com.amazonaws.services.s3.model.CannedAccessControlList.PublicRead;
import static java.util.UUID.randomUUID;
import static org.springframework.transaction.event.TransactionPhase.AFTER_COMMIT;
import static org.springframework.transaction.event.TransactionPhase.AFTER_ROLLBACK;

@Slf4j
@Service
public class ResourceService {

    private AmazonS3 s3client;
    private String bucket;

    public AmazonS3 getS3client() {
        return s3client;
    }

    @Autowired
    public void setS3client(AmazonS3 s3client) {
        this.s3client = s3client;
    }

    public String getBucket() {
        return bucket;
    }

    @Value("${spring.s3.bucket}")
    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public Resource getResource(String id) {
        try {
            log.debug("Going to get resource {} in bucket {}", id, bucket);
            S3Object s3Object = s3client.getObject(bucket, id);
            return new Resource() {
                @Override
                public String getId() {
                    return s3Object.getKey();
                }

                @Override
                public InputStream getInputStream() {
                    return s3Object.getObjectContent();
                }

                @Override
                public String getContentType() {
                    return s3Object.getObjectMetadata().getContentType();
                }

                @Override
                public long getContentLength() {
                    return s3Object.getObjectMetadata().getContentLength();
                }

                @Override
                public void close() throws IOException {
                    s3Object.close();
                }
            };
        } catch (AmazonS3Exception e) {
            throw new NotFoundEntityException(e, "exception.notFound.image.id");
        }
    }

    public String createResource(Resource resource) {
        try {
            var key = randomUUID().toString();
            var metadata = new ObjectMetadata();
            metadata.setContentLength(resource.getContentLength());
            metadata.setContentType(resource.getContentType());
            log.debug("Going to create resource {} in bucket {}", key, bucket);
            s3client.putObject(new PutObjectRequest(bucket, key, resource.getInputStream(), metadata)
                    .withCannedAcl(PublicRead));
            log.info("Resource {} has been successfully created", key);
            return key;
        } catch (IOException e) {
            throw new ServiceException(e, e.getMessage());
        }
    }

    public void deleteResource(String id) {
        try {
            log.debug("Going to delete resource {} in bucket {}", id, bucket);
            s3client.deleteObject(new DeleteObjectRequest(bucket, id));
            log.info("Resource {} in bucket {} has been successfully deleted", id, bucket);
        } catch (AmazonS3Exception e) {
            throw new NotFoundEntityException(e, "exception.notFound.image.id");
        }
    }

    @Service
    public static class EventListener {

        private ResourceService resourceService;

        public ResourceService getResourceService() {
            return resourceService;
        }

        @Autowired
        public void setResourceService(ResourceService resourceService) {
            this.resourceService = resourceService;
        }

        @TransactionalEventListener(phase = AFTER_ROLLBACK)
        public void onResourceCreatedRollback(ResourceCreatedEvent event) {
            resourceService.deleteResource(event.getId());
        }

        @TransactionalEventListener(phase = AFTER_COMMIT)
        public void onResourceDeletedCommit(ResourceDeletedEvent event) {
            resourceService.deleteResource(event.getId());
        }

    }

}
