package com.getjavajob.training.matiivv.socialnetwork.service.service.group;

import com.getjavajob.training.matiivv.socialnetwork.dao.group.GroupRepository;
import com.getjavajob.training.matiivv.socialnetwork.dao.group.MembershipRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Group;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Membership;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.group.GroupSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.event.BusinessEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.NotFoundEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.Resource;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceCreatedEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceDeletedEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status.ACCEPTED;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.Role.ADMIN;
import static java.lang.String.format;

@Slf4j
@Service
@Validated
@Transactional(readOnly = true)
public class GroupService {

    private GroupRepository groupRepository;
    private MembershipRepository membershipRepository;
    private ResourceService resourceService;
    private ApplicationEventPublisher eventPublisher;

    public GroupRepository getGroupRepository() {
        return groupRepository;
    }

    @Autowired
    public void setGroupRepository(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    public MembershipRepository getMembershipRepository() {
        return membershipRepository;
    }

    @Autowired
    public void setMembershipRepository(MembershipRepository membershipRepository) {
        this.membershipRepository = membershipRepository;
    }

    public ResourceService getResourceService() {
        return resourceService;
    }

    @Autowired
    public void setResourceService(ResourceService resourceService) {
        this.resourceService = resourceService;
    }

    public ApplicationEventPublisher getEventPublisher() {
        return eventPublisher;
    }

    @Autowired
    public void setEventPublisher(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @PreAuthorize("hasPermission(#groupId, 'Group', 'READ')")
    public Group getGroup(long groupId) {
        log.debug("Going to find group {}", groupId);
        return groupRepository.findById(groupId).orElseThrow(() -> {
            log.warn("Not found group {}", groupId);
            return new NotFoundEntityException("exception.notFound.group.id");
        });
    }

    @PreAuthorize("isAuthenticated()")
    public Page<GroupSummary> getGroups(String pattern, Pageable pageRequest) {
        log.debug("Going to get groups for pattern {} / {}", pattern, pageRequest);
        return groupRepository.findByNamePattern(pattern.trim(), pageRequest);
    }

    @Transactional
    @PreAuthorize("isAuthenticated()")
    public Group createGroup(@Valid Group group, Resource groupImage) {
        group.setId(null);
        group.setImageId(null);
        if (groupImage != null) {
            log.debug("Going to upload group {} image", group.getName());
            String imageId = resourceService.createResource(groupImage);
            eventPublisher.publishEvent(new ResourceCreatedEvent(this, imageId));
            log.info("Group {} image has been successfully uploaded {}", group.getName(), imageId);
            group.setImageId(imageId);
        }
        group.setDeleted(false);
        log.debug("Going to register group {}", group);
        Group result = groupRepository.saveAndFlush(group);
        log.info("Group has been successfully registered {}", result);
        var creator = new Membership(new Membership.Id(group.getId(), group.getCreator().getId()));
        creator.setGroup(result);
        creator.setMember(result.getCreator());
        creator.setRole(ADMIN);
        creator.setStatus(ACCEPTED);
        log.debug("Going to create group {} membership {} {}", creator.getGroup().getId(), creator.getMember().getId(), creator);
        membershipRepository.saveAndFlush(creator);
        eventPublisher.publishEvent(new BusinessEvent(this, format("Group '%s' / id '%d' was created.",
                result.getName(),
                result.getId()
        )));
        log.info("Group {} membership {} has been successfully created {}", creator.getGroup().getId(), creator.getMember().getId(), creator);
        return result;
    }

    @Transactional
    @PreAuthorize("hasPermission(#groupId, 'Group', 'EDIT')")
    public Group editGroupInfo(long groupId, @Valid Group groupInfo) {
        Group group = getGroup(groupId);
        group.setName(groupInfo.getName());
        group.setDescription(groupInfo.getDescription());
        log.debug("Going to edit group info {}", groupInfo);
        Group result = groupRepository.saveAndFlush(group);
        eventPublisher.publishEvent(new BusinessEvent(this, format("Group '%s' / id '%d' info was edited.",
                result.getName(),
                result.getId()
        )));
        log.info("Group info has been successfully edited {}", result);
        return result;
    }

    @Transactional
    @PreAuthorize("hasPermission(#groupId, 'Group', 'EDIT')")
    public Group editGroupImage(long groupId, Resource groupImage) {
        Group group = getGroup(groupId);
        if (group.getImageId() != null) {
            eventPublisher.publishEvent(new ResourceDeletedEvent(this, group.getImageId()));
            group.setImageId(null);
        }
        if (groupImage != null) {
            log.debug("Going to upload group {} image", groupId);
            String imageId = resourceService.createResource(groupImage);
            eventPublisher.publishEvent(new ResourceCreatedEvent(this, imageId));
            log.info("Group {} image has been successfully uploaded {}", groupId, imageId);
            group.setImageId(imageId);
        }
        log.debug("Going to edit group {} ImageId {}", groupId, group.getImageId());
        Group result = groupRepository.saveAndFlush(group);
        eventPublisher.publishEvent(new BusinessEvent(this, format("Group '%s' / id '%d' image was changed.",
                result.getName(),
                result.getId()
        )));
        log.info("Group ImageId has been successfully edited {}", result);
        return result;
    }

    @Transactional
    @PreAuthorize("hasPermission(#groupId, 'Group', 'EDIT')")
    public Group editGroupDeleted(long groupId, Group groupDeleted) {
        Group group = getGroup(groupId);
        group.setDeleted(groupDeleted.isDeleted());
        log.debug("Going to edit group deleted flag {}", groupDeleted);
        Group result = groupRepository.saveAndFlush(group);
        eventPublisher.publishEvent(new BusinessEvent(this, format("Group '%s' / id '%d' was %s.",
                result.getName(),
                result.getId(),
                result.isDeleted() ? "deleted" : "restored"
        )));
        log.info("Group deleted flag has been successfully edited {}", result);
        return result;
    }

}
