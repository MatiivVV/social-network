package com.getjavajob.training.matiivv.socialnetwork.service.security.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.stereotype.SecurityComponent;
import com.getjavajob.training.matiivv.socialnetwork.service.security.EntityPermissionEvaluator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;

import java.util.Objects;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.Role.ADMIN;
import static com.getjavajob.training.matiivv.socialnetwork.service.security.account.AccountPermission.USE_CHAT;

@Slf4j
@SecurityComponent
public class AccountPermissionEvaluator implements EntityPermissionEvaluator<Account, Long, AccountPermission> {

    @Override
    public Class<Account> getEntityType() {
        return Account.class;
    }

    @Override
    public Class<Long> getIdentityType() {
        return Long.class;
    }

    @Override
    public Class<AccountPermission> getPermissionType() {
        return AccountPermission.class;
    }

    @Override
    public boolean testPermissionById(Authentication authentication, Long accountId, AccountPermission permission) {
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            var user = (AccountUserDetails) authentication.getPrincipal();
            if (user.getRole() == ADMIN && permission != USE_CHAT) {
                log.debug("Granted account {} {} permission for user {}", accountId, permission, user.getId());
                return true;
            }
            switch (permission) {
                case READ:
                    log.debug("Granted account {} {} permission for user {}", accountId, permission, user.getId());
                    return true;
                case USE_CHAT:
                case EDIT:
                    if (Objects.equals(user.getId(), accountId)) {
                        log.debug("Granted account {} {} permission for user {}", accountId, permission, user.getId());
                        return true;
                    } else {
                        log.info("Denied account {} {} permission for user {}", accountId, permission, user.getId());
                        return false;
                    }
                case EDIT_ROLE:
                    if (user.getRole() == ADMIN) {
                        log.debug("Granted account {} {} permission for user {}", accountId, permission, user.getId());
                        return true;
                    } else {
                        log.info("Denied account {} {} permission for user {}", accountId, permission, user.getId());
                        return false;
                    }
                default:
                    log.error("Illegal account permission: {}", permission);
                    return false;
            }
        }
        log.info("Denied account {} {} permission for anonymous", accountId, permission);
        return false;
    }

}
