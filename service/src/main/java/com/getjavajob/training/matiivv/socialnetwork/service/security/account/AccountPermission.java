package com.getjavajob.training.matiivv.socialnetwork.service.security.account;

public enum AccountPermission {

    READ,
    EDIT,
    EDIT_ROLE,
    USE_CHAT

}
