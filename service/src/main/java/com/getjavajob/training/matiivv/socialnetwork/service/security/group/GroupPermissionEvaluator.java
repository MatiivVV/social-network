package com.getjavajob.training.matiivv.socialnetwork.service.security.group;

import com.getjavajob.training.matiivv.socialnetwork.dao.group.MembershipRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Group;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Membership;
import com.getjavajob.training.matiivv.socialnetwork.domain.stereotype.SecurityComponent;
import com.getjavajob.training.matiivv.socialnetwork.service.security.EntityPermissionEvaluator;
import com.getjavajob.training.matiivv.socialnetwork.service.security.account.AccountUserDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;

import java.util.Optional;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.Role.ADMIN;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Membership.Id;

@Slf4j
@SecurityComponent
public class GroupPermissionEvaluator implements EntityPermissionEvaluator<Group, Long, GroupPermission> {

    private MembershipRepository membershipRepository;

    public MembershipRepository getMembershipRepository() {
        return membershipRepository;
    }

    @Autowired
    public void setMembershipRepository(MembershipRepository membershipRepository) {
        this.membershipRepository = membershipRepository;
    }

    @Override
    public Class<Group> getEntityType() {
        return Group.class;
    }

    @Override
    public Class<Long> getIdentityType() {
        return Long.class;
    }

    @Override
    public Class<GroupPermission> getPermissionType() {
        return GroupPermission.class;
    }

    @Override
    public boolean testPermissionById(Authentication authentication, Long groupId, GroupPermission permission) {
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            var user = (AccountUserDetails) authentication.getPrincipal();
            if (user.getRole() == ADMIN) {
                log.debug("Granted group {} {} permission for user {}", groupId, permission, user.getId());
                return true;
            }
            Optional<Membership> membership;
            switch (permission) {
                case READ:
                    log.debug("Granted group {} {} permission for user {}", groupId, permission, user.getId());
                    return true;
                case READ_SEND_MESSAGES:
                    membership = membershipRepository.findById(new Id(groupId, user.getId()));
                    if (membership.isPresent()) {
                        log.debug("Granted group {} {} permission for user {}", groupId, permission, user.getId());
                        return true;
                    } else {
                        log.info("Denied group {} {} permission for user {}", groupId, permission, user.getId());
                        return false;
                    }
                case EDIT:
                    membership = membershipRepository.findById(new Id(groupId, user.getId()));
                    if (membership.isPresent() && membership.get().getRole() == ADMIN) {
                        log.debug("Granted group {} {} permission for user {}", groupId, permission, user.getId());
                        return true;
                    } else {
                        log.info("Denied group {} {} permission for user {}", groupId, permission, user.getId());
                        return false;
                    }
                default:
                    log.error("Illegal group permission: {}", permission);
                    return false;
            }
        }
        log.info("Denied group {} {} permission for anonymous", groupId, permission);
        return false;
    }

}
