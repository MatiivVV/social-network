package com.getjavajob.training.matiivv.socialnetwork.service.security.group;

public enum GroupPermission {

    READ,
    READ_SEND_MESSAGES,
    EDIT

}
