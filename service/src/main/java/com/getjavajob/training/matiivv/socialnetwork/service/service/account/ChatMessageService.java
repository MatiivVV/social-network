package com.getjavajob.training.matiivv.socialnetwork.service.service.account;

import com.getjavajob.training.matiivv.socialnetwork.dao.account.AccountRepository;
import com.getjavajob.training.matiivv.socialnetwork.dao.account.ChatMessageRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.ChatMessage;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.ChatMessageSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.event.BusinessEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.NotFoundEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.Resource;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceCreatedEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

import static java.lang.String.format;

@Slf4j
@Service
@Validated
@Transactional(readOnly = true)
public class ChatMessageService {

    private AccountRepository accountRepository;
    private ChatMessageRepository chatMessageRepository;
    private ChatService chatService;
    private ResourceService resourceService;
    private ApplicationEventPublisher eventPublisher;

    public AccountRepository getAccountRepository() {
        return accountRepository;
    }

    @Autowired
    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public ChatMessageRepository getChatMessageRepository() {
        return chatMessageRepository;
    }

    @Autowired
    public void setChatMessageRepository(ChatMessageRepository chatMessageRepository) {
        this.chatMessageRepository = chatMessageRepository;
    }

    public ChatService getChatService() {
        return chatService;
    }

    @Autowired
    public void setChatService(ChatService chatService) {
        this.chatService = chatService;
    }

    public ResourceService getResourceService() {
        return resourceService;
    }

    @Autowired
    public void setResourceService(ResourceService resourceService) {
        this.resourceService = resourceService;
    }

    public ApplicationEventPublisher getEventPublisher() {
        return eventPublisher;
    }

    @Autowired
    public void setEventPublisher(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @PreAuthorize("hasPermission(#recipientId, 'Account', 'USE_CHAT') OR hasPermission(#authorId, 'Account', 'USE_CHAT')")
    public ChatMessage getChatMessage(long recipientId, long authorId, long messageId) {
        log.debug("Going to find chat message {} for recipient {} from author {}", messageId, recipientId, authorId);
        return chatMessageRepository.findByIdAndRecipientIdAndAuthorId(messageId, recipientId, authorId).orElseThrow(() -> {
            log.warn("Not found chat message {} for recipient {} from author {}", messageId, recipientId, authorId);
            return new NotFoundEntityException("exception.notFound.chatMessage.id");
        });
    }

    @PreAuthorize("hasPermission(#accountId, 'Account', 'USE_CHAT')")
    public Page<ChatMessageSummary> getChatMessages(long accountId, long opponentId, Pageable pageRequest) {
        log.debug("Going to find chat messages for account {} and opponent {} / {}", accountId, opponentId, pageRequest);
        return chatMessageRepository.findByAccountIdAndOpponentId(accountId, opponentId, pageRequest);
    }

    @Transactional
    @PreAuthorize("hasPermission(#recipientId, 'Account', 'READ') AND hasPermission(#authorId, 'Account', 'USE_CHAT')")
    public ChatMessage sendChatMessage(long recipientId, long authorId, @Valid ChatMessage message,
                                       Resource image) {
        message.setId(null);
        if (accountRepository.existsById(recipientId)) {
            message.setRecipient(accountRepository.getOne(recipientId));
        } else {
            log.warn("Not found chat message recipient {}", recipientId);
            throw new NotFoundEntityException("exception.notFound.chatMessage.recipient.id");
        }
        if (accountRepository.existsById(authorId)) {
            message.setAuthor(accountRepository.getOne(authorId));
        } else {
            log.warn("Not found chat message author {}", authorId);
            throw new NotFoundEntityException("exception.notFound.chatMessage.author.id");
        }
        message.setImageId(null);
        if (image != null) {
            log.debug("Going to upload chat message image");
            String imageId = resourceService.createResource(image);
            eventPublisher.publishEvent(new ResourceCreatedEvent(this, imageId));
            log.info("Chat message image has been successfully uploaded {}", imageId);
            message.setImageId(imageId);
        }
        message.setRead(false);
        if (!chatService.chatExists(authorId, recipientId)) {
            chatService.createChat(authorId, recipientId);
        }
        if (!chatService.chatExists(recipientId, authorId)) {
            chatService.createChat(recipientId, authorId);
        }
        log.debug("Going to save chat message for recipient {} from author {} {}", recipientId, authorId, message);
        ChatMessage result = chatMessageRepository.saveAndFlush(message);
        eventPublisher.publishEvent(new BusinessEvent(this, format("Account '%s %s' / id '%d' sent chat message to '%s %s' / id '%d'.",
                result.getAuthor().getFirstName(),
                result.getAuthor().getLastName(),
                result.getAuthor().getId(),
                result.getRecipient().getFirstName(),
                result.getRecipient().getLastName(),
                result.getRecipient().getId()
        )));
        log.info("Chat message for recipient {} from author {} has been successfully saved {}", recipientId, authorId, result);
        return result;
    }

}
