package com.getjavajob.training.matiivv.socialnetwork.service.security.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.stereotype.SecurityComponent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.Role.valueOf;

@Slf4j
@SecurityComponent
public class AccountAuthenticationConverter implements UserAuthenticationConverter {

    @Override
    public Map<String, ?> convertUserAuthentication(Authentication authentication) {
        var response = new LinkedHashMap<String, Object>();
        response.put(Attribute.NAME.getKey(), authentication.getName());
        if (authentication.getPrincipal() instanceof AccountUserDetails) {
            var user = (AccountUserDetails) authentication.getPrincipal();
            response.put(Attribute.ID.getKey(), user.getId());
            response.put(Attribute.ROLE.getKey(), user.getRole().toString());
            response.put(Attribute.SESSION.getKey(), user.getSession());
        }
        return response;
    }

    @Override
    public Authentication extractAuthentication(Map<String, ?> map) {
        if (map.containsKey(Attribute.NAME.getKey())) {
            var account = new Account(((Integer) map.get(Attribute.ID.getKey())).longValue());
            account.setEmailAddress((String) map.get(Attribute.NAME.getKey()));
            account.setRole(valueOf((String) map.get(Attribute.ROLE.getKey())));
            account.setSession((String) map.get(Attribute.SESSION.getKey()));
            var principal = new AccountUserDetails(account);
            return new UsernamePasswordAuthenticationToken(principal, "N/A", principal.getAuthorities());
        }
        log.warn("Authentication token map {} doesn't contain key {}", map, Attribute.NAME.getKey());
        return null;
    }

    private enum Attribute {

        ID("user_id"),
        NAME("user_name"),
        ROLE("user_role"),
        SESSION("user_session");

        private final String key;

        private Attribute(String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }

    }
}
