package com.getjavajob.training.matiivv.socialnetwork.service.service.exception;

import lombok.Getter;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceResolvable;

import java.util.Locale;

@Getter
public class ServiceException extends RuntimeException implements MessageSourceResolvable {

    private final String[] codes;
    private final Object[] arguments;

    public ServiceException(String errorCode, Object... arguments) {
        this(null, errorCode, arguments);
    }

    public ServiceException(Throwable cause, String errorCode, Object... arguments) {
        super(errorCode, cause);
        codes = new String[]{errorCode};
        this.arguments = arguments.clone();
    }

    @Override
    public String getDefaultMessage() {
        return getMessage();
    }

    public String getLocalizedMessage(MessageSource messageSource, Locale locale) {
        return messageSource.getMessage(codes[0], arguments, getMessage(), locale);
    }

}
