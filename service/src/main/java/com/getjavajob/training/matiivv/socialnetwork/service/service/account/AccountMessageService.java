package com.getjavajob.training.matiivv.socialnetwork.service.service.account;

import com.getjavajob.training.matiivv.socialnetwork.dao.account.AccountMessageRepository;
import com.getjavajob.training.matiivv.socialnetwork.dao.account.AccountRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.AccountMessage;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.MessageSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.event.BusinessEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.NotFoundEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.Resource;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceCreatedEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceDeletedEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

import static java.lang.String.format;

@Slf4j
@Service
@Validated
@Transactional(readOnly = true)
public class AccountMessageService {

    private AccountRepository accountRepository;
    private AccountMessageRepository accountMessageRepository;
    private ResourceService resourceService;
    private ApplicationEventPublisher eventPublisher;

    public AccountRepository getAccountRepository() {
        return accountRepository;
    }

    @Autowired
    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public AccountMessageRepository getAccountMessageRepository() {
        return accountMessageRepository;
    }

    @Autowired
    public void setAccountMessageRepository(AccountMessageRepository accountMessageRepository) {
        this.accountMessageRepository = accountMessageRepository;
    }

    public ResourceService getResourceService() {
        return resourceService;
    }

    @Autowired
    public void setResourceService(ResourceService resourceService) {
        this.resourceService = resourceService;
    }

    public ApplicationEventPublisher getEventPublisher() {
        return eventPublisher;
    }

    @Autowired
    public void setEventPublisher(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @PreAuthorize("hasPermission(#accountId, 'Account', 'READ')")
    public AccountMessage getAccountMessage(long accountId, long authorId, long messageId) {
        log.debug("Going to find account message {} for account {} from author {}", messageId, accountId, authorId);
        return accountMessageRepository.findByIdAndAccountIdAndAuthorId(messageId, accountId, authorId).orElseThrow(() -> {
            log.warn("Not found account message {} for account {} from author {}", messageId, accountId, authorId);
            return new NotFoundEntityException("exception.notFound.accountMessage.id");
        });
    }

    @PreAuthorize("hasPermission(#accountId, 'Account', 'READ')")
    public Page<MessageSummary> getAccountMessages(long accountId, Pageable pageRequest) {
        log.debug("Going to find account messages for account {} / {}", accountId, pageRequest);
        return accountMessageRepository.findByAccountId(accountId, pageRequest);
    }

    @Transactional
    @PreAuthorize("hasPermission(#accountId, 'Account', 'READ') AND hasPermission(#authorId, 'Account', 'EDIT')")
    public AccountMessage sendAccountMessage(long accountId, long authorId, @Valid AccountMessage message,
                                             Resource image) {
        message.setId(null);
        if (accountRepository.existsById(accountId)) {
            message.setAccount(accountRepository.getOne(accountId));
        } else {
            log.warn("Not found account message recipient {}", accountId);
            throw new NotFoundEntityException("exception.notFound.accountMessage.account.id");
        }
        if (accountRepository.existsById(authorId)) {
            message.setAuthor(accountRepository.getOne(authorId));
        } else {
            log.warn("Not found account message author {}", authorId);
            throw new NotFoundEntityException("exception.notFound.accountMessage.author.id");
        }
        message.setImageId(null);
        if (image != null) {
            log.debug("Going to upload account message image");
            String imageId = resourceService.createResource(image);
            eventPublisher.publishEvent(new ResourceCreatedEvent(this, imageId));
            log.info("Account message image has been successfully uploaded {}", imageId);
            message.setImageId(imageId);
        }
        log.debug("Going to save account message for account {} from author {} {}", accountId, authorId, message);
        AccountMessage result = accountMessageRepository.saveAndFlush(message);
        eventPublisher.publishEvent(new BusinessEvent(this, format("Account '%s %s' / id '%d' public message id '%d' was sent by '%s %s' / id '%d'.",
                result.getAccount().getFirstName(),
                result.getAccount().getLastName(),
                result.getAccount().getId(),
                result.getId(),
                result.getAuthor().getFirstName(),
                result.getAuthor().getLastName(),
                result.getAuthor().getId()
        )));
        log.info("Account message for account {} from author {} has been successfully saved {}", accountId, authorId, result);
        return result;
    }

    @Transactional
    @PreAuthorize("hasPermission(#accountId, 'Account', 'EDIT') OR hasPermission(#authorId, 'Account', 'EDIT')")
    public void deleteAccountMessage(long accountId, long authorId, long messageId) {
        AccountMessage message = getAccountMessage(accountId, authorId, messageId);
        if (message.getImageId() != null) {
            eventPublisher.publishEvent(new ResourceDeletedEvent(this, message.getImageId()));
        }
        log.debug("Going to delete account message {} for account {} from author {}", messageId, accountId, authorId);
        eventPublisher.publishEvent(new BusinessEvent(this, format("Account '%s %s' / id '%d' public message id '%d' was deleted.",
                message.getAccount().getFirstName(),
                message.getAccount().getLastName(),
                message.getAccount().getId(),
                message.getId()
        )));
        accountMessageRepository.delete(message);
        accountMessageRepository.flush();
        log.info("Account message {} for account {} from author {} has been successfully deleted", messageId, accountId, authorId);
    }

}
