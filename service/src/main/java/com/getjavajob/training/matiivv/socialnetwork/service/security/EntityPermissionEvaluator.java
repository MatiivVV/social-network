package com.getjavajob.training.matiivv.socialnetwork.service.security;

import org.springframework.data.domain.Persistable;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;

import java.io.Serializable;

public interface EntityPermissionEvaluator<T extends Persistable<ID>, ID extends Serializable, P extends Enum<P>>
        extends PermissionEvaluator {

    Class<T> getEntityType();

    Class<ID> getIdentityType();

    Class<P> getPermissionType();

    @Override
    default boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
        try {
            T typedTargetDomainObject = getEntityType().cast(targetDomainObject);
            P typedPermission = Enum.valueOf(getPermissionType(), (String) permission);
            return testPermission(authentication, typedTargetDomainObject, typedPermission);
        } catch (IllegalArgumentException | ClassCastException e) {
            return false;
        }
    }

    default boolean testPermission(Authentication authentication, T object, P permission) {
        return testPermissionById(authentication, object.getId(), permission);
    }

    @Override
    default boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
        try {
            ID typedTargetId = getIdentityType().cast(targetId);
            P typedPermission = Enum.valueOf(getPermissionType(), (String) permission);
            return testPermissionById(authentication, typedTargetId, typedPermission);
        } catch (IllegalArgumentException | ClassCastException e) {
            return false;
        }
    }

    boolean testPermissionById(Authentication authentication, ID id, P permission);

}
