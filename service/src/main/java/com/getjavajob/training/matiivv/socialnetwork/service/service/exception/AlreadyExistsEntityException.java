package com.getjavajob.training.matiivv.socialnetwork.service.service.exception;

public class AlreadyExistsEntityException extends ServiceException {

    public AlreadyExistsEntityException(String errorCode, Object... arguments) {
        super(errorCode, arguments);
    }

    public AlreadyExistsEntityException(Throwable cause, String errorCode, Object... arguments) {
        super(cause, errorCode, arguments);
    }

}
