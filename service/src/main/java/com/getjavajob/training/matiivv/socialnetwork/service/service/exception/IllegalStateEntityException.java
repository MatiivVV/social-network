package com.getjavajob.training.matiivv.socialnetwork.service.service.exception;

public class IllegalStateEntityException extends ServiceException {

    public IllegalStateEntityException(String errorCode, Object... arguments) {
        super(errorCode, arguments);
    }

    public IllegalStateEntityException(Throwable cause, String errorCode, Object... arguments) {
        super(cause, errorCode, arguments);
    }

}
