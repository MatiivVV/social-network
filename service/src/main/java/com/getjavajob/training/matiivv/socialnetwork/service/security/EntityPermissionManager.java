package com.getjavajob.training.matiivv.socialnetwork.service.security;

import com.getjavajob.training.matiivv.socialnetwork.domain.stereotype.SecurityComponent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toMap;

@Slf4j
@SecurityComponent
public class EntityPermissionManager implements org.springframework.security.access.PermissionEvaluator {

    private Map<String, EntityPermissionEvaluator<?, ? extends Serializable, ?>> permissionEvaluators;

    public Map<String, EntityPermissionEvaluator<?, ? extends Serializable, ?>> getPermissionEvaluators() {
        return permissionEvaluators;
    }

    @Autowired
    public void setPermissionEvaluators(List<EntityPermissionEvaluator<?, ? extends Serializable, ?>> permissionEvaluators) {
        this.permissionEvaluators = permissionEvaluators.stream()
                .collect(toMap(e -> e.getEntityType().getSimpleName(), e -> e));
    }

    @Override
    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
        if (targetDomainObject == null || permission == null) {
            log.error("Illegal arguments: targetDomainObject {}; permission {}", targetDomainObject, permission);
            return false;
        }
        String key = targetDomainObject.getClass().getSimpleName();
        EntityPermissionEvaluator<?, ? extends Serializable, ?> permissionEvaluator = permissionEvaluators.get(key);
        if (permissionEvaluator != null) {
            return permissionEvaluator.hasPermission(authentication, targetDomainObject, permission);
        }
        log.error("Not fount permission evaluator for targetDomainObject class {}", key);
        return false;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
        if (targetId == null || targetType == null || permission == null) {
            log.error("Illegal arguments: targetId: {}; targetType {}; permission {}", targetId, targetType, permission);
            return false;
        }
        EntityPermissionEvaluator<?, ? extends Serializable, ?> permissionEvaluator = permissionEvaluators.get(targetType);
        if (permissionEvaluator != null) {
            return permissionEvaluator.hasPermission(authentication, targetId, targetType, permission);
        }
        log.error("Not fount permission evaluator for targetType {}", targetType);
        return false;
    }

}
