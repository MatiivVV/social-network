package com.getjavajob.training.matiivv.socialnetwork.service.service.event;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.context.ApplicationEvent;

@Value
@EqualsAndHashCode(callSuper = true)
public class BusinessEvent extends ApplicationEvent {

    private String description;

    public BusinessEvent(Object source, String description) {
        super(source);
        this.description = description;
    }

}
