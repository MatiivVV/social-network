package com.getjavajob.training.matiivv.socialnetwork.service.security.account;

import com.getjavajob.training.matiivv.socialnetwork.dao.account.AccountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Primary
@Service
@Transactional(readOnly = true)
public class AccountUserDetailsService implements UserDetailsService {

    private AccountRepository accountRepository;

    public AccountRepository getAccountRepository() {
        return accountRepository;
    }

    @Autowired
    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public AccountUserDetails loadUserByUsername(String username) {
        log.debug("Going to find account {}", username);
        return accountRepository.findByEmailAddress(username)
                .map(AccountUserDetails::new)
                .orElseThrow(() -> {
                    log.warn("Not found account {}", username);
                    return new UsernameNotFoundException("exception.notFound.account.email");
                });
    }

}
