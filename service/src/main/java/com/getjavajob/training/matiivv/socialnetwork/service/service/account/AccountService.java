package com.getjavajob.training.matiivv.socialnetwork.service.service.account;

import com.getjavajob.training.matiivv.socialnetwork.dao.account.AccountRepository;
import com.getjavajob.training.matiivv.socialnetwork.dao.account.ChatMessageRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.*;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.AccountSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.event.BusinessEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.IllegalStateEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.NotFoundEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.Resource;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceCreatedEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceDeletedEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.Role.USER;
import static java.lang.String.format;
import static java.util.UUID.randomUUID;

@Slf4j
@Service
@Validated
@Transactional(readOnly = true)
public class AccountService {

    private AccountRepository accountRepository;
    private ChatMessageRepository chatMessageRepository;
    private PasswordEncoder passwordEncoder;
    private ResourceService resourceService;
    private ApplicationEventPublisher eventPublisher;

    public AccountRepository getAccountRepository() {
        return accountRepository;
    }

    @Autowired
    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public ChatMessageRepository getChatMessageRepository() {
        return chatMessageRepository;
    }

    @Autowired
    public void setChatMessageRepository(ChatMessageRepository chatMessageRepository) {
        this.chatMessageRepository = chatMessageRepository;
    }

    public PasswordEncoder getPasswordEncoder() {
        return passwordEncoder;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public ResourceService getResourceService() {
        return resourceService;
    }

    @Autowired
    public void setResourceService(ResourceService resourceService) {
        this.resourceService = resourceService;
    }

    public ApplicationEventPublisher getEventPublisher() {
        return eventPublisher;
    }

    @Autowired
    public void setEventPublisher(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @PreAuthorize("hasPermission(#accountId, 'Account', 'READ')")
    public Account getAccount(long accountId) {
        var account = getAccountLazily(accountId);
        log.debug("Going to find account {} phones", accountId);
        //noinspection ResultOfMethodCallIgnored
        account.getPhones().size();
        log.debug("Going to find account {} new message count", accountId);
        account.setNewMessageCount(chatMessageRepository.countByRecipientIdAndReadFalse(accountId));
        return account;
    }

    private Account getAccountLazily(long accountId) {
        log.debug("Going to find account {}", accountId);
        return accountRepository.findById(accountId).orElseThrow(() -> {
            log.warn("Not found account {}", accountId);
            return new NotFoundEntityException("exception.notFound.account.id");
        });
    }

    @PreAuthorize("isAuthenticated()")
    public Page<AccountSummary> getAccounts(String pattern, Pageable pageRequest) {
        log.debug("Going to find accounts for pattern {} / {}", pattern, pageRequest);
        return accountRepository.findByNamePattern(pattern.trim(), pageRequest);
    }

    @Transactional
    @Validated(ValidRegistration.class)
    public Account registerAccount(@Valid Account account, Resource accountImage) {
        account.setId(null);
        account.setPassword(passwordEncoder.encode(account.getRawPassword()));
        account.setRawPassword(null);
        account.setRole(USER);
        account.setImageId(null);
        if (accountImage != null) {
            log.debug("Going to upload account {} image", account.getEmailAddress());
            String imageId = resourceService.createResource(accountImage);
            eventPublisher.publishEvent(new ResourceCreatedEvent(this, imageId));
            log.info("Account {} image has been successfully uploaded {}", account.getEmailAddress(), imageId);
            account.setImageId(imageId);
        }
        account.setSession(randomUUID().toString());
        account.setDeleted(false);
        log.debug("Going to register account {}", account);
        Account result = accountRepository.saveAndFlush(account);
        log.info("Account has been successfully registered {}", result);
        eventPublisher.publishEvent(new BusinessEvent(this, format("Account '%s %s' / id '%d' was registered.",
                result.getFirstName(),
                result.getLastName(),
                result.getId()
        )));
        return result;
    }

    @Transactional
    @Validated(ValidInfo.class)
    @PreAuthorize("hasPermission(#accountId, 'Account', 'EDIT')")
    public Account editAccountInfo(long accountId, @Valid Account accountInfo) {
        Account account = getAccountLazily(accountId);
        account.setEmailAddress(accountInfo.getEmailAddress());
        account.setFirstName(accountInfo.getFirstName());
        account.setLastName(accountInfo.getLastName());
        account.setPaternalName(accountInfo.getPaternalName());
        account.setBirthday(accountInfo.getBirthday());
        account.setPersonalAddress(accountInfo.getPersonalAddress());
        account.setBusinessAddress(accountInfo.getBusinessAddress());
        account.setPhones(accountInfo.getPhones());
        account.setSkype(accountInfo.getSkype());
        account.setAdditionalInfo(accountInfo.getAdditionalInfo());
        account.setLocale(accountInfo.getLocale());
        log.debug("Going to edit account info {}", accountInfo);
        Account result = accountRepository.saveAndFlush(account);
        eventPublisher.publishEvent(new BusinessEvent(this, format("Account '%s %s' / id '%d' info was edited.",
                result.getFirstName(),
                result.getLastName(),
                result.getId()
        )));
        log.info("Account info has been successfully edited {}", result);
        return result;
    }

    @Transactional
    @Validated({ValidOldPassword.class, ValidPassword.class})
    @PreAuthorize("hasPermission(#accountId, 'Account', 'EDIT')")
    public Account editAccountPassword(long accountId, @Valid Account accountPassword) {
        Account account = getAccountLazily(accountId);
        if (!passwordEncoder.matches(accountPassword.getOldPassword(), account.getPassword())) {
            throw new IllegalStateEntityException("exception.illegalState.account.oldPassword");
        }
        account.setPassword(passwordEncoder.encode(accountPassword.getRawPassword()));
        accountPassword.setOldPassword(null);
        accountPassword.setRawPassword(null);
        log.debug("Going to edit account {} password", accountId);
        Account result = accountRepository.saveAndFlush(account);
        eventPublisher.publishEvent(new BusinessEvent(this, format("Account '%s %s' / id '%d' password was changed.",
                result.getFirstName(),
                result.getLastName(),
                result.getId()
        )));
        log.info("Account {} password has been successfully edited", accountId);
        return result;
    }

    @Transactional
    @Validated(ValidRole.class)
    @PreAuthorize("hasPermission(#accountId, 'Account', 'EDIT_ROLE')")
    public Account editAccountRole(long accountId, @Valid Account accountRole) {
        Account account = getAccountLazily(accountId);
        account.setRole(accountRole.getRole());
        log.debug("Going to edit account role {}", accountRole);
        Account result = accountRepository.saveAndFlush(account);
        eventPublisher.publishEvent(new BusinessEvent(this, format("Account '%s %s' / id '%d' role was changed to '%s'.",
                result.getFirstName(),
                result.getLastName(),
                result.getId(),
                result.getRole().toString().toLowerCase()
        )));
        log.info("Account role has been successfully edited {}", result);
        return result;
    }

    @Transactional
    @PreAuthorize("hasPermission(#accountId, 'Account', 'EDIT')")
    public Account editAccountImage(long accountId, Resource accountImage) {
        Account account = getAccountLazily(accountId);
        if (account.getImageId() != null) {
            eventPublisher.publishEvent(new ResourceDeletedEvent(this, account.getImageId()));
            account.setImageId(null);
        }
        if (accountImage != null) {
            log.debug("Going to upload account {} image", accountId);
            String imageId = resourceService.createResource(accountImage);
            eventPublisher.publishEvent(new ResourceCreatedEvent(this, imageId));
            log.info("Account {} image has been successfully uploaded {}", accountId, imageId);
            account.setImageId(imageId);
        }
        log.debug("Going to edit account {} ImageId {}", accountId, account.getImageId());
        Account result = accountRepository.saveAndFlush(account);
        eventPublisher.publishEvent(new BusinessEvent(this, format("Account '%s %s' / id '%d' image was changed.",
                result.getFirstName(),
                result.getLastName(),
                result.getId()
        )));
        log.info("Account ImageId has been successfully edited {}", result);
        return result;
    }

    @Transactional
    @PreAuthorize("hasPermission(#accountId, 'Account', 'EDIT')")
    public void resetAccountSession(long accountId) {
        Account account = getAccountLazily(accountId);
        account.setSession(randomUUID().toString());
        log.debug("Going to reset account {} session", accountId);
        Account result = accountRepository.saveAndFlush(account);
        eventPublisher.publishEvent(new BusinessEvent(this, format("Account '%s %s' / id '%d' session was reset.",
                result.getFirstName(),
                result.getLastName(),
                result.getId()
        )));
        log.info("Account {} session has been successfully reset", result);
    }

    @Transactional
    @PreAuthorize("hasPermission(#accountId, 'Account', 'EDIT')")
    public Account editAccountDeleted(long accountId, Account accountDeleted) {
        Account account = getAccountLazily(accountId);
        account.setDeleted(accountDeleted.isDeleted());
        log.debug("Going to edit account deleted flag {}", accountDeleted);
        Account result = accountRepository.saveAndFlush(account);
        eventPublisher.publishEvent(new BusinessEvent(this, format("Account '%s %s' / id '%d' was %s.",
                result.getFirstName(),
                result.getLastName(),
                result.getId(),
                result.isDeleted() ? "deleted" : "restored"
        )));
        log.info("Account deleted flag has been successfully edited {}", result);
        return result;
    }

}
