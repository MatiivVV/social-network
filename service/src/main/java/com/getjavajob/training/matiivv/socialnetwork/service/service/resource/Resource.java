package com.getjavajob.training.matiivv.socialnetwork.service.service.resource;

import java.io.IOException;
import java.io.InputStream;

public interface Resource extends AutoCloseable {

    String getId();

    InputStream getInputStream() throws IOException;

    String getContentType();

    long getContentLength();

}
