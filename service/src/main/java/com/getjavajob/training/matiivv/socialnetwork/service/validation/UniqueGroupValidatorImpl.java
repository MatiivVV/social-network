package com.getjavajob.training.matiivv.socialnetwork.service.validation;

import com.getjavajob.training.matiivv.socialnetwork.dao.group.GroupRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.constraint.UniqueGroupValidator;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Group;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Group_;
import com.getjavajob.training.matiivv.socialnetwork.domain.stereotype.ValidationComponent;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidatorContext;

@ValidationComponent
public class UniqueGroupValidatorImpl implements UniqueGroupValidator {

    private GroupRepository groupRepository;

    public GroupRepository getGroupRepository() {
        return groupRepository;
    }

    @Autowired
    public void setGroupRepository(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    @Override
    public boolean isValid(Group group, ConstraintValidatorContext context) {
        if (group == null || group.getName() == null) {
            return true;
        }

        boolean duplicateExists;
        if (group.getId() == null) {
            duplicateExists = groupRepository.existsByName(group.getName());
        } else {
            duplicateExists = groupRepository.existsByNameAndIdNot(group.getName(), group.getId());
        }

        if (duplicateExists) {
            context.disableDefaultConstraintViolation();
            String messageTemplate = context.getDefaultConstraintMessageTemplate();
            context.buildConstraintViolationWithTemplate(messageTemplate)
                    .addPropertyNode(Group_.NAME)
                    .addConstraintViolation();
            return false;
        }
        return true;
    }

}
