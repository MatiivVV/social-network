package com.getjavajob.training.matiivv.socialnetwork.service.service.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

import static org.springframework.transaction.event.TransactionPhase.AFTER_COMMIT;

@Slf4j
@Profile("development")
@Service
public class BusinessEventService {

    private JmsTemplate jmsTemplate;

    public JmsTemplate getJmsTemplate() {
        return jmsTemplate;
    }

    @Autowired
    public void setJmsTemplate(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @TransactionalEventListener(phase = AFTER_COMMIT)
    public void onBusinessEvent(BusinessEvent businessEvent) {
        log.debug("Going to send business event '{}' to queue", businessEvent.getDescription());
        jmsTemplate.send(session -> session.createTextMessage(businessEvent.getDescription()));
        log.info("Business event '{}' has been successfully sent to queue", businessEvent.getDescription());
    }

}
