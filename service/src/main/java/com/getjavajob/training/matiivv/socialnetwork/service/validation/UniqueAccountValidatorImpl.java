package com.getjavajob.training.matiivv.socialnetwork.service.validation;

import com.getjavajob.training.matiivv.socialnetwork.dao.account.AccountRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.constraint.UniqueAccountValidator;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account_;
import com.getjavajob.training.matiivv.socialnetwork.domain.stereotype.ValidationComponent;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidatorContext;

@ValidationComponent
public class UniqueAccountValidatorImpl implements UniqueAccountValidator {

    private AccountRepository accountRepository;

    public AccountRepository getAccountRepository() {
        return accountRepository;
    }

    @Autowired
    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public boolean isValid(Account account, ConstraintValidatorContext context) {
        if (account == null || account.getEmailAddress() == null) {
            return true;
        }

        boolean duplicateExists;
        if (account.getId() == null) {
            duplicateExists = accountRepository.existsByEmailAddress(account.getEmailAddress());
        } else {
            duplicateExists = accountRepository.existsByEmailAddressAndIdNot(account.getEmailAddress(), account.getId());
        }

        if (duplicateExists) {
            context.disableDefaultConstraintViolation();
            String messageTemplate = context.getDefaultConstraintMessageTemplate();
            context.buildConstraintViolationWithTemplate(messageTemplate)
                    .addPropertyNode(Account_.EMAIL_ADDRESS)
                    .addConstraintViolation();
            return false;
        }
        return true;
    }

}
