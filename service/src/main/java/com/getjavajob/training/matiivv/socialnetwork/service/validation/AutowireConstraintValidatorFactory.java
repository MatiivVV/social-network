package com.getjavajob.training.matiivv.socialnetwork.service.validation;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorFactory;

public class AutowireConstraintValidatorFactory implements ConstraintValidatorFactory {

    private AutowireCapableBeanFactory beanFactory;

    public AutowireCapableBeanFactory getBeanFactory() {
        return beanFactory;
    }

    @Autowired
    public void setBeanFactory(AutowireCapableBeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    @Override
    public <T extends ConstraintValidator<?, ?>> T getInstance(Class<T> key) {
        try {
            return beanFactory.getBean(key);
        } catch (BeansException exc) {
            return beanFactory.createBean(key);
        }
    }

    @Override
    public void releaseInstance(ConstraintValidator<?, ?> instance) {
        beanFactory.destroyBean(instance);
    }

}
