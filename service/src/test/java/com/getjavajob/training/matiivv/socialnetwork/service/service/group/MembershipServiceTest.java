package com.getjavajob.training.matiivv.socialnetwork.service.service.group;

import com.getjavajob.training.matiivv.socialnetwork.dao.account.AccountRepository;
import com.getjavajob.training.matiivv.socialnetwork.dao.group.GroupRepository;
import com.getjavajob.training.matiivv.socialnetwork.dao.group.MembershipRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Group;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Membership;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.group.AccountMembershipSummary;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.group.GroupMembershipSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.event.BusinessEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.AlreadyExistsEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.IllegalStateEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.NotFoundEntityException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.EnumSet;
import java.util.List;
import java.util.Optional;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status.*;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.Role.ADMIN;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.Role.USER;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Membership.Id;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class MembershipServiceTest {

    @SpyBean
    private MembershipService membershipService;
    @MockBean
    private AccountRepository accountRepository;
    @MockBean
    private GroupRepository groupRepository;
    @MockBean
    private MembershipRepository membershipRepository;
    @MockBean
    private ApplicationEventPublisher eventPublisher;
    private Object[] mocks;
    private Group group = new Group(1L);
    private Account member = new Account(1L);
    private Membership membership = new Membership(new Id(1L, 1L));

    @BeforeEach
    public void setUp() {
        membershipService.setEventPublisher(eventPublisher);
        membership.setGroup(group);
        membership.setMember(member);
        when(membershipRepository.findById(membership.getId())).thenReturn(Optional.of(membership));
        when(groupRepository.existsById(group.getId())).thenReturn(true);
        when(accountRepository.existsById(member.getId())).thenReturn(true);
        when(groupRepository.getOne(group.getId())).thenReturn(group);
        when(accountRepository.getOne(member.getId())).thenReturn(member);
        mocks = new Object[]{accountRepository,
                groupRepository,
                membershipRepository,
                eventPublisher};
    }

    @Test
    public void testGetMembership() {
        assertSame(membership, membershipService.getMembership(group.getId(), member.getId()));
        verify(membershipRepository).findById(eq(membership.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testGetMembershipNotFound() {
        var missingMemberId = 99L;
        var missingMembershipId = new Id(group.getId(), missingMemberId);
        when(membershipRepository.findById(missingMembershipId)).thenReturn(Optional.empty());
        assertThrows(NotFoundEntityException.class, () -> membershipService.getMembership(group.getId(), missingMemberId));
        verify(membershipRepository).findById(eq(missingMembershipId));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testGetGroupMemberships() {
        var roles = EnumSet.of(USER, ADMIN);
        var statuses = EnumSet.of(ACCEPTED, PENDING);
        Pageable pageRequest = PageRequest.of(1, 1);
        var result = new GroupMembershipSummary(1L, null, null, null, null, null, false, null, null, null);
        var page = new PageImpl<>(List.of(result), pageRequest, 1L);
        when(membershipRepository.searchForGroup(group.getId(), roles, statuses, pageRequest)).thenReturn(page);
        assertSame(page, membershipService.getGroupMemberships(group.getId(), roles, statuses, pageRequest));
        verify(membershipRepository).searchForGroup(eq(group.getId()), eq(roles), eq(statuses), eq(pageRequest));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testGetGroupMembers() {
        var roles = EnumSet.of(USER, ADMIN);
        var statuses = EnumSet.of(ACCEPTED);
        Pageable pageRequest = PageRequest.of(1, 1);
        var result = new GroupMembershipSummary(1L, null, null, null, null, null, false, null, null, null);
        var page = new PageImpl<>(List.of(result), pageRequest, 1L);
        when(membershipRepository.searchForGroup(group.getId(), roles, statuses, pageRequest)).thenReturn(page);
        assertSame(page, membershipService.getGroupMembers(group.getId(), roles, pageRequest));
        verify(membershipRepository).searchForGroup(eq(group.getId()), eq(roles), eq(statuses), eq(pageRequest));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testGetAccountMemberships() {
        var roles = EnumSet.of(USER, ADMIN);
        var statuses = EnumSet.of(ACCEPTED, PENDING);
        Pageable pageRequest = PageRequest.of(1, 1);
        var result = new AccountMembershipSummary(1L, null, null, null, false, null, null, null);
        var page = new PageImpl<>(List.of(result), pageRequest, 1L);
        when(membershipRepository.searchForAccount(member.getId(), roles, statuses, pageRequest)).thenReturn(page);
        assertSame(page, membershipService.getAccountMemberships(member.getId(), roles, statuses, pageRequest));
        verify(membershipRepository).searchForAccount(eq(member.getId()), eq(roles), eq(statuses), eq(pageRequest));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testGetAccountGroups() {
        var roles = EnumSet.of(USER, ADMIN);
        var statuses = EnumSet.of(ACCEPTED);
        Pageable pageRequest = PageRequest.of(1, 1);
        var result = new AccountMembershipSummary(1L, null, null, null, false, null, null, null);
        var page = new PageImpl<>(List.of(result), pageRequest, 1L);
        when(membershipRepository.searchForAccount(member.getId(), roles, statuses, pageRequest)).thenReturn(page);
        assertSame(page, membershipService.getAccountGroups(member.getId(), roles, pageRequest));
        verify(membershipRepository).searchForAccount(eq(member.getId()), eq(roles), eq(statuses), eq(pageRequest));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testCreateMembership() {
        when(membershipRepository.saveAndFlush(any(Membership.class))).thenReturn(membership);
        assertSame(membership, membershipService.createMembership(group.getId(), member.getId()));
        verify(groupRepository).existsById(eq(group.getId()));
        verify(accountRepository).existsById(eq(member.getId()));
        verify(groupRepository).getOne(eq(group.getId()));
        verify(accountRepository).getOne(eq(member.getId()));
        verify(membershipRepository).saveAndFlush(any(Membership.class));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testCreateMembershipAlreadyExists() {
        when(membershipRepository.saveAndFlush(any(Membership.class))).thenThrow(DataIntegrityViolationException.class);
        assertThrows(AlreadyExistsEntityException.class, () -> membershipService.createMembership(group.getId(), member.getId()));
        verify(groupRepository).existsById(eq(group.getId()));
        verify(accountRepository).existsById(eq(member.getId()));
        verify(groupRepository).getOne(eq(group.getId()));
        verify(accountRepository).getOne(eq(member.getId()));
        verify(membershipRepository).saveAndFlush(any(Membership.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testCreateMembershipNotFoundMember() {
        when(accountRepository.existsById(member.getId())).thenReturn(false);
        assertThrows(NotFoundEntityException.class, () -> membershipService.createMembership(group.getId(), member.getId()));
        verify(groupRepository).existsById(eq(group.getId()));
        verify(accountRepository).existsById(eq(member.getId()));
        verify(groupRepository).getOne(eq(group.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testCreateMembershipNotFoundGroup() {
        when(groupRepository.existsById(group.getId())).thenReturn(false);
        assertThrows(NotFoundEntityException.class, () -> membershipService.createMembership(group.getId(), member.getId()));
        verify(groupRepository).existsById(eq(group.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testEditMembershipRoleAccepted() {
        membership.setRole(USER);
        membership.setStatus(ACCEPTED);
        var membershipRole = new Membership();
        membershipRole.setRole(ADMIN);
        doReturn(membership).when(membershipService).getMembership(group.getId(), member.getId());
        when(membershipRepository.save(membership)).thenReturn(membership);
        assertAll(
                () -> assertSame(membership, membershipService
                        .editMembershipRole(group.getId(), member.getId(), membershipRole)),
                () -> assertEquals(ADMIN, membership.getRole())
        );
        verify(membershipService).getMembership(eq(group.getId()), eq(member.getId()));
        verify(membershipRepository).save(same(membership));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testEditMembershipRoleNotAccepted() {
        membership.setRole(USER);
        membership.setStatus(PENDING);
        var membershipRole = new Membership();
        membershipRole.setRole(ADMIN);
        doReturn(membership).when(membershipService).getMembership(group.getId(), member.getId());
        assertThrows(NotFoundEntityException.class, () ->
                membershipService.editMembershipRole(group.getId(), member.getId(), membershipRole));
        verify(membershipService).getMembership(eq(group.getId()), eq(member.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testEditMembershipStatusPending() {
        membership.setRole(USER);
        membership.setStatus(PENDING);
        var membershipStatus = new Membership();
        membershipStatus.setStatus(ACCEPTED);
        doReturn(membership).when(membershipService).getMembership(group.getId(), member.getId());
        when(membershipRepository.save(membership)).thenReturn(membership);
        assertAll(
                () -> assertSame(membership, membershipService
                        .editMembershipStatus(group.getId(), member.getId(), membershipStatus)),
                () -> assertEquals(ACCEPTED, membership.getStatus())
        );
        verify(membershipService).getMembership(eq(group.getId()), eq(member.getId()));
        verify(membershipRepository).save(same(membership));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testEditMembershipStatusDeclinedToPending() {
        membership.setRole(USER);
        membership.setStatus(DECLINED);
        var membershipStatus = new Membership();
        membershipStatus.setStatus(PENDING);
        doReturn(membership).when(membershipService).getMembership(group.getId(), member.getId());
        when(membershipRepository.save(membership)).thenReturn(membership);
        assertAll(
                () -> assertSame(membership, membershipService
                        .editMembershipStatus(group.getId(), member.getId(), membershipStatus)),
                () -> assertEquals(PENDING, membership.getStatus())
        );
        verify(membershipService).getMembership(eq(group.getId()), eq(member.getId()));
        verify(membershipRepository).save(same(membership));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testEditMembershipStatusNotPendingAndNotDeclinedToPending() {
        membership.setRole(USER);
        membership.setStatus(ACCEPTED);
        var membershipStatus = new Membership();
        membershipStatus.setStatus(PENDING);
        doReturn(membership).when(membershipService).getMembership(group.getId(), member.getId());
        assertThrows(IllegalStateEntityException.class, () ->
                membershipService.editMembershipStatus(group.getId(), member.getId(), membershipStatus));
        verify(membershipService).getMembership(eq(group.getId()), eq(member.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testDeleteMembership() {
        doReturn(membership).when(membershipService).getMembership(group.getId(), member.getId());
        membershipService.deleteMembership(group.getId(), member.getId());
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verify(membershipRepository).deleteById(eq(membership.getId()));
    }

    @Test
    public void testDeleteMembershipNotFound() {
        doThrow(EmptyResultDataAccessException.class).when(membershipRepository).deleteById(membership.getId());
        assertThrows(NotFoundEntityException.class, () ->
                membershipService.deleteMembership(group.getId(), member.getId()));
        verify(membershipRepository).deleteById(eq(membership.getId()));
    }

    @TestConfiguration
    static class MembershipServiceTestConfiguration {

        @Bean
        public MembershipService membershipService() {
            return new MembershipService();
        }

    }

}
