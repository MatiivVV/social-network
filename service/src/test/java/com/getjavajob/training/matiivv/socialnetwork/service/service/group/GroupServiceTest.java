package com.getjavajob.training.matiivv.socialnetwork.service.service.group;

import com.getjavajob.training.matiivv.socialnetwork.dao.group.GroupRepository;
import com.getjavajob.training.matiivv.socialnetwork.dao.group.MembershipRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Group;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Membership;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.group.GroupSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.event.BusinessEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.NotFoundEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.Resource;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceCreatedEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceDeletedEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class GroupServiceTest {

    @Autowired
    private GroupService groupService;
    @MockBean
    private GroupRepository groupRepository;
    @MockBean
    private MembershipRepository membershipRepository;
    @MockBean
    private ResourceService resourceService;
    @MockBean
    private ApplicationEventPublisher eventPublisher;
    private Object[] mocks;
    private Group group = spy(new Group(1L));

    @BeforeEach
    public void setUp() {
        groupService.setEventPublisher(eventPublisher);
        when(groupRepository.findById(group.getId())).thenReturn(Optional.of(group));
        when(groupRepository.saveAndFlush(same(group))).thenReturn(group);
        mocks = new Object[]{groupRepository,
                membershipRepository,
                resourceService,
                eventPublisher};
    }

    @Test
    public void testGetGroup() {
        assertSame(group, groupService.getGroup(group.getId()));
        verify(groupRepository).findById(eq(group.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testGetAccountNotFound() {
        var missingAccountId = 99L;
        when(groupRepository.findById(missingAccountId)).thenReturn(Optional.empty());
        assertThrows(NotFoundEntityException.class, () -> groupService.getGroup(missingAccountId));
        verify(groupRepository).findById(eq(missingAccountId));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testGetGroups() {
        var pattern = "  pattern  ";
        Pageable pageRequest = PageRequest.of(1, 1);
        var result = new GroupSummary(1L, null, null, null);
        var page = new PageImpl<>(List.of(result), pageRequest, 1L);
        when(groupRepository.findByNamePattern(pattern.trim(), pageRequest)).thenReturn(page);
        assertSame(page, groupService.getGroups(pattern, pageRequest));
        verify(groupRepository).findByNamePattern(eq(pattern.trim()), eq(pageRequest));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testCreateGroup() {
        var imageId = "imageId";
        group.setCreator(new Account(1L));
        when(resourceService.createResource(any(Resource.class))).thenReturn(imageId);
        when(membershipRepository.saveAndFlush(any(Membership.class))).thenReturn(mock(Membership.class));
        assertAll(
                () -> assertSame(group, groupService.createGroup(group, mock(Resource.class))),
                () -> assertEquals(imageId, group.getImageId()),
                () -> assertFalse(group.isDeleted())
        );
        verify(resourceService).createResource(any(Resource.class));
        verify(eventPublisher).publishEvent(any(ResourceCreatedEvent.class));
        verify(groupRepository).saveAndFlush(same(group));
        verify(membershipRepository).saveAndFlush(any(Membership.class));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testEditGroupInfo() {
        var groupInfo = new Group();
        assertSame(group, groupService.editGroupInfo(group.getId(), groupInfo));
        verify(groupRepository).findById(eq(group.getId()));
        verify(group, never()).setDeleted(anyBoolean());
        verify(groupRepository).saveAndFlush(same(group));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testEditGroupImageCreate() {
        var imageId = "imageId";
        when(resourceService.createResource(any(Resource.class))).thenReturn(imageId);
        assertAll(
                () -> assertSame(group, groupService.editGroupImage(group.getId(), mock(Resource.class))),
                () -> assertEquals(imageId, group.getImageId())
        );
        verify(groupRepository).findById(eq(group.getId()));
        verify(resourceService).createResource(any(Resource.class));
        verify(eventPublisher).publishEvent(any(ResourceCreatedEvent.class));
        verify(groupRepository).saveAndFlush(same(group));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testEditAccountImageReplace() {
        group.setImageId("oldImageId");
        var imageId = "imageId";
        when(resourceService.createResource(any(Resource.class))).thenReturn(imageId);
        assertAll(
                () -> assertSame(group, groupService.editGroupImage(group.getId(), mock(Resource.class))),
                () -> assertEquals(imageId, group.getImageId())
        );
        verify(groupRepository).findById(eq(group.getId()));
        verify(resourceService).createResource(any(Resource.class));
        verify(eventPublisher).publishEvent(any(ResourceDeletedEvent.class));
        verify(eventPublisher).publishEvent(any(ResourceCreatedEvent.class));
        verify(groupRepository).saveAndFlush(same(group));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testEditAccountImageDelete() {
        group.setImageId("oldImageId");
        assertAll(
                () -> assertSame(group, groupService.editGroupImage(group.getId(), null)),
                () -> assertNull(group.getImageId())
        );
        verify(groupRepository).findById(eq(group.getId()));
        verify(eventPublisher).publishEvent(any(ResourceDeletedEvent.class));
        verify(groupRepository).saveAndFlush(same(group));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testEditAccountDeleted() {
        var groupDeleted = new Group();
        groupDeleted.setDeleted(true);
        assertAll(
                () -> assertSame(group, groupService.editGroupDeleted(group.getId(), groupDeleted)),
                () -> assertEquals(groupDeleted.isDeleted(), group.isDeleted())
        );
        verify(groupRepository).findById(eq(group.getId()));
        verify(groupRepository).saveAndFlush(same(group));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @TestConfiguration
    static class GroupServiceTestConfiguration {

        @Bean
        public GroupService groupService() {
            return new GroupService();
        }

    }

}
