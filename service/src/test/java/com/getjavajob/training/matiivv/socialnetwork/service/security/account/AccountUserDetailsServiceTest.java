package com.getjavajob.training.matiivv.socialnetwork.service.security.account;

import com.getjavajob.training.matiivv.socialnetwork.dao.account.AccountRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class AccountUserDetailsServiceTest {

    @Autowired
    private AccountUserDetailsService accountUserDetailsService;
    @MockBean
    private AccountRepository accountRepository;
    private Account account = new Account(1L);

    @Test
    public void testLoadUserByUsername() {
        var emailAddress = "emailAddress";
        when(accountRepository.findByEmailAddress(emailAddress)).thenReturn(Optional.of(account));
        assertSame(account, accountUserDetailsService.loadUserByUsername(emailAddress).getAccount());
        verify(accountRepository).findByEmailAddress(eq(emailAddress));
        verifyNoMoreInteractions(accountRepository);
    }

    @Test
    public void testLoadUserByUsernameNotFound() {
        var missingEmailAddress = "missingEmailAddress";
        when(accountRepository.findByEmailAddress(missingEmailAddress)).thenReturn(Optional.empty());
        assertThrows(UsernameNotFoundException.class, () -> accountUserDetailsService.loadUserByUsername(missingEmailAddress));
        verify(accountRepository).findByEmailAddress(eq(missingEmailAddress));
        verifyNoMoreInteractions(accountRepository);
    }

    @TestConfiguration
    static class AccountUserDetailsServiceTestConfiguration {

        @Bean
        public AccountUserDetailsService accountService() {
            return new AccountUserDetailsService();
        }

    }

}
