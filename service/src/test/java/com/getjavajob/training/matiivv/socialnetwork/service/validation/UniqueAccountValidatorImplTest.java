package com.getjavajob.training.matiivv.socialnetwork.service.validation;

import com.getjavajob.training.matiivv.socialnetwork.dao.account.AccountRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.constraint.UniqueAccountValidator;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account_;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.validation.ConstraintValidatorContext;

import static javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;
import static javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder.NodeBuilderCustomizableContext;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class UniqueAccountValidatorImplTest {

    private static final String TEMPLATE = "template";

    @Autowired
    private UniqueAccountValidator uniqueAccountValidator;
    @MockBean
    private AccountRepository accountRepository;
    @MockBean
    private ConstraintValidatorContext constraintValidatorContext;
    @MockBean
    private ConstraintViolationBuilder constraintViolationBuilder;
    @MockBean
    private NodeBuilderCustomizableContext nodeBuilderCustomizableContext;
    private Object[] mocks;
    private Account account = new Account();

    @BeforeEach
    public void setUp() {
        account.setEmailAddress("email");
        when(constraintValidatorContext.getDefaultConstraintMessageTemplate()).thenReturn(TEMPLATE);
        when(constraintValidatorContext.buildConstraintViolationWithTemplate(TEMPLATE)).thenReturn(constraintViolationBuilder);
        when(constraintViolationBuilder.addPropertyNode(Account_.EMAIL_ADDRESS)).thenReturn(nodeBuilderCustomizableContext);
        mocks = new Object[]{accountRepository,
                constraintValidatorContext,
                constraintViolationBuilder,
                nodeBuilderCustomizableContext};
    }

    @Test
    public void testIsValidNewAccountTrue() {
        when(accountRepository.existsByEmailAddress(account.getEmailAddress())).thenReturn(false);
        assertTrue(uniqueAccountValidator.isValid(account, constraintValidatorContext));
        verify(accountRepository, times(1)).existsByEmailAddress(eq(account.getEmailAddress()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testIsValidNewAccountFalse() {
        when(accountRepository.existsByEmailAddress(account.getEmailAddress())).thenReturn(true);
        assertFalse(uniqueAccountValidator.isValid(account, constraintValidatorContext));
        verify(accountRepository, times(1)).existsByEmailAddress(eq(account.getEmailAddress()));
        verify(constraintValidatorContext, times(1)).getDefaultConstraintMessageTemplate();
        verify(constraintValidatorContext, times(1)).disableDefaultConstraintViolation();
        verify(constraintValidatorContext, times(1)).buildConstraintViolationWithTemplate(eq(TEMPLATE));
        verify(constraintViolationBuilder, times(1)).addPropertyNode(eq(Account_.EMAIL_ADDRESS));
        verify(nodeBuilderCustomizableContext, times(1)).addConstraintViolation();
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testIsValidExistentAccountTrue() {
        account.setId(1L);
        when(accountRepository.existsByEmailAddressAndIdNot(account.getEmailAddress(), account.getId())).thenReturn(false);
        assertTrue(uniqueAccountValidator.isValid(account, constraintValidatorContext));
        verify(accountRepository, times(1)).existsByEmailAddressAndIdNot(eq(account.getEmailAddress()), eq(account.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testIsValidExistentAccountFalse() {
        account.setId(1L);
        when(accountRepository.existsByEmailAddressAndIdNot(account.getEmailAddress(), account.getId())).thenReturn(true);
        assertFalse(uniqueAccountValidator.isValid(account, constraintValidatorContext));
        verify(accountRepository, times(1)).existsByEmailAddressAndIdNot(eq(account.getEmailAddress()), eq(account.getId()));
        verify(constraintValidatorContext, times(1)).getDefaultConstraintMessageTemplate();
        verify(constraintValidatorContext, times(1)).disableDefaultConstraintViolation();
        verify(constraintValidatorContext, times(1)).buildConstraintViolationWithTemplate(eq(TEMPLATE));
        verify(constraintViolationBuilder, times(1)).addPropertyNode(eq(Account_.EMAIL_ADDRESS));
        verify(nodeBuilderCustomizableContext, times(1)).addConstraintViolation();
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testIsValidNullEmailAddress() {
        assertTrue(uniqueAccountValidator.isValid(new Account(), constraintValidatorContext));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testIsValidNullAccount() {
        assertTrue(uniqueAccountValidator.isValid(null, constraintValidatorContext));
        verifyNoMoreInteractions(mocks);
    }

    @TestConfiguration
    static class UniqueAccountValidatorImplTestConfiguration {

        @Bean
        public UniqueAccountValidator uniqueAccountValidator() {
            return new UniqueAccountValidatorImpl();
        }

    }

}
