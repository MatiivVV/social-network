package com.getjavajob.training.matiivv.socialnetwork.service.service.account;

import com.getjavajob.training.matiivv.socialnetwork.dao.account.AccountMessageRepository;
import com.getjavajob.training.matiivv.socialnetwork.dao.account.AccountRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.AccountMessage;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.MessageSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.event.BusinessEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.NotFoundEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.Resource;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceCreatedEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceDeletedEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class AccountMessageServiceTest {

    @SpyBean
    private AccountMessageService accountMessageService;
    @MockBean
    private AccountRepository accountRepository;
    @MockBean
    private AccountMessageRepository accountMessageRepository;
    @MockBean
    private ResourceService resourceService;
    @MockBean
    private ApplicationEventPublisher eventPublisher;
    private Object[] mocks;
    private Account account = new Account(1L);
    private Account author = new Account(2L);
    private AccountMessage accountMessage = new AccountMessage(1L);

    @BeforeEach
    public void setUp() {
        accountMessageService.setEventPublisher(eventPublisher);
        accountMessage.setAccount(account);
        accountMessage.setAuthor(author);
        when(accountMessageRepository.findByIdAndAccountIdAndAuthorId(accountMessage.getId(), account.getId(), author.getId()))
                .thenReturn(Optional.of(accountMessage));
        when(accountMessageRepository.saveAndFlush(same(accountMessage))).thenReturn(accountMessage);
        when(accountRepository.existsById(account.getId())).thenReturn(true);
        when(accountRepository.existsById(author.getId())).thenReturn(true);
        when(accountRepository.getOne(account.getId())).thenReturn(account);
        when(accountRepository.getOne(author.getId())).thenReturn(author);
        mocks = new Object[]{accountRepository,
                accountMessageRepository,
                resourceService,
                eventPublisher};
    }

    @Test
    public void testGetAccountMessage() {
        assertSame(accountMessage, accountMessageService.getAccountMessage(account.getId(), author.getId(), accountMessage.getId()));
        verify(accountMessageRepository)
                .findByIdAndAccountIdAndAuthorId(eq(accountMessage.getId()), eq(account.getId()), eq(author.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testGetAccountMessageNotFound() {
        var missingAccountMessageId = 99L;
        when(accountMessageRepository.findByIdAndAccountIdAndAuthorId(missingAccountMessageId, account.getId(), author.getId()))
                .thenReturn(Optional.empty());
        assertThrows(NotFoundEntityException.class, () ->
                accountMessageService.getAccountMessage(account.getId(), author.getId(), missingAccountMessageId));
        verify(accountMessageRepository)
                .findByIdAndAccountIdAndAuthorId(eq(missingAccountMessageId), eq(account.getId()), eq(author.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testGetAccountMessages() {
        Pageable pageRequest = PageRequest.of(1, 1);
        var result = new MessageSummary(1L, 2L, null, null, null, false, null, null, null);
        var page = new PageImpl<>(List.of(result), pageRequest, 1L);
        when(accountMessageRepository.findByAccountId(account.getId(), pageRequest)).thenReturn(page);
        assertSame(page, accountMessageService.getAccountMessages(account.getId(), pageRequest));
        verify(accountMessageRepository).findByAccountId(eq(account.getId()), eq(pageRequest));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testSendAccountMessage() {
        var imageId = "ImageId";
        when(resourceService.createResource(any(Resource.class))).thenReturn(imageId);
        assertAll(
                () -> assertSame(accountMessage, accountMessageService
                        .sendAccountMessage(account.getId(), author.getId(), accountMessage, mock(Resource.class))),
                () -> assertEquals(imageId, accountMessage.getImageId())
        );
        verify(accountRepository).existsById(eq(account.getId()));
        verify(accountRepository).existsById(eq(author.getId()));
        verify(accountRepository).getOne(eq(account.getId()));
        verify(accountRepository).getOne(eq(author.getId()));
        verify(resourceService).createResource(any(Resource.class));
        verify(eventPublisher).publishEvent(any(ResourceCreatedEvent.class));
        verify(accountMessageRepository).saveAndFlush(same(accountMessage));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testSendAccountMessageMissingImage() {
        assertAll(
                () -> assertSame(accountMessage, accountMessageService
                        .sendAccountMessage(account.getId(), author.getId(), accountMessage, null)),
                () -> assertNull(accountMessage.getImageId())
        );
        verify(accountRepository).existsById(eq(account.getId()));
        verify(accountRepository).existsById(eq(author.getId()));
        verify(accountRepository).getOne(eq(account.getId()));
        verify(accountRepository).getOne(eq(author.getId()));
        verify(accountMessageRepository).saveAndFlush(same(accountMessage));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testSendAccountMessageNotFoundAuthor() {
        var imageId = "imageId";
        when(resourceService.createResource(any(Resource.class))).thenReturn(imageId);
        when(accountRepository.existsById(author.getId())).thenReturn(false);
        assertThrows(NotFoundEntityException.class, () ->
                accountMessageService.sendAccountMessage(account.getId(), author.getId(), accountMessage, mock(Resource.class)));
        verify(accountRepository).existsById(eq(account.getId()));
        verify(accountRepository).existsById(eq(author.getId()));
        verify(accountRepository).getOne(eq(account.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testSendAccountMessageNotFoundAccount() {
        var imageId = "imageId";
        when(resourceService.createResource(any(Resource.class))).thenReturn(imageId);
        when(accountRepository.existsById(account.getId())).thenReturn(false);
        assertThrows(NotFoundEntityException.class, () ->
                accountMessageService.sendAccountMessage(account.getId(), author.getId(), accountMessage, mock(Resource.class)));
        verify(accountRepository).existsById(eq(account.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testDeleteAccountMessage() {
        accountMessage.setImageId("imageId");
        accountMessageService.deleteAccountMessage(account.getId(), author.getId(), accountMessage.getId());
        verify(accountMessageRepository)
                .findByIdAndAccountIdAndAuthorId(eq(accountMessage.getId()), eq(account.getId()), eq(author.getId()));
        verify(eventPublisher).publishEvent(any(ResourceDeletedEvent.class));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verify(accountMessageRepository).delete(same(accountMessage));
        verify(accountMessageRepository).flush();
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testDeleteAccountMessageMissingImage() {
        doReturn(accountMessage)
                .when(accountMessageService)
                .getAccountMessage(account.getId(), author.getId(), accountMessage.getId());
        accountMessageService.deleteAccountMessage(account.getId(), author.getId(), accountMessage.getId());
        verify(accountMessageService)
                .getAccountMessage(eq(account.getId()), eq(author.getId()), eq(accountMessage.getId()));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verify(accountMessageRepository).delete(same(accountMessage));
    }

    @Test
    public void testDeleteAccountMessageNotFound() {
        var missingAccountMessageId = 99L;
        doThrow(NotFoundEntityException.class)
                .when(accountMessageService)
                .getAccountMessage(account.getId(), author.getId(), missingAccountMessageId);
        assertThrows(NotFoundEntityException.class, () ->
                accountMessageService.deleteAccountMessage(account.getId(), author.getId(), missingAccountMessageId));
        verify(accountMessageService)
                .getAccountMessage(eq(account.getId()), eq(author.getId()), eq(missingAccountMessageId));
    }

    @TestConfiguration
    static class AccountMessageServiceTestConfiguration {

        @Bean
        public AccountMessageService accountMessageService() {
            return new AccountMessageService();
        }

    }

}
