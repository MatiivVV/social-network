package com.getjavajob.training.matiivv.socialnetwork.service.security.group;

import com.getjavajob.training.matiivv.socialnetwork.dao.group.MembershipRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Group;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Membership;
import com.getjavajob.training.matiivv.socialnetwork.service.security.account.AccountUserDetails;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.Role.ADMIN;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.Role.USER;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class GroupPermissionEvaluatorTest {

    @Autowired
    private GroupPermissionEvaluator groupPermissionEvaluator;
    @MockBean
    private MembershipRepository membershipRepository;
    @MockBean
    private Authentication authentication;
    private Account user = new Account(1L);
    private Group group = new Group(1L);
    private Membership membership = new Membership(new Membership.Id(1L, 1L));

    @BeforeEach
    public void setUp() {
        when(authentication.getPrincipal()).thenReturn(new AccountUserDetails(user));
        user.setRole(USER);
    }

    @Test
    public void testGetEntityType() {
        assertEquals(Group.class, groupPermissionEvaluator.getEntityType());
    }

    @Test
    public void testGetIdentityType() {
        assertEquals(Long.class, groupPermissionEvaluator.getIdentityType());
    }

    @Test
    public void testGetPermissionType() {
        assertEquals(GroupPermission.class, groupPermissionEvaluator.getPermissionType());
    }

    @Test
    public void testTestPermissionByIdAdmin() {
        user.setRole(ADMIN);
        assertAll(
                () -> assertTrue(groupPermissionEvaluator.testPermissionById(authentication, group.getId(), GroupPermission.READ)),
                () -> assertTrue(groupPermissionEvaluator.testPermissionById(authentication, group.getId(), GroupPermission.READ_SEND_MESSAGES)),
                () -> assertTrue(groupPermissionEvaluator.testPermissionById(authentication, group.getId(), GroupPermission.EDIT))
        );
    }

    @Test
    public void testTestPermissionByIdNotMember() {
        when(membershipRepository.findById(membership.getId())).thenReturn(Optional.empty());
        assertAll(
                () -> assertTrue(groupPermissionEvaluator.testPermissionById(authentication, group.getId(), GroupPermission.READ)),
                () -> assertFalse(groupPermissionEvaluator.testPermissionById(authentication, group.getId(), GroupPermission.READ_SEND_MESSAGES)),
                () -> assertFalse(groupPermissionEvaluator.testPermissionById(authentication, group.getId(), GroupPermission.EDIT))
        );

        verify(membershipRepository, times(2)).findById(membership.getId());
    }

    @Test
    public void testTestPermissionByIdMemberUser() {
        membership.setRole(USER);
        when(membershipRepository.findById(membership.getId())).thenReturn(Optional.of(membership));
        assertAll(
                () -> assertTrue(groupPermissionEvaluator.testPermissionById(authentication, group.getId(), GroupPermission.READ)),
                () -> assertTrue(groupPermissionEvaluator.testPermissionById(authentication, group.getId(), GroupPermission.READ_SEND_MESSAGES)),
                () -> assertFalse(groupPermissionEvaluator.testPermissionById(authentication, group.getId(), GroupPermission.EDIT))
        );

        verify(membershipRepository, times(2)).findById(membership.getId());
    }

    @Test
    public void testTestPermissionByIdMemberAdmin() {
        membership.setRole(ADMIN);
        when(membershipRepository.findById(membership.getId())).thenReturn(Optional.of(membership));
        assertAll(
                () -> assertTrue(groupPermissionEvaluator.testPermissionById(authentication, group.getId(), GroupPermission.READ)),
                () -> assertTrue(groupPermissionEvaluator.testPermissionById(authentication, group.getId(), GroupPermission.READ_SEND_MESSAGES)),
                () -> assertTrue(groupPermissionEvaluator.testPermissionById(authentication, group.getId(), GroupPermission.EDIT))
        );

        verify(membershipRepository, times(2)).findById(membership.getId());
    }

    @Test
    public void testTestPermissionByIdAnonymous() {
        authentication = mock(AnonymousAuthenticationToken.class);
        assertAll(
                () -> assertFalse(groupPermissionEvaluator.testPermissionById(authentication, group.getId(), GroupPermission.READ)),
                () -> assertFalse(groupPermissionEvaluator.testPermissionById(authentication, group.getId(), GroupPermission.READ_SEND_MESSAGES)),
                () -> assertFalse(groupPermissionEvaluator.testPermissionById(authentication, group.getId(), GroupPermission.EDIT))
        );
    }

    @TestConfiguration
    static class GroupPermissionEvaluatorTestConfiguration {

        @Bean
        public GroupPermissionEvaluator groupPermissionEvaluator() {
            return new GroupPermissionEvaluator();
        }

    }

}
