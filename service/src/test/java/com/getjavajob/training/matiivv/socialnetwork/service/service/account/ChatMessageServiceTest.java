package com.getjavajob.training.matiivv.socialnetwork.service.service.account;

import com.getjavajob.training.matiivv.socialnetwork.dao.account.AccountRepository;
import com.getjavajob.training.matiivv.socialnetwork.dao.account.ChatMessageRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.ChatMessage;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.ChatMessageSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.event.BusinessEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.NotFoundEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.Resource;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceCreatedEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class ChatMessageServiceTest {

    @SpyBean
    private ChatMessageService chatMessageService;
    @MockBean
    private AccountRepository accountRepository;
    @MockBean
    private ChatMessageRepository chatMessageRepository;
    @MockBean
    private ChatService chatService;
    @MockBean
    private ResourceService resourceService;
    @MockBean
    private ApplicationEventPublisher eventPublisher;
    private Object[] mocks;
    private Account recipient = new Account(1L);
    private Account author = new Account(2L);
    private ChatMessage chatMessage = new ChatMessage(1L);

    @BeforeEach
    public void setUp() {
        chatMessageService.setEventPublisher(eventPublisher);
        chatMessage.setRecipient(recipient);
        chatMessage.setAuthor(author);
        when(chatMessageRepository.findByIdAndRecipientIdAndAuthorId(chatMessage.getId(), recipient.getId(), author.getId()))
                .thenReturn(Optional.of(chatMessage));
        when(chatMessageRepository.saveAndFlush(same(chatMessage))).thenReturn(chatMessage);
        when(accountRepository.existsById(recipient.getId())).thenReturn(true);
        when(accountRepository.existsById(author.getId())).thenReturn(true);
        when(accountRepository.getOne(recipient.getId())).thenReturn(recipient);
        when(accountRepository.getOne(author.getId())).thenReturn(author);
        when(chatService.chatExists(recipient.getId(), author.getId())).thenReturn(false);
        when(chatService.chatExists(author.getId(), recipient.getId())).thenReturn(true);
        mocks = new Object[]{accountRepository,
                chatMessageRepository,
                chatService,
                resourceService,
                eventPublisher};
    }

    @Test
    public void testGetChatMessage() {
        assertSame(chatMessage, chatMessageService.getChatMessage(recipient.getId(), author.getId(), chatMessage.getId()));
        verify(chatMessageRepository)
                .findByIdAndRecipientIdAndAuthorId(eq(chatMessage.getId()), eq(recipient.getId()), eq(author.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testGetChatMessageNotFound() {
        var missingAccountMessageId = 99L;
        when(chatMessageRepository.findByIdAndRecipientIdAndAuthorId(missingAccountMessageId, recipient.getId(), author.getId()))
                .thenReturn(Optional.empty());
        assertThrows(NotFoundEntityException.class, () ->
                chatMessageService.getChatMessage(recipient.getId(), author.getId(), missingAccountMessageId));
        verify(chatMessageRepository)
                .findByIdAndRecipientIdAndAuthorId(eq(missingAccountMessageId), eq(recipient.getId()), eq(author.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testGetChatMessages() {
        Pageable pageRequest = PageRequest.of(1, 1);
        var result = new ChatMessageSummary(1L, 2L, 1L, null, null, null, false);
        var page = new PageImpl<>(List.of(result), pageRequest, 1L);
        when(chatMessageRepository.findByAccountIdAndOpponentId(recipient.getId(), author.getId(), pageRequest)).thenReturn(page);
        assertSame(page, chatMessageService.getChatMessages(recipient.getId(), author.getId(), pageRequest));
        verify(chatMessageRepository).findByAccountIdAndOpponentId(eq(recipient.getId()), eq(author.getId()), eq(pageRequest));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testSendChatMessage() {
        var imageId = "imageId";
        when(resourceService.createResource(any(Resource.class))).thenReturn(imageId);
        assertAll(
                () -> assertSame(chatMessage, chatMessageService
                        .sendChatMessage(recipient.getId(), author.getId(), chatMessage, mock(Resource.class))),
                () -> assertEquals(imageId, chatMessage.getImageId()),
                () -> assertFalse(chatMessage.isRead())
        );
        verify(accountRepository).existsById(eq(recipient.getId()));
        verify(accountRepository).existsById(eq(author.getId()));
        verify(accountRepository).getOne(eq(recipient.getId()));
        verify(accountRepository).getOne(eq(author.getId()));
        verify(resourceService).createResource(any(Resource.class));
        verify(eventPublisher).publishEvent(any(ResourceCreatedEvent.class));
        verify(chatService).chatExists(eq(recipient.getId()), eq(author.getId()));
        verify(chatService).createChat(eq(recipient.getId()), eq(author.getId()));
        verify(chatService).chatExists(eq(author.getId()), eq(recipient.getId()));
        verify(chatMessageRepository).saveAndFlush(same(chatMessage));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testSendChatMessageMissingImage() {
        assertAll(
                () -> assertSame(chatMessage, chatMessageService
                        .sendChatMessage(recipient.getId(), author.getId(), chatMessage, null)),
                () -> assertFalse(chatMessage.isRead())
        );
        verify(accountRepository).existsById(eq(recipient.getId()));
        verify(accountRepository).existsById(eq(author.getId()));
        verify(accountRepository).getOne(eq(recipient.getId()));
        verify(accountRepository).getOne(eq(author.getId()));
        verify(chatService).chatExists(eq(recipient.getId()), eq(author.getId()));
        verify(chatService).createChat(eq(recipient.getId()), eq(author.getId()));
        verify(chatService).chatExists(eq(author.getId()), eq(recipient.getId()));
        verify(chatMessageRepository).saveAndFlush(same(chatMessage));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testSendChatMessageNotFoundAuthor() {
        var imageId = "imageId";
        when(resourceService.createResource(any(Resource.class))).thenReturn(imageId);
        when(accountRepository.existsById(author.getId())).thenReturn(false);
        assertThrows(NotFoundEntityException.class, () ->
                chatMessageService.sendChatMessage(recipient.getId(), author.getId(), chatMessage, mock(Resource.class)));
        verify(accountRepository).existsById(eq(recipient.getId()));
        verify(accountRepository).existsById(eq(author.getId()));
        verify(accountRepository).getOne(eq(recipient.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testSendChatMessageNotFoundRecipient() {
        var imageId = "imageId";
        when(resourceService.createResource(any(Resource.class))).thenReturn(imageId);
        when(accountRepository.existsById(recipient.getId())).thenReturn(false);
        assertThrows(NotFoundEntityException.class, () ->
                chatMessageService.sendChatMessage(recipient.getId(), author.getId(), chatMessage, mock(Resource.class)));
        verify(accountRepository).existsById(eq(recipient.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @TestConfiguration
    static class ChatMessageServiceTestConfiguration {

        @Bean
        public ChatMessageService chatMessageService() {
            return new ChatMessageService();
        }

    }

}
