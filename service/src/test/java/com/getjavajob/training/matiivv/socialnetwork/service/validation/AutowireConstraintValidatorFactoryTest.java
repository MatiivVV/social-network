package com.getjavajob.training.matiivv.socialnetwork.service.validation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.validation.ConstraintValidator;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class AutowireConstraintValidatorFactoryTest {

    @Autowired
    private AutowireConstraintValidatorFactory autowireConstraintValidatorFactory;
    @MockBean
    private AutowireCapableBeanFactory beanFactory;

    @BeforeEach
    public void setUp() {
        autowireConstraintValidatorFactory.setBeanFactory(beanFactory);
    }

    @Test
    public void testGetInstanceExistent() {
        var instance = mock(ConstraintValidator.class);
        when(beanFactory.getBean(ConstraintValidator.class)).thenReturn(instance);
        assertSame(instance, autowireConstraintValidatorFactory.getInstance(ConstraintValidator.class));
        verify(beanFactory, times(1)).getBean(eq(ConstraintValidator.class));
        verifyNoMoreInteractions(beanFactory);
    }

    @Test
    public void testGetInstanceNew() {
        var instance = mock(ConstraintValidator.class);
        when(beanFactory.getBean(ConstraintValidator.class)).thenThrow(NoSuchBeanDefinitionException.class);
        when(beanFactory.createBean(ConstraintValidator.class)).thenReturn(instance);
        assertSame(instance, autowireConstraintValidatorFactory.getInstance(ConstraintValidator.class));
        verify(beanFactory, times(1)).getBean(eq(ConstraintValidator.class));
        verify(beanFactory, times(1)).createBean(eq(ConstraintValidator.class));
        verifyNoMoreInteractions(beanFactory);

    }

    @TestConfiguration
    static class AutowireConstraintValidatorFactoryTestConfiguration {

        @Bean
        public AutowireConstraintValidatorFactory autowireConstraintValidatorFactory() {
            return new AutowireConstraintValidatorFactory();
        }

    }

}
