package com.getjavajob.training.matiivv.socialnetwork.service.service.account;

import com.getjavajob.training.matiivv.socialnetwork.dao.account.AccountRepository;
import com.getjavajob.training.matiivv.socialnetwork.dao.account.ChatMessageRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.AccountSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.event.BusinessEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.IllegalStateEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.NotFoundEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.Resource;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceCreatedEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceDeletedEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.Role;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.Role.ADMIN;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.Role.USER;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class AccountServiceTest {

    @Autowired
    private AccountService accountService;
    @MockBean
    private AccountRepository accountRepository;
    @MockBean
    private ChatMessageRepository chatMessageRepository;
    @MockBean
    private PasswordEncoder passwordEncoder;
    @MockBean
    private ResourceService resourceService;
    @MockBean
    private ApplicationEventPublisher eventPublisher;
    private Object[] mocks;
    private Account account = spy(new Account(1L));

    @BeforeEach
    public void setUp() {
        accountService.setEventPublisher(eventPublisher);
        when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));
        when(accountRepository.saveAndFlush(same(account))).thenReturn(account);
        mocks = new Object[]{accountRepository,
                chatMessageRepository,
                passwordEncoder,
                resourceService,
                eventPublisher};
    }

    @Test
    public void testGetAccount() {
        account.setPhones(List.of());
        when(chatMessageRepository.countByRecipientIdAndReadFalse(account.getId())).thenReturn(2L);
        assertSame(account, accountService.getAccount(account.getId()));
        assertEquals(2L, account.getNewMessageCount());
        verify(accountRepository).findById(eq(account.getId()));
        //noinspection ResultOfMethodCallIgnored
        verify(account).getPhones();
        verify(chatMessageRepository).countByRecipientIdAndReadFalse(eq(account.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testGetAccountNotFound() {
        var missingAccountId = 99L;
        when(accountRepository.findById(missingAccountId)).thenReturn(Optional.empty());
        assertThrows(NotFoundEntityException.class, () -> accountService.getAccount(missingAccountId));
        verify(accountRepository).findById(eq(missingAccountId));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testGetAccounts() {
        var pattern = "  pattern  ";
        Pageable pageRequest = PageRequest.of(1, 1);
        var result = new AccountSummary(1L, null, null, null, null, null);
        var page = new PageImpl<>(List.of(result), pageRequest, 1L);
        when(accountRepository.findByNamePattern(pattern.trim(), pageRequest)).thenReturn(page);
        assertSame(page, accountService.getAccounts(pattern, pageRequest));
        verify(accountRepository).findByNamePattern(eq(pattern.trim()), eq(pageRequest));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testRegisterAccount() {
        var rawPassword = "rawPassword";
        var password = "password";
        var imageId = "imageId";
        account.setRawPassword(rawPassword);
        when(passwordEncoder.encode(rawPassword)).thenReturn(password);
        when(resourceService.createResource(any(Resource.class))).thenReturn(imageId);
        assertAll(
                () -> assertSame(account, accountService.registerAccount(account, mock(Resource.class))),
                () -> assertNull(account.getRawPassword()),
                () -> assertEquals(password, account.getPassword()),
                () -> assertEquals(USER, account.getRole()),
                () -> assertEquals(imageId, account.getImageId()),
                () -> assertNotNull(account.getSession()),
                () -> assertFalse(account.isDeleted())
        );
        verify(passwordEncoder).encode(eq(rawPassword));
        verify(resourceService).createResource(any(Resource.class));
        verify(eventPublisher).publishEvent(any(ResourceCreatedEvent.class));
        verify(accountRepository).saveAndFlush(same(account));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testEditAccountInfo() {
        var accountInfo = new Account();
        assertSame(account, accountService.editAccountInfo(account.getId(), accountInfo));
        verify(accountRepository).findById(eq(account.getId()));
        verify(account, never()).setRawPassword(anyString());
        verify(account, never()).setPassword(anyString());
        verify(account, never()).setRole(any(Role.class));
        verify(account, never()).setImageId(anyString());
        verify(account, never()).setSession(anyString());
        verify(account, never()).setDeleted(anyBoolean());
        verify(accountRepository).saveAndFlush(same(account));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testEditAccountPassword() {
        account.setPassword("oldPassword");
        var accountPassword = new Account();
        accountPassword.setOldPassword("oldRawPassword");
        accountPassword.setRawPassword("rawPassword");
        var password = "password";
        when(passwordEncoder.matches(accountPassword.getOldPassword(), account.getPassword())).thenReturn(true);
        when(passwordEncoder.encode(accountPassword.getRawPassword())).thenReturn(password);
        assertAll(
                () -> assertSame(account, accountService.editAccountPassword(account.getId(), accountPassword)),
                () -> assertNull(account.getRawPassword()),
                () -> assertNull(accountPassword.getOldPassword()),
                () -> assertNull(accountPassword.getRawPassword()),
                () -> assertEquals(password, account.getPassword())
        );
        verify(accountRepository).findById(eq(account.getId()));
        verify(passwordEncoder).matches(eq("oldRawPassword"), eq("oldPassword"));
        verify(passwordEncoder).encode(eq("rawPassword"));
        verify(accountRepository).saveAndFlush(same(account));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testEditAccountPasswordInvalidOldPassword() {
        account.setPassword("oldPassword");
        var accountPassword = new Account();
        accountPassword.setOldPassword("oldRawPassword");
        accountPassword.setRawPassword("rawPassword");
        when(passwordEncoder.matches(accountPassword.getOldPassword(), account.getPassword())).thenReturn(false);
        assertThrows(IllegalStateEntityException.class, () -> accountService.editAccountPassword(account.getId(), accountPassword));
        verify(accountRepository).findById(eq(account.getId()));
        verify(passwordEncoder).matches(eq("oldRawPassword"), eq("oldPassword"));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testEditAccountRole() {
        var accountRole = new Account();
        accountRole.setRole(ADMIN);
        assertAll(
                () -> assertSame(account, accountService.editAccountRole(account.getId(), accountRole)),
                () -> assertEquals(accountRole.getRole(), account.getRole())
        );
        verify(accountRepository).findById(eq(account.getId()));
        verify(accountRepository).saveAndFlush(same(account));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testEditAccountImageCreate() {
        var imageId = "imageId";
        when(resourceService.createResource(any(Resource.class))).thenReturn(imageId);
        assertAll(
                () -> assertSame(account, accountService.editAccountImage(account.getId(), mock(Resource.class))),
                () -> assertEquals(imageId, account.getImageId())
        );
        verify(accountRepository).findById(eq(account.getId()));
        verify(resourceService).createResource(any(Resource.class));
        verify(eventPublisher).publishEvent(any(ResourceCreatedEvent.class));
        verify(accountRepository).saveAndFlush(same(account));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testEditAccountImageReplace() {
        account.setImageId("oldImageId");
        var imageId = "imageId";
        when(resourceService.createResource(any(Resource.class))).thenReturn(imageId);
        assertAll(
                () -> assertSame(account, accountService.editAccountImage(account.getId(), mock(Resource.class))),
                () -> assertEquals(imageId, account.getImageId())
        );
        verify(accountRepository).findById(eq(account.getId()));
        verify(resourceService).createResource(any(Resource.class));
        verify(eventPublisher).publishEvent(any(ResourceDeletedEvent.class));
        verify(eventPublisher).publishEvent(any(ResourceCreatedEvent.class));
        verify(accountRepository).saveAndFlush(same(account));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testEditAccountImageDelete() {
        account.setImageId("oldImageId");
        assertAll(
                () -> assertSame(account, accountService.editAccountImage(account.getId(), null)),
                () -> assertNull(account.getImageId())
        );
        verify(accountRepository).findById(eq(account.getId()));
        verify(eventPublisher).publishEvent(any(ResourceDeletedEvent.class));
        verify(accountRepository).saveAndFlush(same(account));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testResetAccountSession() {
        accountService.resetAccountSession(account.getId());
        assertNotNull(account.getSession());
        verify(accountRepository).findById(eq(account.getId()));
        verify(accountRepository).saveAndFlush(same(account));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testEditAccountDeleted() {
        var accountDeleted = new Account();
        accountDeleted.setDeleted(true);
        assertAll(
                () -> assertSame(account, accountService.editAccountDeleted(account.getId(), accountDeleted)),
                () -> assertEquals(accountDeleted.isDeleted(), account.isDeleted())
        );
        verify(accountRepository).findById(eq(account.getId()));
        verify(accountRepository).saveAndFlush(same(account));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @TestConfiguration
    static class AccountServiceTestConfiguration {

        @Bean
        public AccountService accountService() {
            return new AccountService();
        }

    }

}
