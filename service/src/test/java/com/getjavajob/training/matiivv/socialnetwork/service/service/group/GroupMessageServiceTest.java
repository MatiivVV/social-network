package com.getjavajob.training.matiivv.socialnetwork.service.service.group;

import com.getjavajob.training.matiivv.socialnetwork.dao.account.AccountRepository;
import com.getjavajob.training.matiivv.socialnetwork.dao.group.GroupMessageRepository;
import com.getjavajob.training.matiivv.socialnetwork.dao.group.GroupRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Group;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.GroupMessage;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.MessageSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.event.BusinessEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.NotFoundEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.Resource;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceCreatedEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceDeletedEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.resource.ResourceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class GroupMessageServiceTest {

    @SpyBean
    private GroupMessageService groupMessageService;
    @MockBean
    private AccountRepository accountRepository;
    @MockBean
    private GroupRepository groupRepository;
    @MockBean
    private GroupMessageRepository groupMessageRepository;
    @MockBean
    private ResourceService resourceService;
    @MockBean
    private ApplicationEventPublisher eventPublisher;
    private Object[] mocks;
    private Group group = new Group(1L);
    private Account author = new Account(1L);
    private GroupMessage groupMessage = new GroupMessage(1L);

    @BeforeEach
    public void setUp() {
        groupMessageService.setEventPublisher(eventPublisher);
        groupMessage.setGroup(group);
        groupMessage.setAuthor(author);
        when(groupMessageRepository.findByIdAndGroupIdAndAuthorId(groupMessage.getId(), group.getId(), author.getId()))
                .thenReturn(Optional.of(groupMessage));
        when(groupMessageRepository.saveAndFlush(same(groupMessage))).thenReturn(groupMessage);
        when(groupRepository.existsById(group.getId())).thenReturn(true);
        when(accountRepository.existsById(author.getId())).thenReturn(true);
        when(groupRepository.getOne(group.getId())).thenReturn(group);
        when(accountRepository.getOne(author.getId())).thenReturn(author);
        mocks = new Object[]{accountRepository,
                groupRepository,
                groupMessageRepository,
                resourceService,
                eventPublisher};
    }

    @Test
    public void testGetGroupMessage() {
        assertSame(groupMessage, groupMessageService.getGroupMessage(group.getId(), author.getId(), groupMessage.getId()));
        verify(groupMessageRepository)
                .findByIdAndGroupIdAndAuthorId(eq(groupMessage.getId()), eq(group.getId()), eq(author.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testGetGroupMessageNotFound() {
        var missingGroupMessageId = 99L;
        when(groupMessageRepository.findByIdAndGroupIdAndAuthorId(missingGroupMessageId, group.getId(), author.getId()))
                .thenReturn(Optional.empty());
        assertThrows(NotFoundEntityException.class, () ->
                groupMessageService.getGroupMessage(group.getId(), author.getId(), missingGroupMessageId));
        verify(groupMessageRepository)
                .findByIdAndGroupIdAndAuthorId(eq(missingGroupMessageId), eq(group.getId()), eq(author.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testGetGroupMessages() {
        Pageable pageRequest = PageRequest.of(1, 1);
        var result = new MessageSummary(1L, 2L, null, null, null, false, null, null, null);
        var page = new PageImpl<>(List.of(result), pageRequest, 1L);
        when(groupMessageRepository.findByGroupId(group.getId(), pageRequest)).thenReturn(page);
        assertSame(page, groupMessageService.getGroupMessages(group.getId(), pageRequest));
        verify(groupMessageRepository).findByGroupId(eq(group.getId()), eq(pageRequest));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testSendGroupMessage() {
        var imageId = "imageId";
        when(resourceService.createResource(any(Resource.class))).thenReturn(imageId);
        assertAll(
                () -> assertSame(groupMessage, groupMessageService
                        .sendGroupMessage(group.getId(), author.getId(), groupMessage, mock(Resource.class))),
                () -> assertEquals(imageId, groupMessage.getImageId())
        );
        verify(groupRepository).existsById(eq(group.getId()));
        verify(accountRepository).existsById(eq(author.getId()));
        verify(groupRepository).getOne(eq(group.getId()));
        verify(accountRepository).getOne(eq(author.getId()));
        verify(resourceService).createResource(any(Resource.class));
        verify(eventPublisher).publishEvent(any(ResourceCreatedEvent.class));
        verify(groupMessageRepository).saveAndFlush(same(groupMessage));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testSendGroupMessageMissingImage() {
        assertAll(
                () -> assertSame(groupMessage, groupMessageService
                        .sendGroupMessage(group.getId(), author.getId(), groupMessage, null)),
                () -> assertNull(groupMessage.getImageId())
        );
        verify(groupRepository).existsById(eq(group.getId()));
        verify(accountRepository).existsById(eq(author.getId()));
        verify(groupRepository).getOne(eq(group.getId()));
        verify(accountRepository).getOne(eq(author.getId()));
        verify(groupMessageRepository).saveAndFlush(same(groupMessage));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testSendGroupMessageNotFoundAuthor() {
        var imageId = "imageId";
        when(resourceService.createResource(any(Resource.class))).thenReturn(imageId);
        when(accountRepository.existsById(author.getId())).thenReturn(false);
        assertThrows(NotFoundEntityException.class, () ->
                groupMessageService.sendGroupMessage(group.getId(), author.getId(), groupMessage, mock(Resource.class)));
        verify(groupRepository).existsById(eq(group.getId()));
        verify(accountRepository).existsById(eq(author.getId()));
        verify(groupRepository).getOne(eq(group.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testSendGroupMessageNotFoundGroup() {
        var imageId = "imageId";
        when(resourceService.createResource(any(Resource.class))).thenReturn(imageId);
        when(groupRepository.existsById(group.getId())).thenReturn(false);
        assertThrows(NotFoundEntityException.class, () ->
                groupMessageService.sendGroupMessage(group.getId(), author.getId(), groupMessage, mock(Resource.class)));
        verify(groupRepository).existsById(eq(group.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testDeleteGroupMessage() {
        groupMessage.setImageId("imageId");
        groupMessageService.deleteGroupMessage(group.getId(), author.getId(), groupMessage.getId());
        verify(groupMessageRepository)
                .findByIdAndGroupIdAndAuthorId(eq(groupMessage.getId()), eq(group.getId()), eq(author.getId()));
        verify(eventPublisher).publishEvent(any(ResourceDeletedEvent.class));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verify(groupMessageRepository).delete(same(groupMessage));
        verify(groupMessageRepository).flush();
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testDeleteGroupMessageMissingImage() {
        doReturn(groupMessage)
                .when(groupMessageService)
                .getGroupMessage(group.getId(), author.getId(), groupMessage.getId());
        groupMessageService.deleteGroupMessage(group.getId(), author.getId(), groupMessage.getId());
        verify(groupMessageService).getGroupMessage(eq(group.getId()), eq(author.getId()), eq(groupMessage.getId()));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verify(groupMessageRepository).delete(same(groupMessage));
    }

    @Test
    public void testDeleteGroupMessageNotFound() {
        var missingGroupMessageId = 99L;
        doThrow(NotFoundEntityException.class)
                .when(groupMessageService)
                .getGroupMessage(group.getId(), author.getId(), missingGroupMessageId);
        assertThrows(NotFoundEntityException.class, () ->
                groupMessageService.deleteGroupMessage(group.getId(), author.getId(), missingGroupMessageId));
        verify(groupMessageService).getGroupMessage(eq(group.getId()), eq(author.getId()), eq(missingGroupMessageId));
    }

    @TestConfiguration
    static class GroupMessageServiceTestConfiguration {

        @Bean
        public GroupMessageService groupMessageService() {
            return new GroupMessageService();
        }

    }

}
