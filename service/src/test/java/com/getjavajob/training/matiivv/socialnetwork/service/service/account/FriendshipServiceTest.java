package com.getjavajob.training.matiivv.socialnetwork.service.service.account;

import com.getjavajob.training.matiivv.socialnetwork.dao.account.AccountRepository;
import com.getjavajob.training.matiivv.socialnetwork.dao.account.FriendshipRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Friendship;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.FriendshipSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.event.BusinessEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.AlreadyExistsEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.IllegalStateEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.NotFoundEntityException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.EnumSet;
import java.util.List;
import java.util.Optional;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status.*;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Friendship.Id;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Friendship.Type.REQUEST;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Friendship.Type.RESPONSE;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class FriendshipServiceTest {

    @SpyBean
    private FriendshipService friendshipService;
    @MockBean
    private AccountRepository accountRepository;
    @MockBean
    private FriendshipRepository friendshipRepository;
    @MockBean
    private ApplicationEventPublisher eventPublisher;
    private Object[] mocks;
    private Account account = new Account(1L);
    private Account friend = new Account(2L);
    private Friendship friendship = new Friendship(new Id(1L, 2L));
    private Friendship inverseFriendship = new Friendship(new Id(2L, 1L));

    @BeforeEach
    public void setUp() {
        friendshipService.setEventPublisher(eventPublisher);
        friendship.setAccount(account);
        friendship.setFriend(friend);
        inverseFriendship.setAccount(friend);
        inverseFriendship.setFriend(account);
        when(friendshipRepository.findById(friendship.getId())).thenReturn(Optional.of(friendship));
        when(accountRepository.existsById(account.getId())).thenReturn(true);
        when(accountRepository.existsById(friend.getId())).thenReturn(true);
        when(accountRepository.getOne(account.getId())).thenReturn(account);
        when(accountRepository.getOne(friend.getId())).thenReturn(friend);
        mocks = new Object[]{accountRepository,
                friendshipRepository,
                eventPublisher};
    }

    @Test
    public void testGetFriendship() {
        assertSame(friendship, friendshipService.getFriendship(account.getId(), friend.getId()));
        verify(friendshipRepository).findById(eq(friendship.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testGetFriendshipNotFound() {
        var missingFriendId = 99L;
        var missingFriendshipId = new Id(account.getId(), missingFriendId);
        when(friendshipRepository.findById(missingFriendshipId)).thenReturn(Optional.empty());
        assertThrows(NotFoundEntityException.class, () ->
                friendshipService.getFriendship(account.getId(), missingFriendId));
        verify(friendshipRepository).findById(eq(missingFriendshipId));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testGetFriendships() {
        var types = EnumSet.of(RESPONSE, REQUEST);
        var statuses = EnumSet.of(ACCEPTED, PENDING);
        Pageable pageRequest = PageRequest.of(1, 1);
        var result = new FriendshipSummary(2L, null, null, null, null, null, false, null, null, null);
        var page = new PageImpl<>(List.of(result), pageRequest, 1L);
        when(friendshipRepository.search(account.getId(), types, statuses, pageRequest)).thenReturn(page);
        assertSame(page, friendshipService.getFriendships(account.getId(), types, statuses, pageRequest));
        verify(friendshipRepository).search(eq(account.getId()), eq(types), eq(statuses), eq(pageRequest));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testGetFriends() {
        var statuses = EnumSet.of(ACCEPTED);
        Pageable pageRequest = PageRequest.of(1, 1);
        var result = new FriendshipSummary(2L, null, null, null, null, null, false, null, null, null);
        var page = new PageImpl<>(List.of(result), pageRequest, 1L);
        when(friendshipRepository.search(account.getId(), null, statuses, pageRequest)).thenReturn(page);
        assertSame(page, friendshipService.getFriends(account.getId(), pageRequest));
        verify(friendshipRepository).search(eq(account.getId()), isNull(), eq(statuses), eq(pageRequest));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testCreateFriendship() {
        when(friendshipRepository.save(any(Friendship.class))).thenReturn(friendship);
        when(friendshipRepository.saveAndFlush(any(Friendship.class))).thenReturn(friendship);
        assertSame(friendship, friendshipService.createFriendship(account.getId(), friend.getId()));
        verify(accountRepository).existsById(eq(account.getId()));
        verify(accountRepository).existsById(eq(friend.getId()));
        verify(accountRepository).getOne(eq(account.getId()));
        verify(accountRepository).getOne(eq(friend.getId()));
        verify(friendshipRepository).save(any(Friendship.class));
        verify(friendshipRepository).saveAndFlush(any(Friendship.class));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testCreateFriendshipAlreadyExists() {
        when(friendshipRepository.save(any(Friendship.class))).thenReturn(friendship);
        when(friendshipRepository.saveAndFlush(any(Friendship.class))).thenThrow(DataIntegrityViolationException.class);
        assertThrows(AlreadyExistsEntityException.class, () ->
                friendshipService.createFriendship(account.getId(), friend.getId()));
        verify(accountRepository).existsById(eq(account.getId()));
        verify(accountRepository).existsById(eq(friend.getId()));
        verify(accountRepository).getOne(eq(account.getId()));
        verify(accountRepository).getOne(eq(friend.getId()));
        verify(friendshipRepository).save(any(Friendship.class));
        verify(friendshipRepository).saveAndFlush(any(Friendship.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testCreateFriendshipNotFoundFriend() {
        when(accountRepository.existsById(friend.getId())).thenReturn(false);
        assertThrows(NotFoundEntityException.class, () ->
                friendshipService.createFriendship(account.getId(), friend.getId()));
        verify(accountRepository).existsById(eq(account.getId()));
        verify(accountRepository).existsById(eq(friend.getId()));
        verify(accountRepository).getOne(eq(account.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testCreateFriendshipNotFoundAccount() {
        when(accountRepository.existsById(account.getId())).thenReturn(false);
        assertThrows(NotFoundEntityException.class, () ->
                friendshipService.createFriendship(account.getId(), friend.getId()));
        verify(accountRepository).existsById(eq(account.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testCreateFriendshipSelfReference() {
        assertThrows(IllegalStateEntityException.class, () ->
                friendshipService.createFriendship(account.getId(), account.getId()));
    }

    @Test
    public void testEditFriendshipStatusResponsePending() {
        friendship.setType(RESPONSE);
        friendship.setStatus(PENDING);
        var friendshipStatus = new Friendship();
        friendshipStatus.setStatus(ACCEPTED);
        doReturn(friendship).when(friendshipService).getFriendship(account.getId(), friend.getId());
        doReturn(inverseFriendship).when(friendshipService).getFriendship(friend.getId(), account.getId());
        when(friendshipRepository.saveAndFlush(friendship)).thenReturn(friendship);
        when(friendshipRepository.save(inverseFriendship)).thenReturn(inverseFriendship);
        assertAll(
                () -> assertSame(friendship, friendshipService
                        .editFriendshipStatus(account.getId(), friend.getId(), friendshipStatus)),
                () -> assertEquals(ACCEPTED, friendship.getStatus()),
                () -> assertEquals(ACCEPTED, inverseFriendship.getStatus())
        );
        verify(friendshipService).getFriendship(eq(account.getId()), eq(friend.getId()));
        verify(friendshipService).getFriendship(eq(friend.getId()), eq(account.getId()));
        verify(friendshipRepository).save(same(inverseFriendship));
        verify(friendshipRepository).saveAndFlush(same(friendship));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testEditFriendshipStatusResponseNotPending() {
        friendship.setType(RESPONSE);
        friendship.setStatus(DECLINED);
        var friendshipStatus = new Friendship();
        friendshipStatus.setStatus(PENDING);
        doReturn(friendship).when(friendshipService).getFriendship(account.getId(), friend.getId());
        doReturn(inverseFriendship).when(friendshipService).getFriendship(friend.getId(), account.getId());
        assertThrows(IllegalStateEntityException.class, () ->
                friendshipService.editFriendshipStatus(account.getId(), friend.getId(), friendshipStatus));
        verify(friendshipService).getFriendship(eq(account.getId()), eq(friend.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testEditFriendshipStatusRequestDeclinedToPending() {
        friendship.setType(REQUEST);
        friendship.setStatus(DECLINED);
        var friendshipStatus = new Friendship();
        friendshipStatus.setStatus(PENDING);
        doReturn(friendship).when(friendshipService).getFriendship(account.getId(), friend.getId());
        doReturn(inverseFriendship).when(friendshipService).getFriendship(friend.getId(), account.getId());
        when(friendshipRepository.saveAndFlush(friendship)).thenReturn(friendship);
        when(friendshipRepository.save(inverseFriendship)).thenReturn(inverseFriendship);
        assertAll(
                () -> assertSame(friendship, friendshipService
                        .editFriendshipStatus(account.getId(), friend.getId(), friendshipStatus)),
                () -> assertEquals(PENDING, friendship.getStatus()),
                () -> assertEquals(PENDING, inverseFriendship.getStatus())
        );
        verify(friendshipService).getFriendship(eq(account.getId()), eq(friend.getId()));
        verify(friendshipService).getFriendship(eq(friend.getId()), eq(account.getId()));
        verify(friendshipRepository).save(same(inverseFriendship));
        verify(friendshipRepository).saveAndFlush(same(friendship));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testEditFriendshipStatusRequestNotDeclinedToPending() {
        friendship.setType(RESPONSE);
        friendship.setStatus(ACCEPTED);
        var friendshipStatus = new Friendship();
        friendshipStatus.setStatus(PENDING);
        doReturn(friendship).when(friendshipService).getFriendship(account.getId(), friend.getId());
        doReturn(inverseFriendship).when(friendshipService).getFriendship(friend.getId(), account.getId());
        assertThrows(IllegalStateEntityException.class, () ->
                friendshipService.editFriendshipStatus(account.getId(), friend.getId(), friendshipStatus));
        verify(friendshipService).getFriendship(eq(account.getId()), eq(friend.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testDeleteFriendship() {
        doReturn(friendship).when(friendshipService).getFriendship(account.getId(), friend.getId());
        friendshipService.deleteFriendship(account.getId(), friend.getId());
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verify(friendshipRepository).deleteById(eq(friendship.getId()));
        verify(friendshipRepository).deleteById(eq(inverseFriendship.getId()));
    }

    @Test
    public void testDeleteFriendshipNotFound() {
        doThrow(EmptyResultDataAccessException.class).when(friendshipRepository).deleteById(friendship.getId());
        assertThrows(NotFoundEntityException.class, () ->
                friendshipService.deleteFriendship(account.getId(), friend.getId()));
        verify(friendshipRepository).deleteById(eq(friendship.getId()));
    }

    @TestConfiguration
    static class FriendshipServiceTestConfiguration {

        @Bean
        public FriendshipService friendshipService() {
            return new FriendshipService();
        }

    }

}
