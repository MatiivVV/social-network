package com.getjavajob.training.matiivv.socialnetwork.service.service.account;

import com.getjavajob.training.matiivv.socialnetwork.dao.account.AccountRepository;
import com.getjavajob.training.matiivv.socialnetwork.dao.account.ChatMessageRepository;
import com.getjavajob.training.matiivv.socialnetwork.dao.account.ChatRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Chat;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.ChatSummary;
import com.getjavajob.training.matiivv.socialnetwork.service.service.event.BusinessEvent;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.AlreadyExistsEntityException;
import com.getjavajob.training.matiivv.socialnetwork.service.service.exception.NotFoundEntityException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Chat.Id;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class ChatServiceTest {

    @SpyBean
    private ChatService chatService;
    @MockBean
    private AccountRepository accountRepository;
    @MockBean
    private ChatRepository chatRepository;
    @MockBean
    private ChatMessageRepository chatMessageRepository;
    @MockBean
    private ApplicationEventPublisher eventPublisher;
    private Object[] mocks;
    private Account account = new Account(1L);
    private Account opponent = new Account(2L);
    private Chat chat = new Chat(new Id(1L, 2L));

    @BeforeEach
    public void setUp() {
        chatService.setEventPublisher(eventPublisher);
        chat.setAccount(account);
        chat.setOpponent(opponent);
        when(chatRepository.findById(chat.getId())).thenReturn(Optional.of(chat));
        when(accountRepository.existsById(account.getId())).thenReturn(true);
        when(accountRepository.existsById(opponent.getId())).thenReturn(true);
        when(accountRepository.getOne(account.getId())).thenReturn(account);
        when(accountRepository.getOne(opponent.getId())).thenReturn(opponent);
        mocks = new Object[]{accountRepository,
                chatRepository,
                chatMessageRepository,
                eventPublisher};
    }

    @Test
    public void testGetChat() {
        assertSame(chat, chatService.getChat(account.getId(), opponent.getId()));
        verify(chatRepository).findById(eq(chat.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testGetChatNotFound() {
        var missingOpponentId = 99L;
        var missingChatId = new Id(account.getId(), missingOpponentId);
        when(chatRepository.findById(missingChatId)).thenReturn(Optional.empty());
        assertThrows(NotFoundEntityException.class, () -> chatService.getChat(account.getId(), missingOpponentId));
        verify(chatRepository).findById(eq(missingChatId));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testGetChats() {
        Pageable pageRequest = PageRequest.of(1, 1);
        var result = new ChatSummary(2L, null, null, null, false, null, null, 2L);
        var page = new PageImpl<>(List.of(result), pageRequest, 1L);
        when(chatRepository.findByAccountId(account.getId(), pageRequest)).thenReturn(page);
        assertSame(page, chatService.getChats(account.getId(), pageRequest));
        verify(chatRepository).findByAccountId(eq(account.getId()), eq(pageRequest));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testCreateChat() {
        when(chatRepository.saveAndFlush(any(Chat.class))).thenReturn(chat);
        assertSame(chat, chatService.createChat(account.getId(), opponent.getId()));
        verify(accountRepository).existsById(eq(account.getId()));
        verify(accountRepository).existsById(eq(opponent.getId()));
        verify(accountRepository).getOne(eq(account.getId()));
        verify(accountRepository).getOne(eq(opponent.getId()));
        verify(chatRepository).saveAndFlush(any(Chat.class));
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testCreateChatAlreadyExists() {
        when(chatRepository.saveAndFlush(any(Chat.class))).thenThrow(DataIntegrityViolationException.class);
        assertThrows(AlreadyExistsEntityException.class, () -> chatService.createChat(account.getId(), opponent.getId()));
        verify(accountRepository).existsById(eq(account.getId()));
        verify(accountRepository).existsById(eq(opponent.getId()));
        verify(accountRepository).getOne(eq(account.getId()));
        verify(accountRepository).getOne(eq(opponent.getId()));
        verify(chatRepository).saveAndFlush(any(Chat.class));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testCreateChatNotFoundOpponent() {
        when(accountRepository.existsById(opponent.getId())).thenReturn(false);
        assertThrows(NotFoundEntityException.class, () -> chatService.createChat(account.getId(), opponent.getId()));
        verify(accountRepository).existsById(eq(account.getId()));
        verify(accountRepository).existsById(eq(opponent.getId()));
        verify(accountRepository).getOne(eq(account.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testCreateChatNotFoundAccount() {
        when(accountRepository.existsById(account.getId())).thenReturn(false);
        assertThrows(NotFoundEntityException.class, () -> chatService.createChat(account.getId(), opponent.getId()));
        verify(accountRepository).existsById(eq(account.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testSetChatRead() {
        when(chatMessageRepository.setReadByRecipientIdAndAuthorId(account.getId(), opponent.getId())).thenReturn(3);
        assertSame(3L, chatService.setChatRead(account.getId(), opponent.getId()));
        verify(chatMessageRepository).setReadByRecipientIdAndAuthorId(eq(account.getId()), eq(opponent.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testDeleteChat() {
        doReturn(chat).when(chatService).getChat(account.getId(), opponent.getId());
        chatService.deleteChat(account.getId(), opponent.getId());
        verify(eventPublisher).publishEvent(any(BusinessEvent.class));
        verify(chatRepository).deleteById(eq(chat.getId()));
    }

    @Test
    public void testDeleteFriendshipNotFound() {
        doThrow(EmptyResultDataAccessException.class).when(chatRepository).deleteById(chat.getId());
        assertThrows(NotFoundEntityException.class, () -> chatService.deleteChat(account.getId(), opponent.getId()));
        verify(chatRepository).deleteById(eq(chat.getId()));
    }

    @TestConfiguration
    static class ChatServiceTestConfiguration {

        @Bean
        public ChatService chatService() {
            return new ChatService();
        }

    }

}
