package com.getjavajob.training.matiivv.socialnetwork.service.validation;

import com.getjavajob.training.matiivv.socialnetwork.dao.group.GroupRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.constraint.UniqueGroupValidator;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Group;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Group_;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.validation.ConstraintValidatorContext;

import static javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;
import static javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder.NodeBuilderCustomizableContext;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class UniqueGroupValidatorImplTest {

    private static final String TEMPLATE = "template";

    @Autowired
    private UniqueGroupValidator uniqueGroupValidator;
    @MockBean
    private GroupRepository groupRepository;
    @MockBean
    private ConstraintValidatorContext constraintValidatorContext;
    @MockBean
    private ConstraintViolationBuilder constraintViolationBuilder;
    @MockBean
    private NodeBuilderCustomizableContext nodeBuilderCustomizableContext;
    private Object[] mocks;
    private Group group = new Group();

    @BeforeEach
    public void setUp() {
        group.setName("name");
        when(constraintValidatorContext.getDefaultConstraintMessageTemplate()).thenReturn(TEMPLATE);
        when(constraintValidatorContext.buildConstraintViolationWithTemplate(TEMPLATE)).thenReturn(constraintViolationBuilder);
        when(constraintViolationBuilder.addPropertyNode(Group_.NAME)).thenReturn(nodeBuilderCustomizableContext);
        mocks = new Object[]{groupRepository,
                constraintValidatorContext,
                constraintViolationBuilder,
                nodeBuilderCustomizableContext};
    }

    @Test
    public void testIsValidNewGroupTrue() {
        when(groupRepository.existsByName(group.getName())).thenReturn(false);
        assertTrue(uniqueGroupValidator.isValid(group, constraintValidatorContext));
        verify(groupRepository, times(1)).existsByName(eq(group.getName()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testIsValidNewGroupFalse() {
        when(groupRepository.existsByName(group.getName())).thenReturn(true);
        assertFalse(uniqueGroupValidator.isValid(group, constraintValidatorContext));
        verify(groupRepository, times(1)).existsByName(eq(group.getName()));
        verify(constraintValidatorContext, times(1)).getDefaultConstraintMessageTemplate();
        verify(constraintValidatorContext, times(1)).disableDefaultConstraintViolation();
        verify(constraintValidatorContext, times(1)).buildConstraintViolationWithTemplate(eq(TEMPLATE));
        verify(constraintViolationBuilder, times(1)).addPropertyNode(eq(Group_.NAME));
        verify(nodeBuilderCustomizableContext, times(1)).addConstraintViolation();
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testIsValidExistentGroupTrue() {
        group.setId(1L);
        when(groupRepository.existsByNameAndIdNot(group.getName(), group.getId())).thenReturn(false);
        assertTrue(uniqueGroupValidator.isValid(group, constraintValidatorContext));
        verify(groupRepository, times(1)).existsByNameAndIdNot(eq(group.getName()), eq(group.getId()));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testIsValidExistentGroupFalse() {
        group.setId(1L);
        when(groupRepository.existsByNameAndIdNot(group.getName(), group.getId())).thenReturn(true);
        assertFalse(uniqueGroupValidator.isValid(group, constraintValidatorContext));
        verify(groupRepository, times(1)).existsByNameAndIdNot(eq(group.getName()), eq(group.getId()));
        verify(constraintValidatorContext, times(1)).getDefaultConstraintMessageTemplate();
        verify(constraintValidatorContext, times(1)).disableDefaultConstraintViolation();
        verify(constraintValidatorContext, times(1)).buildConstraintViolationWithTemplate(eq(TEMPLATE));
        verify(constraintViolationBuilder, times(1)).addPropertyNode(eq(Group_.NAME));
        verify(nodeBuilderCustomizableContext, times(1)).addConstraintViolation();
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testIsValidNullName() {
        assertTrue(uniqueGroupValidator.isValid(new Group(), constraintValidatorContext));
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void testIsValidNullGroup() {
        assertTrue(uniqueGroupValidator.isValid(null, constraintValidatorContext));
        verifyNoMoreInteractions(mocks);
    }

    @TestConfiguration
    static class UniqueGroupValidatorImplTestConfiguration {

        @Bean
        public UniqueGroupValidator uniqueGroupValidator() {
            return new UniqueGroupValidatorImpl();
        }

    }

}
