package com.getjavajob.training.matiivv.socialnetwork.service.security.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.Role.ADMIN;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.Role.USER;
import static com.getjavajob.training.matiivv.socialnetwork.service.security.account.AccountPermission.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class AccountPermissionEvaluatorTest {

    @Autowired
    private AccountPermissionEvaluator accountPermissionEvaluator;
    @MockBean
    private Authentication authentication;
    private Account user = new Account(1L);
    private Account account = new Account(2L);

    @BeforeEach
    public void setUp() {
        when(authentication.getPrincipal()).thenReturn(new AccountUserDetails(user));
        user.setRole(USER);
    }

    @Test
    public void testGetEntityType() {
        assertEquals(Account.class, accountPermissionEvaluator.getEntityType());
    }

    @Test
    public void testGetIdentityType() {
        assertEquals(Long.class, accountPermissionEvaluator.getIdentityType());
    }

    @Test
    public void testGetPermissionType() {
        assertEquals(AccountPermission.class, accountPermissionEvaluator.getPermissionType());
    }

    @Test
    public void testTestPermissionByIdAdmin() {
        user.setRole(ADMIN);
        assertAll(
                () -> assertTrue(accountPermissionEvaluator.testPermissionById(authentication, account.getId(), READ)),
                () -> assertTrue(accountPermissionEvaluator.testPermissionById(authentication, account.getId(), EDIT)),
                () -> assertTrue(accountPermissionEvaluator.testPermissionById(authentication, account.getId(), EDIT_ROLE)),
                () -> assertFalse(accountPermissionEvaluator.testPermissionById(authentication, account.getId(), USE_CHAT))
        );
    }

    @Test
    public void testTestPermissionByIdUserSame() {
        assertAll(
                () -> assertTrue(accountPermissionEvaluator.testPermissionById(authentication, user.getId(), READ)),
                () -> assertTrue(accountPermissionEvaluator.testPermissionById(authentication, user.getId(), EDIT)),
                () -> assertFalse(accountPermissionEvaluator.testPermissionById(authentication, user.getId(), EDIT_ROLE)),
                () -> assertTrue(accountPermissionEvaluator.testPermissionById(authentication, user.getId(), USE_CHAT))
        );
    }

    @Test
    public void testTestPermissionByIdUserOther() {
        assertAll(
                () -> assertTrue(accountPermissionEvaluator.testPermissionById(authentication, account.getId(), READ)),
                () -> assertFalse(accountPermissionEvaluator.testPermissionById(authentication, account.getId(), EDIT)),
                () -> assertFalse(accountPermissionEvaluator.testPermissionById(authentication, account.getId(), EDIT_ROLE)),
                () -> assertFalse(accountPermissionEvaluator.testPermissionById(authentication, account.getId(), USE_CHAT))
        );
    }

    @Test
    public void testTestPermissionByIdAnonymous() {
        authentication = mock(AnonymousAuthenticationToken.class);
        assertAll(
                () -> assertFalse(accountPermissionEvaluator.testPermissionById(authentication, account.getId(), READ)),
                () -> assertFalse(accountPermissionEvaluator.testPermissionById(authentication, account.getId(), EDIT)),
                () -> assertFalse(accountPermissionEvaluator.testPermissionById(authentication, account.getId(), EDIT_ROLE)),
                () -> assertFalse(accountPermissionEvaluator.testPermissionById(authentication, account.getId(), USE_CHAT))
        );
    }

    @TestConfiguration
    static class AccountPermissionEvaluatorTestConfiguration {

        @Bean
        public AccountPermissionEvaluator accountPermissionEvaluator() {
            return new AccountPermissionEvaluator();
        }

    }

}
