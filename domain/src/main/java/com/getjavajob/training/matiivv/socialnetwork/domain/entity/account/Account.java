package com.getjavajob.training.matiivv.socialnetwork.domain.entity.account;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.getjavajob.training.matiivv.socialnetwork.domain.constraint.EmailAddress;
import com.getjavajob.training.matiivv.socialnetwork.domain.constraint.Unique;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Anchor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static javax.persistence.EnumType.STRING;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
@ToString(callSuper = true)
@Unique(message = "{validation.uniqueAccount.account.emailAddress}", groups = Account.ValidInfo.class)
@Entity
@Table(name = "acc_account")
public class Account extends Anchor<Long> {

    @EqualsAndHashCode.Include
    @NotNull(message = "{validation.empty.account.emailAddress}", groups = ValidInfo.class)
    @Size(max = 50, message = "{validation.size.account.emailAddress}", groups = ValidInfo.class)
    @EmailAddress(message = "{validation.emailAddress.account.emailAddress}", groups = ValidInfo.class)
    @Column(name = "email_address")
    private String emailAddress;

    @ToString.Exclude
    @NotNull(message = "{validation.empty.account.password}", groups = ValidOldPassword.class)
    @Size(min = 5, max = 72, message = "{validation.size.account.password}", groups = ValidOldPassword.class)
    @Transient
    private String oldPassword;

    @ToString.Exclude
    @NotNull(message = "{validation.empty.account.password}", groups = ValidPassword.class)
    @Size(min = 5, max = 72, message = "{validation.size.account.password}", groups = ValidPassword.class)
    @Transient
    private String rawPassword;

    @JsonIgnore
    @ToString.Exclude
    @Column(name = "password")
    private String password;

    @NotNull(message = "{validation.empty.account.role}", groups = ValidRole.class)
    @Enumerated(STRING)
    @Column(name = "role")
    private Role role;

    @NotNull(message = "{validation.empty.account.firstName}", groups = ValidInfo.class)
    @Size(min = 2, max = 50, message = "{validation.size.account.firstName}", groups = ValidInfo.class)
    @Column(name = "first_name")
    private String firstName;

    @NotNull(message = "{validation.empty.account.lastName}", groups = ValidInfo.class)
    @Size(min = 2, max = 50, message = "{validation.size.account.lastName}", groups = ValidInfo.class)
    @Column(name = "last_name")
    private String lastName;

    @Size(max = 50, message = "{validation.size.account.paternalName}", groups = ValidInfo.class)
    @Column(name = "paternal_name")
    private String paternalName;

    @NotNull(message = "{validation.empty.account.birthday}", groups = ValidInfo.class)
    @Column(name = "birthday")
    private LocalDate birthday;

    @Column(name = "image_id")
    private String imageId;

    @Valid
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "country", column = @Column(name = "personal_country")),
            @AttributeOverride(name = "city", column = @Column(name = "personal_city")),
            @AttributeOverride(name = "street", column = @Column(name = "personal_street"))
    })
    private Address personalAddress;

    @Valid
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "country", column = @Column(name = "business_country")),
            @AttributeOverride(name = "city", column = @Column(name = "business_city")),
            @AttributeOverride(name = "street", column = @Column(name = "business_street"))
    })
    private Address businessAddress;

    @JacksonXmlElementWrapper(localName = "phones")
    @JacksonXmlProperty(localName = "phone")
    @Valid
    @ElementCollection
    @CollectionTable(
            name = "acc_phn_account_phone",
            joinColumns = @JoinColumn(name = "acc_account")
    )
    @OrderColumn(name = "index")
    private List<Phone> phones;

    @Size(max = 50, message = "{validation.size.account.skype}", groups = ValidInfo.class)
    @Column(name = "skype")
    private String skype;

    @Size(max = 500, message = "{validation.size.account.additionalInfo}", groups = ValidInfo.class)
    @Column(name = "additional_info")
    private String additionalInfo;

    @NotNull(message = "{validation.empty.account.locale}", groups = ValidInfo.class)
    @Size(min = 2, max = 5, message = "{validation.size.account.locale}", groups = ValidInfo.class)
    @Column(name = "locale")
    private String locale;

    @JsonIgnore
    @Column(name = "session")
    private String session;

    @CreatedDate
    @Column(name = "registered_at", updatable = false)
    private LocalDateTime registeredAt;

    @Column(name = "deleted")
    private boolean deleted;

    @Transient
    private long newMessageCount;

    public Account(Long id) {
        super(id);
    }

    public enum Role {

        USER,
        ADMIN,

    }

    public interface ValidInfo {
    }

    public interface ValidOldPassword {
    }

    public interface ValidPassword {
    }

    public interface ValidRole {
    }

    public interface ValidRegistration extends ValidInfo, ValidPassword {
    }

}
