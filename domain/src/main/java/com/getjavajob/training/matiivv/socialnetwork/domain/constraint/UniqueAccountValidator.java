package com.getjavajob.training.matiivv.socialnetwork.domain.constraint;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;

import javax.validation.ConstraintValidator;

public interface UniqueAccountValidator extends ConstraintValidator<Unique, Account> {
}
