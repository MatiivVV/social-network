package com.getjavajob.training.matiivv.socialnetwork.domain.projection.group;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import lombok.Value;

import java.time.LocalDateTime;

@Value
public class AccountMembershipSummary {

    private long id;
    private String name;
    private String imageId;
    private String description;
    private boolean deleted;
    private Account.Role role;
    private Status status;
    private LocalDateTime changedAt;

}
