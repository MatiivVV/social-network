package com.getjavajob.training.matiivv.socialnetwork.domain.entity.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Message;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

import static javax.persistence.FetchType.LAZY;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@ToString(callSuper = true)
@Entity
@Table(name = "cms_chat_message")
public class ChatMessage extends Message {

    @ToString.Exclude
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "acc_recipient")
    private Account recipient;

    @ToString.Exclude
    @ManyToOne(fetch = LAZY)
    @JoinColumns({
            @JoinColumn(name = "acc_author", referencedColumnName = "acc_opponent", insertable = false, updatable = false),
            @JoinColumn(name = "acc_recipient", referencedColumnName = "acc_account", insertable = false, updatable = false)
    })
    private Chat recipientChat;

    @Column(name = "read")
    private boolean read;

    public ChatMessage(Long id) {
        super(id);
    }

}
