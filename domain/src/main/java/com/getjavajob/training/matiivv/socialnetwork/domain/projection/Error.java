package com.getjavajob.training.matiivv.socialnetwork.domain.projection;

import lombok.Value;

import java.util.List;

@Value
public class Error {

    private String code;
    private String message;
    private List<ConstraintViolation> constraintViolations;

}
