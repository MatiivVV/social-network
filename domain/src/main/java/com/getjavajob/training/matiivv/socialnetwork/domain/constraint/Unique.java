package com.getjavajob.training.matiivv.socialnetwork.domain.constraint;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static com.getjavajob.training.matiivv.socialnetwork.domain.constraint.Unique.List;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(TYPE)
@Retention(RUNTIME)
@Repeatable(List.class)
@Documented
@Constraint(validatedBy = {UniqueAccountValidator.class, UniqueGroupValidator.class})
@ReportAsSingleViolation
public @interface Unique {

    String message() default "{com.getjavajob.training.web0710.matiivv.socialnetwork.common.constraint.Unique.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    @Target(TYPE)
    @Retention(RUNTIME)
    @Documented
    @interface List {

        Unique[] value();

    }

}
