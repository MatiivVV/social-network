package com.getjavajob.training.matiivv.socialnetwork.domain.projection;

import lombok.Value;

@Value
public class ConstraintViolation {

    private String field;
    private String code;
    private String message;

}
