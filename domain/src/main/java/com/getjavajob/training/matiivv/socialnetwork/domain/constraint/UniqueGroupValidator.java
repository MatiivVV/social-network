package com.getjavajob.training.matiivv.socialnetwork.domain.constraint;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Group;

import javax.validation.ConstraintValidator;

public interface UniqueGroupValidator extends ConstraintValidator<Unique, Group> {
}
