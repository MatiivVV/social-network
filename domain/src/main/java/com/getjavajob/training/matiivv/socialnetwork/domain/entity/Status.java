package com.getjavajob.training.matiivv.socialnetwork.domain.entity;

public enum Status {

    PENDING,
    ACCEPTED,
    DECLINED

}
