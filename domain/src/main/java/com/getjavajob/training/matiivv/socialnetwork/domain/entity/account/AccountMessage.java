package com.getjavajob.training.matiivv.socialnetwork.domain.entity.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Message;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import static javax.persistence.FetchType.LAZY;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@ToString(callSuper = true)
@Entity
@Table(name = "ams_account_message")
public class AccountMessage extends Message {

    @ToString.Exclude
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "acc_account")
    private Account account;

    public AccountMessage(Long id) {
        super(id);
    }

}
