package com.getjavajob.training.matiivv.socialnetwork.domain.entity.group;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.getjavajob.training.matiivv.socialnetwork.domain.constraint.Unique;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Anchor;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

import static javax.persistence.FetchType.LAZY;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
@ToString(callSuper = true)
@Unique(message = "{validation.uniqueGroup.group.name}")
@Entity
@Table(name = "grp_group")
public class Group extends Anchor<Long> {

    @EqualsAndHashCode.Include
    @NotNull(message = "{validation.empty.group.name}")
    @Size(min = 2, max = 50, message = "{validation.size.group.name}")
    @Column(name = "name")
    private String name;

    @Column(name = "image_id")
    private String imageId;

    @Size(max = 500, message = "{validation.size.group.description}")
    @Column(name = "description")
    private String description;

    @JsonIgnore
    @ToString.Exclude
    @CreatedBy
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "acc_creator", updatable = false)
    private Account creator;

    @CreatedDate
    @Column(name = "created_at", updatable = false)
    private LocalDateTime createdAt;

    @Column(name = "deleted")
    private boolean deleted;

    public Group(Long id) {
        super(id);
    }

}
