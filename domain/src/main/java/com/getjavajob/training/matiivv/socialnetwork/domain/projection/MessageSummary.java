package com.getjavajob.training.matiivv.socialnetwork.domain.projection;

import lombok.Value;

import java.time.LocalDateTime;

@Value
public class MessageSummary {

    private long id;
    private long authorId;
    private String authorFirstName;
    private String authorLastName;
    private String authorImageId;
    private boolean authorDeleted;
    private String message;
    private String imageId;
    private LocalDateTime postedAt;

}
