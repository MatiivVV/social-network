package com.getjavajob.training.matiivv.socialnetwork.domain.entity;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

import static javax.persistence.FetchType.LAZY;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
@ToString(callSuper = true)
@MappedSuperclass
public abstract class Message extends Anchor<Long> {

    @ToString.Exclude
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "acc_author")
    private Account author;

    @EqualsAndHashCode.Include
    @NotNull(message = "{validation.empty.message.message}")
    @Size(max = 500, message = "{validation.size.message.message}")
    @Column(name = "message")
    private String message;

    @EqualsAndHashCode.Include
    @Column(name = "image_id")
    private String imageId;

    @CreatedDate
    @Column(name = "posted_at", updatable = false)
    private LocalDateTime postedAt;

    public Message(Long id) {
        super(id);
    }

}
