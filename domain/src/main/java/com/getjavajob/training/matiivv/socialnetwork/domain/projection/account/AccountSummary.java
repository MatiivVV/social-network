package com.getjavajob.training.matiivv.socialnetwork.domain.projection.account;

import lombok.Value;

@Value
public class AccountSummary {

    private long id;
    private String firstName;
    private String lastName;
    private String imageId;
    private String country;
    private String city;

}
