package com.getjavajob.training.matiivv.socialnetwork.domain.entity.group;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Message;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import static javax.persistence.FetchType.LAZY;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@ToString(callSuper = true)
@Entity
@Table(name = "gms_group_message")
public class GroupMessage extends Message {

    @ToString.Exclude
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "grp_group")
    private Group group;

    public GroupMessage(Long id) {
        super(id);
    }

}
