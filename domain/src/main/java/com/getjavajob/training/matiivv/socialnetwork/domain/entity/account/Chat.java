package com.getjavajob.training.matiivv.socialnetwork.domain.entity.account;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Association;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

import static javax.persistence.FetchType.LAZY;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
@ToString(callSuper = true)
@Entity
@Table(name = "acc_account_acc_opponent")
public class Chat extends Association<Chat.Id> {

    @SuppressWarnings("JpaModelReferenceInspection")
    @ToString.Exclude
    @MapsId("accountId")
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "acc_account")
    private Account account;

    @SuppressWarnings("JpaModelReferenceInspection")
    @ToString.Exclude
    @MapsId("opponentId")
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "acc_opponent")
    private Account opponent;

    @ToString.Exclude
    @OneToMany(mappedBy = "recipientChat")
    private List<ChatMessage> inMessages;

    @CreatedDate
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    public Chat(Id id) {
        super(id);
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Embeddable
    public static class Id implements Association.Identity {

        private Long accountId;
        private Long opponentId;

        @JsonIgnore
        @Override
        public boolean isNew() {
            return accountId == null || opponentId == null;
        }

    }

}
