package com.getjavajob.training.matiivv.socialnetwork.domain.entity.group;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Association;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import lombok.*;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.FetchType.LAZY;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@ToString(callSuper = true)
@Entity
@Table(name = "grp_group_acc_member")
public class Membership extends Association<Membership.Id> {

    @SuppressWarnings("JpaModelReferenceInspection")
    @ToString.Exclude
    @MapsId("groupId")
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "grp_group")
    private Group group;

    @SuppressWarnings("JpaModelReferenceInspection")
    @ToString.Exclude
    @MapsId("memberId")
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "acc_member")
    private Account member;

    @NotNull(message = "{validation.empty.membership.role}", groups = ValidRole.class)
    @Enumerated(STRING)
    @Column(name = "role")
    private Account.Role role;

    @NotNull(message = "{validation.empty.membership.status}", groups = ValidStatus.class)
    @Enumerated(STRING)
    @Column(name = "status")
    private Status status;

    @LastModifiedDate
    @Column(name = "changed_at")
    private LocalDateTime changedAt;

    public Membership(Id id) {
        super(id);
    }

    public interface ValidRole {
    }

    public interface ValidStatus {
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Embeddable
    public static class Id implements Association.Identity {

        private Long groupId;
        private Long memberId;

        @JsonIgnore
        @Override
        public boolean isNew() {
            return groupId == null || memberId == null;
        }

    }

}
