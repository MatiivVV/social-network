package com.getjavajob.training.matiivv.socialnetwork.domain.projection.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status;
import lombok.Value;

import java.time.LocalDateTime;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Friendship.Type;

@Value
public class FriendshipSummary {

    private long id;
    private String firstName;
    private String lastName;
    private String imageId;
    private String country;
    private String city;
    private boolean deleted;
    private Type type;
    private Status status;
    private LocalDateTime changedAt;

}
