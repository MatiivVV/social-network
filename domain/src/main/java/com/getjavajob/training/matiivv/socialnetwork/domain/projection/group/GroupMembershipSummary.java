package com.getjavajob.training.matiivv.socialnetwork.domain.projection.group;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import lombok.Value;

import java.time.LocalDateTime;

@Value
public class GroupMembershipSummary {

    private long id;
    private String firstName;
    private String lastName;
    private String imageId;
    private String country;
    private String city;
    private boolean deleted;
    private Account.Role role;
    private Status status;
    private LocalDateTime changedAt;

}
