package com.getjavajob.training.matiivv.socialnetwork.domain.constraint;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.Pattern;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static javax.validation.constraints.Pattern.Flag.CASE_INSENSITIVE;

@Target({FIELD, ANNOTATION_TYPE, PARAMETER})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {})
@Pattern(
        regexp = "^[a-z0-9`!#$%^&*'{}?/+=|_~-]+(\\.[a-z0-9`!#$%^&*'{}?/+=" +
                "|_~-]+)*@([a-z0-9]([a-z0-9-]*[a-z0-9])?)+(\\.[a-z0-9]" +
                "([a-z0-9-]*[a-z0-9])?)*$",
        flags = {CASE_INSENSITIVE}
)
@ReportAsSingleViolation
public @interface EmailAddress {

    String message() default "{com.getjavajob.training.web0710.matiivv.socialnetwork.common.constraint.EmailAddress.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    @Target({METHOD, FIELD, PARAMETER})
    @Retention(RUNTIME)
    @Documented
    @interface List {

        EmailAddress[] value();

    }

}
