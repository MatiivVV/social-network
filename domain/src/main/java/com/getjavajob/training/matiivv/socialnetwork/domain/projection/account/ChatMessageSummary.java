package com.getjavajob.training.matiivv.socialnetwork.domain.projection.account;

import lombok.Value;

import java.time.LocalDateTime;

@Value
public class ChatMessageSummary {

    private long id;
    private long authorId;
    private long recipientId;
    private String message;
    private String imageId;
    private LocalDateTime postedAt;
    private boolean read;

}
