package com.getjavajob.training.matiivv.socialnetwork.domain.projection.account;

import lombok.Value;

import java.time.LocalDateTime;

@Value
public class ChatSummary {

    private long opponentId;
    private String opponentFirstName;
    private String opponentLastName;
    private String opponentImageId;
    private boolean opponentDeleted;
    private String lastMessage;
    private LocalDateTime changedAt;
    private long newMessageCount;

}
