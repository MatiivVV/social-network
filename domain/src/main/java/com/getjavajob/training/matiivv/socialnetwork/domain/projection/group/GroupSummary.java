package com.getjavajob.training.matiivv.socialnetwork.domain.projection.group;

import lombok.Value;

@Value
public class GroupSummary {

    private long id;
    private String name;
    private String imageId;
    private String description;

}
