package com.getjavajob.training.matiivv.socialnetwork.domain.entity.account;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.validation.constraints.Size;

@Data
@Embeddable
public class Address {

    @Size(max = 50, message = "{validation.size.account.address.country}", groups = Account.ValidInfo.class)
    private String country;
    @Size(max = 50, message = "{validation.size.account.address.city}", groups = Account.ValidInfo.class)
    private String city;
    @Size(max = 50, message = "{validation.size.account.address.street}", groups = Account.ValidInfo.class)
    private String street;

}
