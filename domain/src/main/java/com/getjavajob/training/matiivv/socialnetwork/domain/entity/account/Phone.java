package com.getjavajob.training.matiivv.socialnetwork.domain.entity.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.constraint.PhoneNumber;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static javax.persistence.EnumType.STRING;

@Data
@Embeddable
public class Phone {

    @NotNull(message = "{validation.empty.account.phone.number}", groups = Account.ValidInfo.class)
    @Size(max = 50, message = "{validation.size.account.phone.number}", groups = Account.ValidInfo.class)
    @PhoneNumber(message = "{validation.phoneNumber.account.phone.number}", groups = Account.ValidInfo.class)
    @Column(name = "number")
    private String number;

    @NotNull(message = "{validation.empty.account.phone.type}", groups = Account.ValidInfo.class)
    @Enumerated(STRING)
    @Column(name = "type")
    private Type type;

    public enum Type {

        PERSONAL,
        BUSINESS

    }

}
