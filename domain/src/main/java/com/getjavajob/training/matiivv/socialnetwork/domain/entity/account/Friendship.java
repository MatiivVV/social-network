package com.getjavajob.training.matiivv.socialnetwork.domain.entity.account;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Association;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status;
import lombok.*;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.FetchType.LAZY;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@ToString(callSuper = true)
@Entity
@Table(name = "acc_account_acc_friend")
public class Friendship extends Association<Friendship.Id> {

    @SuppressWarnings("JpaModelReferenceInspection")
    @ToString.Exclude
    @MapsId("accountId")
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "acc_account")
    private Account account;

    @SuppressWarnings("JpaModelReferenceInspection")
    @ToString.Exclude
    @MapsId("friendId")
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "acc_friend")
    private Account friend;

    @Enumerated(STRING)
    @Column(name = "type")
    private Type type;

    @NotNull(message = "{validation.empty.friendship.status}")
    @Enumerated(STRING)
    @Column(name = "status")
    private Status status;

    @LastModifiedDate
    @Column(name = "changed_at")
    private LocalDateTime changedAt;

    public Friendship(Id id) {
        super(id);
    }

    public enum Type {

        REQUEST,
        RESPONSE

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Embeddable
    public static class Id implements Association.Identity {

        private Long accountId;
        private Long friendId;

        @JsonIgnore
        @Override
        public boolean isNew() {
            return accountId == null || friendId == null;
        }

    }

}
