package com.getjavajob.training.matiivv.socialnetwork.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Persistable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EmbeddedId;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.Association.Identity;

@Data
@NoArgsConstructor
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class Association<ID extends Identity> implements Persistable<ID> {

    @EmbeddedId
    private ID id;

    public Association(ID id) {
        this.id = id;
    }

    @JsonIgnore
    @Override
    public boolean isNew() {
        return id == null || id.isNew();
    }

    public interface Identity extends Serializable {

        boolean isNew();

    }

}
