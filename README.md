# Yet Another Social Network  
  
** Functionality: **  
  
+ Register  
+ Login/logout/reset session  
+ Switch language  
+ Search accounts/groups with ajax pagination  
+ Display/edit/delete account  
+ Upload and download avatar  
+ Export/import account info with xml  
+ Send/accept/decline/delete friendship request  
+ Display account friends (friendship requests) with ajax pagination  
+ Display account groups (membership requests) with ajax pagination  
+ Display account public messages with ajax pagination  
+ Send/delete account public messsage  
+ Display chats and chat messages with ajax pagination  
+ Send/recieve chat message real-time  
+ Create/display/edit/delete group  
+ Upload and download group image  
+ Send/accept/decline/delete membership request  
+ Display group members (membership requests) with ajax pagination  
+ Display group public messages with ajax pagination  
+ Send/delete group public messsage  
  
** Tools: **  
Back end: JDK 11, Spring 5.2.3, Spring Boot 2.2.4, REST API + STOMP / Spring MVC 5.2.3 / Servlet API 4.0 + WebSocket API 1.1 / Tomcat 9.0.30, OAuth 2.0 + JWT / Spring Security 5.2.1 + Spring OAuth 2 2.3.8 + Spring Security JWT 1.0.11, Spring Data JPA 2.2.4 / JPA 2.2.3 / Hibernate 5.4.10, Validation API 2.0.2 / Hibernate Validator 6.0.18, Jackson 2.10.2, Lombok 1.18.10, Amazon AWS S3 SDK 1.11.723, JUnit 5.5.2, Mockito 3.1.0, H2 1.4.200, PostgreSQL 10, Maven 3.6.3, Heroku Maven Plugin 2.0.16, Git / Bitbucket, IntelliJIDEA 2019.3.4  
  
Front end: ES 6, Create React App 3.0.1, React 16.3.0, Redux 4.0.5, React Redux 7.2.0, Redux Thunk 2.3.0, React Router 5.1.2, Reactstrap 8.4.1 / Bootstrap 4.4.1, StompJS 2.3.3 / SockJS 1.4.0, i18next 17.3.1, Fort Awesome 5.12.1  
  
** Notes: **  
SQL DDL is located in the `dao\src\test\resources\schema.sql`  
  
** Application: **  
https://matiivv-social-network.herokuapp.com  
  
** Screenshots **  
https://gyazo.com/f140ef7c98cfa73332e1fbffc41c47b1  
https://gyazo.com/01f8b40bee07bf8f24bb24c65d3ceff1  
https://gyazo.com/ae5891e74022e32d6a0f3017a398e5ae  
https://gyazo.com/ce538a7e11920b655e522e64ed808948  
https://gyazo.com/941f9793bfb7b2ec2985790c482b06bd  
https://gyazo.com/8740b0f7bf399acd7f5d16041068278a  
https://gyazo.com/79739470b82e38a240a169de2a3e8e1c  
https://gyazo.com/1c1d82466757fac1c89bf1c68d1b041c  
https://gyazo.com/91c5723b46b5c1fb48b8bd35f896f476  
  
_  
**Matiiv Vasily**  
Training getJavaJob  
http://www.getjavajob.com  