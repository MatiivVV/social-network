INSERT INTO acc_account (
  id,
  email_address,
  password,
  role,
  first_name,
  last_name,
  paternal_name,
  birthday,
  image_id,
  personal_country,
  personal_city,
  personal_street,
  business_country,
  business_city,
  business_street,
  skype,
  additional_info,
  locale,
  session,
  registered_at,
  deleted
) VALUES (
  1,
  'email1',
  'password1',
  'ADMIN',
  'firstName1',
  'lastName1',
  'paternalName1',
  DATE '1990-01-01',
  'image1',
  'personalCountry1',
  'personalCity1',
  'personalStreet1',
  'businessCountry1',
  'businessCity1',
  'businessStreet1',
  'skype1',
  'additionalInfo1',
  'en',
  'session1',
  TIMESTAMP '2019-03-28 00:00:00',
  0
),(
  2,
  'email2',
  'password2',
  'ADMIN',
  'firstName2',
  'lastName2',
  'paternalName2',
  DATE '1990-01-01',
  'image2',
  'personalCountry2',
  'personalCity2',
  'personalStreet2',
  'businessCountry2',
  'businessCity2',
  'businessStreet2',
  'skype2',
  'additionalInfo2',
  'en',
  'session2',
  TIMESTAMP '2019-03-28 00:00:00',
  0
),(
  3,
  'email3',
  'password3',
  'ADMIN',
  'other',
  'other',
  'paternalName3',
  DATE '1990-01-01',
  'image3',
  'personalCountry3',
  'personalCity3',
  'personalStreet3',
  'businessCountry3',
  'businessCity3',
  'businessStreet3',
  'skype3',
  'additionalInfo3',
  'en',
  'session3',
  TIMESTAMP '2019-03-28 00:00:00',
  0
);

INSERT INTO acc_phn_account_phone (acc_account, index, number, type)
VALUES (1, 1, 'number1', 'BUSINESS'),
  (1, 2, 'number2', 'PERSONAL'),
  (2, 1, 'number3', 'BUSINESS');

INSERT INTO acc_account_acc_friend (acc_account, acc_friend, type, status, changed_at)
VALUES (1, 2, 'REQUEST', 'ACCEPTED', TIMESTAMP '2019-03-28 00:00:00'),
  (2, 1, 'RESPONSE', 'ACCEPTED', TIMESTAMP '2019-03-28 00:00:00'),
  (3, 1, 'REQUEST', 'ACCEPTED', TIMESTAMP '2019-03-28 01:00:00'),
  (1, 3, 'RESPONSE', 'ACCEPTED', TIMESTAMP '2019-03-28 01:00:00'),
  (3, 2, 'REQUEST', 'PENDING', TIMESTAMP '2019-03-28 02:00:00');

INSERT INTO ams_account_message (id, acc_author, acc_account, message, image_id, posted_at)
VALUES (1, 1, 2, 'message1', 'image1', TIMESTAMP '2019-03-28 00:00:00'),
  (2, 1, 2, 'message2', 'image2', TIMESTAMP '2019-03-28 01:00:00'),
  (3, 2, 1, 'message3', 'image3', TIMESTAMP '2019-03-28 02:00:00');

INSERT INTO acc_account_acc_opponent (acc_account, acc_opponent, created_at)
VALUES (1, 2, TIMESTAMP '2019-03-28 00:00:00'),
  (2, 1, TIMESTAMP '2019-03-28 00:00:00'),
  (3, 1, TIMESTAMP '2019-03-28 01:00:00'),
  (1, 3, TIMESTAMP '2019-03-28 01:00:00'),
  (3, 2, TIMESTAMP '2019-03-28 02:00:00');

INSERT INTO cms_chat_message (id, acc_author, acc_recipient, message, image_id, posted_at, read)
VALUES (1, 1, 2, 'message1', 'image1', TIMESTAMP '2019-03-28 00:00:00', 1),
  (2, 1, 2, 'message2', 'image2', TIMESTAMP '2019-03-28 01:00:00', 0),
  (3, 2, 1, 'message3', 'image3', TIMESTAMP '2019-03-28 02:00:00', 0),
  (4, 3, 1, 'message4', 'image4', TIMESTAMP '2019-03-28 03:00:00', 0);

INSERT INTO grp_group (id, name, image_id, description, acc_creator, created_at, deleted)
VALUES (1, 'group1', 'image1', 'description1', 1, TIMESTAMP '2019-03-28 00:00:00', 0),
  (2, 'group2', 'image2', 'description2', 1, TIMESTAMP '2019-03-28 00:00:00', 0),
  (3, 'other', 'image3', 'description3', 2, TIMESTAMP '2019-03-28 00:00:00', 0);

INSERT INTO grp_group_acc_member (grp_group, acc_member, role, status, changed_at)
VALUES (1, 1, 'ADMIN', 'ACCEPTED', TIMESTAMP '2019-03-28 00:00:00'),
  (1, 2, 'USER', 'ACCEPTED', TIMESTAMP '2019-03-28 01:00:00'),
  (1, 3, 'USER', 'PENDING', TIMESTAMP '2019-03-28 02:00:00'),
  (2, 1, 'ADMIN', 'ACCEPTED', TIMESTAMP '2019-03-28 03:00:00'),
  (3, 1, 'USER', 'PENDING', TIMESTAMP '2019-03-28 04:00:00');

INSERT INTO gms_group_message (id, acc_author, grp_group, message, image_id, posted_at)
VALUES (1, 1, 1, 'message1', 'image1', TIMESTAMP '2019-03-28 00:00:00'),
  (2, 1, 2, 'message2', 'image2', TIMESTAMP '2019-03-28 01:00:00'),
  (3, 2, 1, 'message3', 'image3', TIMESTAMP '2019-03-28 02:00:00');