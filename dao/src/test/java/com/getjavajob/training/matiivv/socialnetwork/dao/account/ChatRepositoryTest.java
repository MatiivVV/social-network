package com.getjavajob.training.matiivv.socialnetwork.dao.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Chat;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.ChatSummary;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Chat.Id;
import static java.time.LocalDateTime.now;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.data.domain.PageRequest.of;

@DataJpaTest
public class ChatRepositoryTest {

    @Autowired
    private ChatRepository chatRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private TestEntityManager entityManager;
    private Chat chat;

    @BeforeEach
    public void setUp() {
        chat = new Chat(new Id());
        chat.setAccount(accountRepository.getOne(2L));
        chat.setOpponent(accountRepository.getOne(3L));
        chat.setCreatedAt(now());
    }

    @Test
    public void testExistsById() {
        assertTrue(chatRepository.existsById(new Id(1L, 2L)));
    }

    @Test
    public void testExistsByIdMissing() {
        assertFalse(chatRepository.existsById(new Id(2L, 3L)));
    }

    @Test
    public void testFindById() {
        assertTrue(chatRepository.findById(new Id(1L, 2L)).isPresent());
    }

    @Test
    public void testFindByIdMissing() {
        assertFalse(chatRepository.findById(new Id(2L, 3L)).isPresent());
    }

    @Test
    public void testFindByAccountIdOnePage() {
        Pageable pageable = of(0, 100);
        Page<ChatSummary> results = chatRepository.findByAccountId(1L, pageable);
        assertNotNull(results);
        assertAll(
                () -> assertEquals(2L, results.getTotalElements()),
                () -> assertEquals(1, results.getTotalPages()),
                () -> assertEquals(2, results.getNumberOfElements()),
                () -> assertThat(results.stream()
                        .map(ChatSummary::getOpponentId)
                        .collect(toList()), hasItems(2L, 3L)),
                () -> assertThat(results.stream()
                        .map(ChatSummary::getLastMessage)
                        .collect(toList()), hasItems("message3", "message4"))
        );
    }

    @Test
    public void testFindByAccountIdManyPages() {
        Pageable pageable = of(0, 1);
        Page<ChatSummary> results = chatRepository.findByAccountId(1L, pageable);
        assertAll(
                () -> assertNotNull(results),
                () -> assertEquals(2L, results.getTotalElements()),
                () -> assertEquals(2, results.getTotalPages()),
                () -> assertEquals(1, results.getNumberOfElements()),
                () -> assertThat(results.stream().map(ChatSummary::getOpponentId).collect(toList()), hasItems(3L)),
                () -> assertThat(results.stream().map(ChatSummary::getLastMessage).collect(toList()), hasItems("message4"))
        );
    }

    @Test
    public void testCreate() {
        chatRepository.saveAndFlush(chat);
        entityManager.clear();
        assertNotNull(entityManager.find(Chat.class, chat.getId()));
    }

    @Test
    public void testCreateExisting() {
        assertThrows(DataIntegrityViolationException.class, () -> {
            chatRepository.saveAndFlush(chat);
            entityManager.clear();
            chat.setId(new Id());
            chat.setAccount(accountRepository.getOne(2L));
            chat.setOpponent(accountRepository.getOne(3L));
            chatRepository.saveAndFlush(chat);
        });
    }

    @Test
    public void testDeleteById() {
        chatRepository.saveAndFlush(chat);
        entityManager.clear();
        chatRepository.deleteById(chat.getId());
        entityManager.flush();
        entityManager.clear();
        assertNull(entityManager.find(Chat.class, chat.getId()));
    }

    @Test
    public void testDeleteByIdMissing() {
        assertThrows(EmptyResultDataAccessException.class, () -> chatRepository.deleteById(new Id(2L, 3L)));
    }

}