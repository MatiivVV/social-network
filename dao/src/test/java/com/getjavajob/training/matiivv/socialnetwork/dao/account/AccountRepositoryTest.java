package com.getjavajob.training.matiivv.socialnetwork.dao.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Address;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Phone;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.AccountSummary;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.Optional;
import java.util.stream.Stream;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.Role.ADMIN;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Phone.Type.BUSINESS;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Phone.Type.PERSONAL;
import static java.time.LocalDateTime.now;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.data.domain.PageRequest.of;

@DataJpaTest
public class AccountRepositoryTest {

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private TestEntityManager entityManager;
    private Account account;

    @BeforeEach
    public void setUp() {
        account = new Account();
        account.setEmailAddress("email");
        account.setPassword("password");
        account.setRole(ADMIN);
        account.setFirstName("firstName");
        account.setLastName("lastName");
        account.setPaternalName("paternalName");
        account.setBirthday(LocalDate.now());
        account.setImageId("image");
        var personalAddress = new Address();
        personalAddress.setCountry("personalCountry");
        personalAddress.setCity("personalCity");
        personalAddress.setStreet("personalStreet");
        account.setPersonalAddress(personalAddress);
        var businessAddress = new Address();
        businessAddress.setCountry("businessCountry");
        businessAddress.setCity("businessCity");
        businessAddress.setStreet("businessStreet");
        account.setBusinessAddress(businessAddress);
        var phone1 = new Phone();
        phone1.setNumber("number1");
        phone1.setType(PERSONAL);
        var phone2 = new Phone();
        phone2.setNumber("number2");
        phone2.setType(BUSINESS);
        var phone3 = new Phone();
        phone3.setNumber("number3");
        phone3.setType(BUSINESS);
        account.setPhones(Stream.of(phone1, phone2, phone3).collect(toList()));
        account.setSkype("skype");
        account.setAdditionalInfo("additionalInfo");
        account.setLocale("en");
        account.setSession("session");
        account.setRegisteredAt(now());
        account.setDeleted(true);
    }

    @Test
    public void testExistsById() {
        assertTrue(accountRepository.existsById(2L));
    }

    @Test
    public void testExistsByIdMissing() {
        assertFalse(accountRepository.existsById(99L));
    }

    @Test
    public void testExistsByEmailAddress() {
        assertTrue(accountRepository.existsByEmailAddress("email2"));
    }

    @Test
    public void testExistsByEmailAddressMissing() {
        assertFalse(accountRepository.existsByEmailAddress("email99"));
    }

    @Test
    public void testExistsByEmailAddressAndIdNot() {
        assertTrue(accountRepository.existsByEmailAddressAndIdNot("email2", 1L));
    }

    @Test
    public void testExistsByEmailAddressAndIdNotMissing() {
        assertFalse(accountRepository.existsByEmailAddressAndIdNot("email2", 2L));
    }

    @Test
    public void testFindById() {
        Optional<Account> result = accountRepository.findById(2L);
        assertTrue(result.isPresent());
        assertEquals("email2", result.get().getEmailAddress());
    }

    @Test
    public void testFindByIdMissing() {
        assertFalse(accountRepository.findById(99L).isPresent());
    }

    @Test
    public void testFindByEmailAddress() {
        Optional<Account> result = accountRepository.findByEmailAddress("email2");
        assertTrue(result.isPresent());
        assertEquals(2L, result.get().getId());
    }

    @Test
    public void testFindByEmailAddressMissing() {
        assertFalse(accountRepository.findByEmailAddress("email99").isPresent());
    }

    @Test
    public void testFindByNamePatternOnePage() {
        Pageable pageable = of(0, 100);
        Page<AccountSummary> results = accountRepository.findByNamePattern("lastName firstName", pageable);
        assertNotNull(results);
        assertAll(
                () -> assertEquals(2L, results.getTotalElements()),
                () -> assertEquals(1, results.getTotalPages()),
                () -> assertEquals(2, results.getNumberOfElements()),
                () -> assertThat(results.stream().map(AccountSummary::getId).collect(toList()), hasItems(1L, 2L))
        );
    }

    @Test
    public void testFindByNamePatternManyPages() {
        Pageable pageable = of(0, 1);
        Page<AccountSummary> results = accountRepository.findByNamePattern("lastName firstName", pageable);
        assertNotNull(results);
        assertAll(
                () -> assertEquals(2L, results.getTotalElements()),
                () -> assertEquals(2, results.getTotalPages()),
                () -> assertEquals(1, results.getNumberOfElements()),
                () -> assertThat(results.stream().map(AccountSummary::getId).collect(toList()), hasItems(1L))
        );
    }

    @Test
    public void testCreate() {
        accountRepository.saveAndFlush(account);
        entityManager.clear();
        Account result = entityManager.find(Account.class, account.getId());
        assertNotNull(result);
        assertNotNull(result.getPhones());
        assertAll(
                () -> assertEquals(3, result.getPhones().size()),
                () -> assertThat(result
                        .getPhones()
                        .stream()
                        .map(Phone::getNumber)
                        .collect(toList()), hasItems("number1", "number2", "number3"))
        );
    }

    @Test
    public void testCreateExisting() {
        assertThrows(DataIntegrityViolationException.class, () -> {
            accountRepository.saveAndFlush(account);
            entityManager.clear();
            account.setId(null);
            accountRepository.saveAndFlush(account);
        });
    }

    @Test
    public void testUpdate() {
        entityManager.persistAndFlush(account);
        entityManager.clear();
        account.setEmailAddress("new email");
        account.getPhones().get(0).setNumber("new number");
        account.getPhones().remove(1);
        accountRepository.saveAndFlush(account);
        entityManager.clear();
        Account result = entityManager.find(Account.class, account.getId());
        assertNotNull(result);
        assertAll(
                () -> assertEquals("new email", result.getEmailAddress()),
                () -> {
                    assertNotNull(result.getPhones());
                    assertEquals(2, result.getPhones().size());
                    assertThat(result
                            .getPhones()
                            .stream()
                            .map(Phone::getNumber)
                            .collect(toList()), hasItems("new number", "number3"));
                });
    }

    @Test
    public void testDeleteById() {
        entityManager.persistAndFlush(account);
        entityManager.clear();
        accountRepository.deleteById(account.getId());
        entityManager.flush();
        entityManager.clear();
        assertNull(entityManager.find(Account.class, account.getId()));
    }

    @Test
    public void testDeleteByIdMissing() {
        assertThrows(EmptyResultDataAccessException.class, () -> accountRepository.deleteById(99L));
    }

}