package com.getjavajob.training.matiivv.socialnetwork.dao.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.AccountMessage;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.MessageSummary;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

import static java.time.LocalDateTime.now;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.data.domain.PageRequest.of;

@DataJpaTest
public class AccountMessageRepositoryTest {

    @Autowired
    private AccountMessageRepository accountMessageRepository;
    @Autowired
    private TestEntityManager entityManager;
    private AccountMessage accountMessage;

    @BeforeEach
    public void setUp() {
        accountMessage = new AccountMessage();
        accountMessage.setAuthor(new Account(1L));
        accountMessage.setMessage("message");
        accountMessage.setImageId("image");
        accountMessage.setPostedAt(now());
        accountMessage.setAccount(new Account(2L));
    }

    @Test
    public void testExistsById() {
        assertTrue(accountMessageRepository.existsById(1L));
    }

    @Test
    public void testExistsByIdMissing() {
        assertFalse(accountMessageRepository.existsById(99L));
    }

    @Test
    public void testFindById() {
        Optional<AccountMessage> result = accountMessageRepository.findById(2L);
        assertTrue(result.isPresent());
        assertEquals("message2", result.get().getMessage());
    }

    @Test
    public void testFindByIdMissing() {
        assertFalse(accountMessageRepository.findById(99L).isPresent());
    }

    @Test
    public void testFindByIdAndAccountIdAndAuthorId() {
        Optional<AccountMessage> result = accountMessageRepository.findByIdAndAccountIdAndAuthorId(2L, 2L, 1L);
        assertTrue(result.isPresent());
        assertEquals("message2", result.get().getMessage());
    }

    @Test
    public void testFindByIdAndAccountIdAndAuthorIdMissing() {
        assertFalse(accountMessageRepository.findByIdAndAccountIdAndAuthorId(2L, 1L, 1L).isPresent());
    }

    @Test
    public void testFindByAccountIdOnePage() {
        Pageable pageable = of(0, 100);
        Page<MessageSummary> results = accountMessageRepository.findByAccountId(2L, pageable);
        assertNotNull(results);
        assertAll(
                () -> assertEquals(2L, results.getTotalElements()),
                () -> assertEquals(1, results.getTotalPages()),
                () -> assertEquals(2, results.getNumberOfElements()),
                () -> assertThat(results.stream().map(MessageSummary::getId).collect(toList()), hasItems(1L, 2L))
        );
    }

    @Test
    public void testFindByAccountIdManyPages() {
        Pageable pageable = of(0, 1);
        Page<MessageSummary> results = accountMessageRepository.findByAccountId(2L, pageable);
        assertNotNull(results);
        assertAll(
                () -> assertEquals(2L, results.getTotalElements()),
                () -> assertEquals(2, results.getTotalPages()),
                () -> assertEquals(1, results.getNumberOfElements()),
                () -> assertThat(results.stream().map(MessageSummary::getId).collect(toList()), hasItems(2L))
        );
    }

    @Test
    public void testCreate() {
        accountMessageRepository.saveAndFlush(accountMessage);
        entityManager.clear();
        assertNotNull(entityManager.find(AccountMessage.class, accountMessage.getId()));
    }

    @Test
    public void testUpdate() {
        entityManager.persistAndFlush(accountMessage);
        entityManager.clear();
        accountMessage.setMessage("new message");
        accountMessageRepository.saveAndFlush(accountMessage);
        entityManager.clear();
        AccountMessage result = entityManager.find(AccountMessage.class, accountMessage.getId());
        assertNotNull(result);
        assertEquals("new message", result.getMessage());
    }

    @Test
    public void testDeleteById() {
        entityManager.persistAndFlush(accountMessage);
        entityManager.clear();
        accountMessageRepository.deleteById(accountMessage.getId());
        entityManager.flush();
        entityManager.clear();
        assertNull(entityManager.find(AccountMessage.class, accountMessage.getId()));
    }

    @Test
    public void testDeleteByIdMissing() {
        assertThrows(EmptyResultDataAccessException.class, () -> accountMessageRepository.deleteById(99L));
    }

}