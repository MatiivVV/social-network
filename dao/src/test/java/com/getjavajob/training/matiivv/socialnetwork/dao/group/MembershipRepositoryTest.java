package com.getjavajob.training.matiivv.socialnetwork.dao.group;

import com.getjavajob.training.matiivv.socialnetwork.dao.account.AccountRepository;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Membership;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.group.AccountMembershipSummary;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.group.GroupMembershipSummary;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.EnumSet;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status.ACCEPTED;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status.PENDING;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.Role;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.Role.ADMIN;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Membership.Id;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.data.domain.PageRequest.of;

@DataJpaTest
public class MembershipRepositoryTest {

    @Autowired
    private MembershipRepository membershipRepository;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private TestEntityManager entityManager;
    private Membership membership;

    @BeforeEach
    public void setUp() {
        membership = new Membership(new Id());
        membership.setGroup(groupRepository.getOne(3L));
        membership.setMember(accountRepository.getOne(2L));
        membership.setRole(ADMIN);
        membership.setStatus(PENDING);
        membership.setChangedAt(LocalDateTime.now());
    }

    @Test
    public void testExistsById() {
        assertTrue(membershipRepository.existsById(new Id(1L, 1L)));
    }

    @Test
    public void testExistsByIdMissing() {
        assertFalse(membershipRepository.existsById(new Id(3L, 3L)));
    }

    @Test
    public void testFindById() {
        assertTrue(membershipRepository.findById(new Id(1L, 1L)).isPresent());
    }

    @Test
    public void testFindByIdMissing() {
        assertFalse(membershipRepository.findById(new Id(3L, 3L)).isPresent());
    }

    @Test
    public void testSearchForGroupOnePage() {
        Pageable pageable = of(0, 100);
        Page<GroupMembershipSummary> results = membershipRepository.searchForGroup(1L,
                EnumSet.allOf(Role.class), EnumSet.of(ACCEPTED), pageable);
        assertNotNull(results);
        assertAll(
                () -> assertEquals(2L, results.getTotalElements()),
                () -> assertEquals(1, results.getTotalPages()),
                () -> assertEquals(2, results.getNumberOfElements()),
                () -> assertThat(results.stream().map(GroupMembershipSummary::getId).collect(toList()), hasItems(1L, 2L))
        );
    }

    @Test
    public void testSearchForGroupManyPages() {
        Pageable pageable = of(0, 1);
        Page<GroupMembershipSummary> results = membershipRepository.searchForGroup(1L,
                EnumSet.allOf(Role.class), EnumSet.of(ACCEPTED), pageable);
        assertNotNull(results);
        assertAll(
                () -> assertEquals(2L, results.getTotalElements()),
                () -> assertEquals(2, results.getTotalPages()),
                () -> assertEquals(1, results.getNumberOfElements()),
                () -> assertThat(results.stream().map(GroupMembershipSummary::getId).collect(toList()), hasItems(2L))
        );
    }

    @Test
    public void testSearchForAccountOnePage() {
        Pageable pageable = of(0, 100);
        Page<AccountMembershipSummary> results = membershipRepository.searchForAccount(1L,
                EnumSet.allOf(Role.class), EnumSet.of(ACCEPTED), pageable);
        assertNotNull(results);
        assertAll(
                () -> assertEquals(2L, results.getTotalElements()),
                () -> assertEquals(1, results.getTotalPages()),
                () -> assertEquals(2, results.getNumberOfElements()),
                () -> assertThat(results.stream().map(AccountMembershipSummary::getId).collect(toList()), hasItems(1L, 2L))
        );
    }

    @Test
    public void testSearchForAccountManyPages() {
        Pageable pageable = of(0, 1);
        Page<AccountMembershipSummary> results = membershipRepository.searchForAccount(1L,
                EnumSet.allOf(Role.class), EnumSet.of(ACCEPTED), pageable);
        assertNotNull(results);
        assertAll(
                () -> assertEquals(2L, results.getTotalElements()),
                () -> assertEquals(2, results.getTotalPages()),
                () -> assertEquals(1, results.getNumberOfElements()),
                () -> assertThat(results.stream().map(AccountMembershipSummary::getId).collect(toList()), hasItems(2L))
        );
    }

    @Test
    public void testCreate() {
        membershipRepository.saveAndFlush(membership);
        entityManager.clear();
        assertNotNull(entityManager.find(Membership.class, membership.getId()));
    }

    @Test
    public void testCreateExisting() {
        assertThrows(DataIntegrityViolationException.class, () -> {
            membershipRepository.saveAndFlush(membership);
            entityManager.clear();
            membership.setId(new Id());
            membership.setGroup(groupRepository.getOne(3L));
            membership.setMember(accountRepository.getOne(2L));
            membershipRepository.saveAndFlush(membership);
        });
    }

    @Test
    public void testUpdate() {
        membershipRepository.saveAndFlush(membership);
        entityManager.clear();
        membership.setStatus(ACCEPTED);
        membershipRepository.saveAndFlush(membership);
        entityManager.clear();
        Membership result = entityManager.find(Membership.class, membership.getId());
        assertNotNull(result);
        assertEquals(ACCEPTED, result.getStatus());
    }

    @Test
    public void testDeleteById() {
        membershipRepository.saveAndFlush(membership);
        entityManager.clear();
        membershipRepository.deleteById(membership.getId());
        entityManager.flush();
        entityManager.clear();
        assertNull(entityManager.find(Membership.class, membership.getId()));
    }

    @Test
    public void testDeleteByIdMissing() {
        assertThrows(EmptyResultDataAccessException.class, () -> membershipRepository.deleteById(new Id(3L, 3L)));
    }

}