package com.getjavajob.training.matiivv.socialnetwork.dao.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Friendship;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.FriendshipSummary;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.EnumSet;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status.ACCEPTED;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status.PENDING;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Friendship.Id;
import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Friendship.Type.REQUEST;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.data.domain.PageRequest.of;

@DataJpaTest
public class FriendshipRepositoryTest {

    @Autowired
    private FriendshipRepository friendshipRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private TestEntityManager entityManager;
    private Friendship friendship;

    @BeforeEach
    public void setUp() {
        friendship = new Friendship(new Id());
        friendship.setAccount(accountRepository.getOne(2L));
        friendship.setFriend(accountRepository.getOne(3L));
        friendship.setType(REQUEST);
        friendship.setStatus(PENDING);
        friendship.setChangedAt(LocalDateTime.now());
    }

    @Test
    public void testExistsById() {
        assertTrue(friendshipRepository.existsById(new Id(1L, 2L)));
    }

    @Test
    public void testExistsByIdMissing() {
        assertFalse(friendshipRepository.existsById(new Id(2L, 3L)));
    }

    @Test
    public void testFindById() {
        assertTrue(friendshipRepository.findById(new Id(1L, 2L)).isPresent());
    }

    @Test
    public void testFindByIdMissing() {
        assertFalse(friendshipRepository.findById(new Id(2L, 3L)).isPresent());
    }

    @Test
    public void testSearchOnePage() {
        Pageable pageable = of(0, 100);
        Page<FriendshipSummary> results = friendshipRepository.search(3L, EnumSet.of(REQUEST),
                EnumSet.allOf(Status.class), pageable);
        assertNotNull(results);
        assertAll(
                () -> assertEquals(2L, results.getTotalElements()),
                () -> assertEquals(1, results.getTotalPages()),
                () -> assertEquals(2, results.getNumberOfElements()),
                () -> assertThat(results.stream().map(FriendshipSummary::getId).collect(toList()), hasItems(1L, 2L))
        );
    }

    @Test
    public void testSearchManyPages() {
        Pageable pageable = of(0, 1);
        Page<FriendshipSummary> results = friendshipRepository.search(3L, EnumSet.of(REQUEST),
                EnumSet.allOf(Status.class), pageable);
        assertNotNull(results);
        assertAll(
                () -> assertEquals(2L, results.getTotalElements()),
                () -> assertEquals(2, results.getTotalPages()),
                () -> assertEquals(1, results.getNumberOfElements()),
                () -> assertThat(results.stream().map(FriendshipSummary::getId).collect(toList()), hasItems(2L))
        );
    }

    @Test
    public void testCreate() {
        friendshipRepository.saveAndFlush(friendship);
        entityManager.clear();
        assertNotNull(entityManager.find(Friendship.class, friendship.getId()));
    }

    @Test
    public void testCreateExisting() {
        assertThrows(DataIntegrityViolationException.class, () -> {
            friendshipRepository.saveAndFlush(friendship);
            entityManager.clear();
            friendship.setId(new Id());
            friendship.setAccount(accountRepository.getOne(2L));
            friendship.setFriend(accountRepository.getOne(3L));
            friendshipRepository.saveAndFlush(friendship);
        });
    }

    @Test
    public void testUpdate() {
        friendshipRepository.saveAndFlush(friendship);
        entityManager.clear();
        friendship.setStatus(ACCEPTED);
        friendshipRepository.saveAndFlush(friendship);
        entityManager.clear();
        Friendship result = entityManager.find(Friendship.class, friendship.getId());
        assertNotNull(result);
        assertEquals(ACCEPTED, result.getStatus());
    }

    @Test
    public void testDeleteById() {
        friendshipRepository.saveAndFlush(friendship);
        entityManager.clear();
        friendshipRepository.deleteById(friendship.getId());
        entityManager.flush();
        entityManager.clear();
        assertNull(entityManager.find(Friendship.class, friendship.getId()));
    }

    @Test
    public void testDeleteByIdMissing() {
        assertThrows(EmptyResultDataAccessException.class, () -> friendshipRepository.deleteById(new Id(2L, 3L)));
    }

}