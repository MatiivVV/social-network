package com.getjavajob.training.matiivv.socialnetwork.dao.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.ChatMessage;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.ChatMessageSummary;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

import static java.time.LocalDateTime.now;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.data.domain.PageRequest.of;

@DataJpaTest
public class ChatMessageRepositoryTest {

    @Autowired
    private ChatMessageRepository chatMessageRepository;
    @Autowired
    private TestEntityManager entityManager;
    private ChatMessage chatMessage;

    @BeforeEach
    public void setUp() {
        chatMessage = new ChatMessage();
        chatMessage.setAuthor(new Account(1L));
        chatMessage.setMessage("message");
        chatMessage.setImageId("image");
        chatMessage.setPostedAt(now());
        chatMessage.setRecipient(new Account(2L));
    }

    @Test
    public void testExistsById() {
        assertTrue(chatMessageRepository.existsById(1L));
    }

    @Test
    public void testExistsByIdMissing() {
        assertFalse(chatMessageRepository.existsById(99L));
    }

    @Test
    public void testFindById() {
        Optional<ChatMessage> result = chatMessageRepository.findById(2L);
        assertTrue(result.isPresent());
        assertEquals("message2", result.get().getMessage());
    }

    @Test
    public void testFindByIdMissing() {
        assertFalse(chatMessageRepository.findById(99L).isPresent());
    }

    @Test
    public void testFindByIdAndRecipientIdAndAuthorId() {
        Optional<ChatMessage> result = chatMessageRepository.findByIdAndRecipientIdAndAuthorId(2L, 2L, 1L);
        assertTrue(result.isPresent());
        assertEquals("message2", result.get().getMessage());
    }

    @Test
    public void testFindByIdAndRecipientIdAndAuthorIdMissing() {
        assertFalse(chatMessageRepository.findByIdAndRecipientIdAndAuthorId(2L, 1L, 1L).isPresent());
    }

    @Test
    public void testCountByRecipientIdAndReadFalse() {
        assertEquals(2L, chatMessageRepository.countByRecipientIdAndReadFalse(1L));
    }

    @Test
    public void testFindByAccountIdAndOpponentIdOnePage() {
        Pageable pageable = of(0, 100);
        Page<ChatMessageSummary> results = chatMessageRepository.findByAccountIdAndOpponentId(1L, 2L, pageable);
        assertNotNull(results);
        assertAll(
                () -> assertEquals(2L, results.getTotalElements()),
                () -> assertEquals(1, results.getTotalPages()),
                () -> assertEquals(2, results.getNumberOfElements()),
                () -> assertThat(results.stream().map(ChatMessageSummary::getId).collect(toList()), hasItems(2L, 3L))
        );
    }

    @Test
    public void testFindByAccountIdAndOpponentIdManyPages() {
        Pageable pageable = of(0, 1);
        Page<ChatMessageSummary> results = chatMessageRepository.findByAccountIdAndOpponentId(1L, 2L, pageable);
        assertNotNull(results);
        assertAll(
                () -> assertEquals(2L, results.getTotalElements()),
                () -> assertEquals(2, results.getTotalPages()),
                () -> assertEquals(1, results.getNumberOfElements()),
                () -> assertThat(results.stream().map(ChatMessageSummary::getId).collect(toList()), hasItems(3L))
        );
    }

    @Test
    public void testCreate() {
        chatMessageRepository.saveAndFlush(chatMessage);
        entityManager.clear();
        assertNotNull(entityManager.find(ChatMessage.class, chatMessage.getId()));
    }

    @Test
    public void testUpdate() {
        entityManager.persistAndFlush(chatMessage);
        entityManager.clear();
        chatMessage.setMessage("new message");
        chatMessageRepository.saveAndFlush(chatMessage);
        entityManager.clear();
        ChatMessage result = entityManager.find(ChatMessage.class, chatMessage.getId());
        assertNotNull(result);
        assertEquals("new message", result.getMessage());
    }

    @Test
    public void testSetReadByRecipientIdAndAuthorId() {
        assertAll(
                () -> assertEquals(1, chatMessageRepository.setReadByRecipientIdAndAuthorId(2L, 1L)),
                () -> assertEquals(0L, chatMessageRepository.countByRecipientIdAndReadFalse(2L))
        );
    }

    @Test
    public void testDeleteById() {
        entityManager.persistAndFlush(chatMessage);
        entityManager.clear();
        chatMessageRepository.deleteById(chatMessage.getId());
        entityManager.flush();
        entityManager.clear();
        assertNull(entityManager.find(ChatMessage.class, chatMessage.getId()));
    }

    @Test
    public void testDeleteByIdMissing() {
        assertThrows(EmptyResultDataAccessException.class, () -> chatMessageRepository.deleteById(99L));
    }

}