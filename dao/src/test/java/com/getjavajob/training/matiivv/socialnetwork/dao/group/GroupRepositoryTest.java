package com.getjavajob.training.matiivv.socialnetwork.dao.group;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Group;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.group.GroupSummary;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

import static java.time.LocalDateTime.now;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.data.domain.PageRequest.of;

@DataJpaTest
public class GroupRepositoryTest {

    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private TestEntityManager entityManager;
    private Group group;

    @BeforeEach
    public void setUp() {
        group = new Group();
        group.setName("group");
        group.setImageId("image");
        group.setDescription("description");
        group.setCreator(new Account(1L));
        group.setCreatedAt(now());
        group.setDeleted(true);
    }

    @Test
    public void testExistsById() {
        assertTrue(groupRepository.existsById(2L));
    }

    @Test
    public void testExistsByIdMissing() {
        assertFalse(groupRepository.existsById(99L));
    }

    @Test
    public void testExistsByName() {
        assertTrue(groupRepository.existsByName("group2"));
    }

    @Test
    public void testExistsByNameMissing() {
        assertFalse(groupRepository.existsByName("group99"));
    }

    @Test
    public void testExistsByNameAndIdNot() {
        assertTrue(groupRepository.existsByNameAndIdNot("group2", 1L));
    }

    @Test
    public void testExistsByNameAndIdNotMissing() {
        assertFalse(groupRepository.existsByNameAndIdNot("group2", 2L));
    }

    @Test
    public void testFindById() {
        Optional<Group> result = groupRepository.findById(2L);
        assertTrue(result.isPresent());
        assertEquals("group2", result.get().getName());
    }

    @Test
    public void testFindByIdMissing() {
        assertFalse(groupRepository.findById(99L).isPresent());
    }

    @Test
    public void testFindByNamePatternOnePage() {
        Pageable pageable = of(0, 100);
        Page<GroupSummary> results = groupRepository.findByNamePattern("group", pageable);
        assertNotNull(results);
        assertAll(
                () -> assertEquals(2L, results.getTotalElements()),
                () -> assertEquals(1, results.getTotalPages()),
                () -> assertEquals(2, results.getNumberOfElements()),
                () -> assertThat(results.stream().map(GroupSummary::getId).collect(toList()), hasItems(1L, 2L))
        );
    }

    @Test
    public void testFindByNamePatternManyPages() {
        Pageable pageable = of(0, 1);
        Page<GroupSummary> results = groupRepository.findByNamePattern("group", pageable);
        assertNotNull(results);
        assertAll(
                () -> assertEquals(2L, results.getTotalElements()),
                () -> assertEquals(2, results.getTotalPages()),
                () -> assertEquals(1, results.getNumberOfElements()),
                () -> assertThat(results.stream().map(GroupSummary::getId).collect(toList()), hasItems(1L))
        );
    }

    @Test
    public void testCreate() {
        groupRepository.saveAndFlush(group);
        entityManager.clear();
        assertNotNull(entityManager.find(Group.class, group.getId()));
    }

    @Test
    public void testCreateExisting() {
        assertThrows(DataIntegrityViolationException.class, () -> {
            groupRepository.saveAndFlush(group);
            entityManager.clear();
            group.setId(null);
            groupRepository.saveAndFlush(group);
        });
    }

    @Test
    public void testUpdate() {
        entityManager.persistAndFlush(group);
        entityManager.clear();
        group.setName("new name");
        groupRepository.saveAndFlush(group);
        entityManager.clear();
        Group result = entityManager.find(Group.class, group.getId());
        assertNotNull(result);
        assertEquals("new name", result.getName());
    }

    @Test
    public void testDeleteById() {
        entityManager.persistAndFlush(group);
        entityManager.clear();
        groupRepository.deleteById(group.getId());
        entityManager.flush();
        entityManager.clear();
        assertNull(entityManager.find(Group.class, group.getId()));
    }

    @Test
    public void testDeleteByIdMissing() {
        assertThrows(EmptyResultDataAccessException.class, () -> groupRepository.deleteById(99L));
    }

}