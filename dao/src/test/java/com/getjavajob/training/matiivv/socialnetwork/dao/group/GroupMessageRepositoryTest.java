package com.getjavajob.training.matiivv.socialnetwork.dao.group;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Group;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.GroupMessage;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.MessageSummary;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

import static java.time.LocalDateTime.now;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.data.domain.PageRequest.of;

@DataJpaTest
public class GroupMessageRepositoryTest {

    @Autowired
    private GroupMessageRepository groupMessageRepository;
    @Autowired
    private TestEntityManager entityManager;
    private GroupMessage groupMessage;

    @BeforeEach
    public void setUp() {
        groupMessage = new GroupMessage();
        groupMessage.setAuthor(new Account(1L));
        groupMessage.setMessage("message");
        groupMessage.setImageId("image");
        groupMessage.setPostedAt(now());
        groupMessage.setGroup(new Group(3L));
    }

    @Test
    public void testExistsById() {
        assertTrue(groupMessageRepository.existsById(1L));
    }

    @Test
    public void testExistsByIdMissing() {
        assertFalse(groupMessageRepository.existsById(99L));
    }

    @Test
    public void testFindById() {
        Optional<GroupMessage> result = groupMessageRepository.findById(2L);
        assertTrue(result.isPresent());
        assertEquals("message2", result.get().getMessage());
    }

    @Test
    public void testFindByIdMissing() {
        assertFalse(groupMessageRepository.findById(99L).isPresent());
    }

    @Test
    public void testFindByIdAndGroupIdAndAuthorId() {
        Optional<GroupMessage> result = groupMessageRepository.findByIdAndGroupIdAndAuthorId(2L, 2L, 1L);
        assertTrue(result.isPresent());
        assertEquals("message2", result.get().getMessage());
    }

    @Test
    public void testFindByIdAndGroupIdAndAuthorIdMissing() {
        assertFalse(groupMessageRepository.findByIdAndGroupIdAndAuthorId(2L, 1L, 1L).isPresent());
    }

    @Test
    public void testFindByGroupIdOnePage() {
        Pageable pageable = of(0, 100);
        Page<MessageSummary> results = groupMessageRepository.findByGroupId(1L, pageable);
        assertNotNull(results);
        assertAll(
                () -> assertEquals(2L, results.getTotalElements()),
                () -> assertEquals(1, results.getTotalPages()),
                () -> assertEquals(2, results.getNumberOfElements()),
                () -> assertThat(results.stream().map(MessageSummary::getId).collect(toList()), hasItems(1L, 3L))
        );
    }

    @Test
    public void testFindByGroupIdManyPages() {
        Pageable pageable = of(0, 1);
        Page<MessageSummary> results = groupMessageRepository.findByGroupId(1L, pageable);
        assertNotNull(results);
        assertAll(
                () -> assertEquals(2L, results.getTotalElements()),
                () -> assertEquals(2, results.getTotalPages()),
                () -> assertEquals(1, results.getNumberOfElements()),
                () -> assertThat(results.stream().map(MessageSummary::getId).collect(toList()), hasItems(3L))
        );
    }

    @Test
    public void testCreate() {
        groupMessageRepository.saveAndFlush(groupMessage);
        entityManager.clear();
        assertNotNull(entityManager.find(GroupMessage.class, groupMessage.getId()));
    }

    @Test
    public void testUpdate() {
        entityManager.persistAndFlush(groupMessage);
        entityManager.clear();
        groupMessage.setMessage("new message");
        groupMessageRepository.saveAndFlush(groupMessage);
        entityManager.clear();
        GroupMessage result = entityManager.find(GroupMessage.class, groupMessage.getId());
        assertNotNull(result);
        assertEquals("new message", result.getMessage());
    }

    @Test
    public void testDeleteById() {
        entityManager.persistAndFlush(groupMessage);
        entityManager.clear();
        groupMessageRepository.deleteById(groupMessage.getId());
        entityManager.flush();
        entityManager.clear();
        assertNull(entityManager.find(GroupMessage.class, groupMessage.getId()));
    }

    @Test
    public void testDeleteByIdMissing() {
        assertThrows(EmptyResultDataAccessException.class, () -> groupMessageRepository.deleteById(99L));
    }

}