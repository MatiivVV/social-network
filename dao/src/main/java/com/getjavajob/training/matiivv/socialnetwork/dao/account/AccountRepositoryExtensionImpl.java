package com.getjavajob.training.matiivv.socialnetwork.dao.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account_;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Address_;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.AccountSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaContext;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Pattern;

public class AccountRepositoryExtensionImpl implements AccountRepositoryExtension {

    private EntityManager entityManager;
    private CriteriaBuilder cb;

    @Autowired
    public void setJpaContext(JpaContext context) {
        entityManager = context.getEntityManagerByManagedType(Account.class);
        cb = entityManager.getCriteriaBuilder();
    }

    @Override
    public Page<AccountSummary> findByNamePattern(String pattern, Pageable pageRequest) {
        String[] tokens = Pattern.compile(" ")
                .splitAsStream(pattern)
                .map(s -> s + '%')
                .limit(2)
                .toArray(String[]::new);

        Function<Root<Account>, Predicate> predicate = root -> {
            Predicate result = cb.equal(root.get(Account_.deleted), false);
            if (tokens.length == 1) {
                Predicate predicate1 = cb.or(
                        cb.like(root.get(Account_.firstName), tokens[0] + '%'),
                        cb.like(root.get(Account_.lastName), tokens[0] + '%')
                );
                result = cb.and(result, predicate1);
            } else if (tokens.length == 2) {
                Predicate predicate1 = cb.and(
                        cb.like(root.get(Account_.firstName), tokens[0] + '%'),
                        cb.like(root.get(Account_.lastName), tokens[1] + '%')
                );
                Predicate predicate2 = cb.and(
                        cb.like(root.get(Account_.firstName), tokens[1] + '%'),
                        cb.like(root.get(Account_.lastName), tokens[0] + '%')
                );
                result = cb.and(result, cb.or(predicate1, predicate2));
            }
            return result;
        };

        CriteriaQuery<Long> countQuery = cb.createQuery(Long.class);
        Root<Account> countRoot = countQuery.from(Account.class);
        long total = entityManager
                .createQuery(countQuery
                        .select(cb.count(countRoot))
                        .where(predicate.apply(countRoot)))
                .getSingleResult();

        CriteriaQuery<AccountSummary> resultQuery = cb.createQuery(AccountSummary.class);
        Root<Account> account = resultQuery.from(Account.class);
        List<AccountSummary> result = entityManager
                .createQuery(resultQuery
                        .select(cb.construct(AccountSummary.class,
                                account.get(Account_.id),
                                account.get(Account_.firstName),
                                account.get(Account_.lastName),
                                account.get(Account_.imageId),
                                account.get(Account_.personalAddress).get(Address_.country),
                                account.get(Account_.personalAddress).get(Address_.city)))
                        .where(predicate.apply(account))
                        .orderBy(cb.asc(account.get(Account_.lastName)),
                                cb.asc(account.get(Account_.firstName))))
                .setFirstResult((int) pageRequest.getOffset())
                .setMaxResults(pageRequest.getPageSize())
                .getResultList();
        return new PageImpl<>(result, pageRequest, total);
    }

}
