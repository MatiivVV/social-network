package com.getjavajob.training.matiivv.socialnetwork.dao.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.FriendshipSummary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Set;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Friendship.Type;

public interface FriendshipRepositoryExtension {

    Page<FriendshipSummary> search(long accountId, Set<Type> types, Set<Status> statuses, Pageable pageRequest);

}
