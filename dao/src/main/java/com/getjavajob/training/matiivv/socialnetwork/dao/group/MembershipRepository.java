package com.getjavajob.training.matiivv.socialnetwork.dao.group;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Membership;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Membership.Id;

@Repository
public interface MembershipRepository extends JpaRepository<Membership, Id>, MembershipRepositoryExtension {
}
