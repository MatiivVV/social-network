package com.getjavajob.training.matiivv.socialnetwork.dao.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.*;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.FriendshipSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaContext;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

public class FriendshipRepositoryExtensionImpl implements FriendshipRepositoryExtension {

    private EntityManager entityManager;
    private CriteriaBuilder cb;

    @Autowired
    public void setJpaContext(JpaContext context) {
        entityManager = context.getEntityManagerByManagedType(Friendship.class);
        cb = entityManager.getCriteriaBuilder();
    }

    @Override
    public Page<FriendshipSummary> search(long accountId, Set<Friendship.Type> types, Set<Status> statuses,
                                          Pageable pageRequest) {
        Function<Root<Friendship>, Predicate> predicate = root -> {
            Predicate result = cb.equal(root.get(Friendship_.account).get(Account_.id), accountId);
            if (types != null && !types.isEmpty()) {
                result = cb.and(result, root.get(Friendship_.type).in(types));
            }
            if (statuses != null && !statuses.isEmpty()) {
                result = cb.and(result, root.get(Friendship_.status).in(statuses));
            }
            return result;
        };

        CriteriaQuery<Long> countQuery = cb.createQuery(Long.class);
        Root<Friendship> countRoot = countQuery.from(Friendship.class);
        long total = entityManager
                .createQuery(countQuery
                        .select(cb.count(countRoot))
                        .where(predicate.apply(countRoot)))
                .getSingleResult();

        CriteriaQuery<FriendshipSummary> resultQuery = cb.createQuery(FriendshipSummary.class);
        Root<Friendship> friendship = resultQuery.from(Friendship.class);
        Join<Friendship, Account> friend = friendship.join(Friendship_.friend);
        List<FriendshipSummary> result = entityManager
                .createQuery(resultQuery
                        .select(cb.construct(FriendshipSummary.class,
                                friend.get(Account_.id),
                                friend.get(Account_.firstName),
                                friend.get(Account_.lastName),
                                friend.get(Account_.imageId),
                                friend.get(Account_.personalAddress).get(Address_.country),
                                friend.get(Account_.personalAddress).get(Address_.city),
                                friend.get(Account_.deleted),
                                friendship.get(Friendship_.type),
                                friendship.get(Friendship_.status),
                                friendship.get(Friendship_.changedAt)))
                        .where(predicate.apply(friendship))
                        .orderBy(cb.desc(friendship.get(Friendship_.changedAt))))
                .setFirstResult((int) pageRequest.getOffset())
                .setMaxResults(pageRequest.getPageSize())
                .getResultList();
        return new PageImpl<>(result, pageRequest, total);
    }

}
