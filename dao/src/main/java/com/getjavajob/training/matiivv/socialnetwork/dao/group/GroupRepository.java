package com.getjavajob.training.matiivv.socialnetwork.dao.group;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Group;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.group.GroupSummary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {

    boolean existsByName(String name);

    @SuppressWarnings("SpringDataRepositoryMethodParametersInspection")
    boolean existsByNameAndIdNot(String name, long id);

    @SuppressWarnings("JpaQlInspection")
    @Query("SELECT NEW com.getjavajob.training.matiivv.socialnetwork.domain.projection.group.GroupSummary( " +
            "   g.id, " +
            "   g.name, " +
            "   g.imageId, " +
            "   g.description) " +
            "FROM Group g " +
            "WHERE g.name LIKE :pattern% " +
            "   AND g.deleted = FALSE " +
            "ORDER BY g.name ASC")
    Page<GroupSummary> findByNamePattern(@Param("pattern") String pattern, Pageable pageRequest);

}
