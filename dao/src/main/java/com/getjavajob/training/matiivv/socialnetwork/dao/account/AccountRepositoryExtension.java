package com.getjavajob.training.matiivv.socialnetwork.dao.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.AccountSummary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface AccountRepositoryExtension {

    Page<AccountSummary> findByNamePattern(String pattern, Pageable pageRequest);

}
