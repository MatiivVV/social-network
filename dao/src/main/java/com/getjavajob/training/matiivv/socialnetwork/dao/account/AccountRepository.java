package com.getjavajob.training.matiivv.socialnetwork.dao.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long>, AccountRepositoryExtension {

    boolean existsByEmailAddress(String emailAddress);

    @SuppressWarnings("SpringDataRepositoryMethodParametersInspection")
    boolean existsByEmailAddressAndIdNot(String emailAddress, long id);

    Optional<Account> findByEmailAddress(String email);

}
