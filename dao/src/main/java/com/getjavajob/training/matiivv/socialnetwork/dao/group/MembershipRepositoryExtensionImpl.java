package com.getjavajob.training.matiivv.socialnetwork.dao.group;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account_;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Address_;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Group;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Group_;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Membership;
import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.Membership_;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.group.AccountMembershipSummary;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.group.GroupMembershipSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaContext;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.Role;

public class MembershipRepositoryExtensionImpl implements MembershipRepositoryExtension {

    private EntityManager entityManager;
    private CriteriaBuilder cb;

    @Autowired
    public void setJpaContext(JpaContext context) {
        entityManager = context.getEntityManagerByManagedType(Membership.class);
        cb = entityManager.getCriteriaBuilder();
    }

    @Override
    public Page<GroupMembershipSummary> searchForGroup(long groupId, Set<Role> roles, Set<Status> statuses,
                                                       Pageable pageRequest) {
        Function<Root<Membership>, Predicate> predicate = root -> {
            Predicate result = cb.equal(root.get(Membership_.group).get(Group_.id), groupId);
            if (roles != null && !roles.isEmpty()) {
                result = cb.and(result, root.get(Membership_.role).in(roles));
            }
            if (statuses != null && !statuses.isEmpty()) {
                result = cb.and(result, root.get(Membership_.status).in(statuses));
            }
            return result;
        };

        CriteriaQuery<Long> countQuery = cb.createQuery(Long.class);
        Root<Membership> countRoot = countQuery.from(Membership.class);
        long total = entityManager
                .createQuery(countQuery
                        .select(cb.count(countRoot))
                        .where(predicate.apply(countRoot)))
                .getSingleResult();

        CriteriaQuery<GroupMembershipSummary> resultQuery = cb.createQuery(GroupMembershipSummary.class);
        Root<Membership> membership = resultQuery.from(Membership.class);
        Join<Membership, Account> member = membership.join(Membership_.member);
        List<GroupMembershipSummary> result = entityManager
                .createQuery(resultQuery
                        .select(cb.construct(GroupMembershipSummary.class,
                                member.get(Account_.id),
                                member.get(Account_.firstName),
                                member.get(Account_.lastName),
                                member.get(Account_.imageId),
                                member.get(Account_.personalAddress).get(Address_.country),
                                member.get(Account_.personalAddress).get(Address_.city),
                                member.get(Account_.deleted),
                                membership.get(Membership_.role),
                                membership.get(Membership_.status),
                                membership.get(Membership_.changedAt)))
                        .where(predicate.apply(membership))
                        .orderBy(cb.desc(membership.get(Membership_.changedAt))))
                .setFirstResult((int) pageRequest.getOffset())
                .setMaxResults(pageRequest.getPageSize())
                .getResultList();
        return new PageImpl<>(result, pageRequest, total);
    }

    @Override
    public Page<AccountMembershipSummary> searchForAccount(long accountId, Set<Role> roles, Set<Status> statuses,
                                                           Pageable pageRequest) {
        Function<Root<Membership>, Predicate> predicate = root -> {
            Predicate result = cb.equal(root.get(Membership_.member).get(Account_.id), accountId);
            if (roles != null && !roles.isEmpty()) {
                result = cb.and(result, root.get(Membership_.role).in(roles));
            }
            if (statuses != null && !statuses.isEmpty()) {
                result = cb.and(result, root.get(Membership_.status).in(statuses));
            }
            return result;
        };

        CriteriaQuery<Long> countQuery = cb.createQuery(Long.class);
        Root<Membership> countRoot = countQuery.from(Membership.class);
        long total = entityManager
                .createQuery(countQuery
                        .select(cb.count(countRoot))
                        .where(predicate.apply(countRoot)))
                .getSingleResult();

        CriteriaQuery<AccountMembershipSummary> resultQuery = cb.createQuery(AccountMembershipSummary.class);
        Root<Membership> membership = resultQuery.from(Membership.class);
        Join<Membership, Group> group = membership.join(Membership_.group);
        List<AccountMembershipSummary> result = entityManager
                .createQuery(resultQuery
                        .select(cb.construct(AccountMembershipSummary.class,
                                group.get(Group_.id),
                                group.get(Group_.name),
                                group.get(Group_.imageId),
                                group.get(Group_.description),
                                group.get(Group_.deleted),
                                membership.get(Membership_.role),
                                membership.get(Membership_.status),
                                membership.get(Membership_.changedAt)))
                        .where(predicate.apply(membership))
                        .orderBy(cb.desc(membership.get(Membership_.changedAt))))
                .setFirstResult((int) pageRequest.getOffset())
                .setMaxResults(pageRequest.getPageSize())
                .getResultList();
        return new PageImpl<>(result, pageRequest, total);
    }

}
