package com.getjavajob.training.matiivv.socialnetwork.dao.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Friendship;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Friendship.Id;

@Repository
public interface FriendshipRepository extends JpaRepository<Friendship, Id>, FriendshipRepositoryExtension {
}
