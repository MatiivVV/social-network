package com.getjavajob.training.matiivv.socialnetwork.dao.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Chat;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.ChatSummary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Chat.Id;

@Repository
public interface ChatRepository extends JpaRepository<Chat, Id> {

    @SuppressWarnings("SpringDataRepositoryMethodReturnTypeInspection")
    @Query(
            value = "SELECT NEW com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.ChatSummary( " +
                    "   o.id, " +
                    "   o.firstName, " +
                    "   o.lastName, " +
                    "   o.imageId, " +
                    "   o.deleted, " +
                    "   lastMessage.message, " +
                    "   COALESCE(MAX(m.postedAt), c.createdAt), " +
                    "   COALESCE(SUM(CASE m.read WHEN FALSE THEN 1 ELSE 0 END), 0)) " +
                    "FROM Chat c " +
                    "   LEFT JOIN c.opponent o " +
                    "       LEFT JOIN c.inMessages lastMessage " +
                    "       ON lastMessage.postedAt >= c.createdAt " +
                    "          LEFT JOIN c.inMessages m " +
                    "          ON m.postedAt >= c.createdAt " +
                    "WHERE c.account.id = :accountId " +
                    "   AND (lastMessage.id IN ( " +
                    "       SELECT MAX(lm.id) " +
                    "       FROM ChatMessage lm " +
                    "       WHERE lm.recipient.id = :accountId " +
                    "       GROUP BY lm.author.id) " +
                    "   OR lastMessage.id IS NULL) " +
                    "GROUP BY o.id, " +
                    "   o.firstName, " +
                    "   o.lastName, " +
                    "   o.imageId, " +
                    "   o.deleted, " +
                    "   lastMessage.message, " +
                    "   c.createdAt " +
                    "ORDER BY COALESCE(MAX(m.postedAt), c.createdAt) DESC",
            countQuery = "SELECT COUNT(c) " +
                    "FROM Chat c " +
                    "WHERE c.account.id = :accountId"
    )
    Page<ChatSummary> findByAccountId(@Param("accountId") long accountId, Pageable pageRequest);

}
