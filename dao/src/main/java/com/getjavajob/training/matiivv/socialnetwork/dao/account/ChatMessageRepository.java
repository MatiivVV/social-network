package com.getjavajob.training.matiivv.socialnetwork.dao.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.ChatMessage;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.ChatMessageSummary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ChatMessageRepository extends JpaRepository<ChatMessage, Long> {

    @SuppressWarnings("SpringDataRepositoryMethodParametersInspection")
    long countByRecipientIdAndReadFalse(long recipientId);

    @Query("SELECT m " +
            "FROM ChatMessage m " +
            "WHERE m.id = :id " +
            "   AND m.recipient.id = :recipientId " +
            "   AND m.author.id = :authorId")
    Optional<ChatMessage> findByIdAndRecipientIdAndAuthorId(@Param("id") long id,
                                                            @Param("recipientId") long recipientId,
                                                            @Param("authorId") long authorId);

    @SuppressWarnings({"JpaQlInspection", "SpringDataRepositoryMethodReturnTypeInspection"})
    @Query(
            value = "SELECT NEW com.getjavajob.training.matiivv.socialnetwork.domain.projection.account.ChatMessageSummary( " +
                    "   m.id, " +
                    "   a.id, " +
                    "   r.id, " +
                    "   m.message, " +
                    "   m.imageId, " +
                    "   m.postedAt, " +
                    "   m.read) " +
                    "FROM ChatMessage m " +
                    "   JOIN m.author a " +
                    "       JOIN m.recipient r " +
                    "WHERE ((r.id = :accountId AND a.id = :opponentId) " +
                    "   OR (a.id = :accountId AND r.id = :opponentId)) " +
                    "   AND m.postedAt > (SELECT c.createdAt" +
                    "       FROM Chat c" +
                    "       WHERE c.account.id = :accountId" +
                    "           AND c.opponent.id = :opponentId)" +
                    "ORDER BY m.postedAt DESC",
            countQuery = "SELECT COUNT(m) " +
                    "FROM ChatMessage m " +
                    "WHERE (m.recipient.id = :accountId AND m.author.id = :opponentId) " +
                    "   OR (m.author.id = :accountId AND m.recipient.id = :opponentId)" +
                    "   AND m.postedAt > (SELECT c.createdAt" +
                    "       FROM Chat c" +
                    "       WHERE c.account.id = :accountId" +
                    "           AND c.opponent.id = :opponentId)"
    )
    Page<ChatMessageSummary> findByAccountIdAndOpponentId(@Param("accountId") long accountId,
                                                          @Param("opponentId") long opponentId,
                                                          Pageable pageRequest);

    @Modifying
    @Query("UPDATE ChatMessage m " +
            "SET m.read = TRUE " +
            "WHERE m.recipient.id = :recipientId " +
            "   AND m.author.id = :authorId " +
            "   AND m.read = FALSE")
    int setReadByRecipientIdAndAuthorId(@Param("recipientId") long recipientId,
                                        @Param("authorId") long authorId);

}
