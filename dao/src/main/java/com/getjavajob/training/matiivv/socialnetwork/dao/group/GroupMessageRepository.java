package com.getjavajob.training.matiivv.socialnetwork.dao.group;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.group.GroupMessage;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.MessageSummary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GroupMessageRepository extends JpaRepository<GroupMessage, Long> {

    @Query("SELECT m " +
            "FROM GroupMessage m " +
            "WHERE m.id = :id " +
            "   AND m.group.id = :groupId " +
            "   AND m.author.id = :authorId")
    Optional<GroupMessage> findByIdAndGroupIdAndAuthorId(@Param("id") long id,
                                                         @Param("groupId") long accountId,
                                                         @Param("authorId") long authorId);

    @SuppressWarnings({"JpaQlInspection", "SpringDataRepositoryMethodReturnTypeInspection"})
    @Query(
            value = "SELECT NEW com.getjavajob.training.matiivv.socialnetwork.domain.projection.MessageSummary( " +
                    "   m.id, " +
                    "   a.id, " +
                    "   a.firstName, " +
                    "   a.lastName, " +
                    "   a.imageId, " +
                    "   a.deleted, " +
                    "   m.message, " +
                    "   m.imageId, " +
                    "   m.postedAt) " +
                    "FROM GroupMessage m " +
                    "   JOIN m.author a " +
                    "WHERE m.group.id = :groupId " +
                    "ORDER BY m.postedAt DESC",
            countQuery = "SELECT COUNT(m) " +
                    "FROM GroupMessage m " +
                    "WHERE m.group.id = :groupId"
    )
    Page<MessageSummary> findByGroupId(@Param("groupId") long groupId, Pageable pageRequest);

}
