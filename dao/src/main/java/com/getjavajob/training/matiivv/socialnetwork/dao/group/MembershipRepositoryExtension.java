package com.getjavajob.training.matiivv.socialnetwork.dao.group;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.Status;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.group.AccountMembershipSummary;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.group.GroupMembershipSummary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Set;

import static com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.Account.Role;

public interface MembershipRepositoryExtension {

    Page<GroupMembershipSummary> searchForGroup(long groupId, Set<Role> roles,
                                                Set<Status> statuses, Pageable pageRequest);

    Page<AccountMembershipSummary> searchForAccount(long accountId, Set<Role> roles,
                                                    Set<Status> statuses, Pageable pageRequest);

}
