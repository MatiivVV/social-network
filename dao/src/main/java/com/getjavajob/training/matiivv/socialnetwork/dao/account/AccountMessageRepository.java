package com.getjavajob.training.matiivv.socialnetwork.dao.account;

import com.getjavajob.training.matiivv.socialnetwork.domain.entity.account.AccountMessage;
import com.getjavajob.training.matiivv.socialnetwork.domain.projection.MessageSummary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountMessageRepository extends JpaRepository<AccountMessage, Long> {

    @Query("SELECT m " +
            "FROM AccountMessage m " +
            "WHERE m.id = :id " +
            "   AND m.account.id = :accountId " +
            "   AND m.author.id = :authorId")
    Optional<AccountMessage> findByIdAndAccountIdAndAuthorId(@Param("id") long id,
                                                             @Param("accountId") long accountId,
                                                             @Param("authorId") long authorId);

    @SuppressWarnings({"JpaQlInspection", "SpringDataRepositoryMethodReturnTypeInspection"})
    @Query(
            value = "SELECT NEW com.getjavajob.training.matiivv.socialnetwork.domain.projection.MessageSummary( " +
                    "   m.id, " +
                    "   a.id, " +
                    "   a.firstName, " +
                    "   a.lastName, " +
                    "   a.imageId, " +
                    "   a.deleted, " +
                    "   m.message, " +
                    "   m.imageId, " +
                    "   m.postedAt) " +
                    "FROM AccountMessage m " +
                    "   JOIN m.author a " +
                    "WHERE m.account.id = :accountId " +
                    "ORDER BY m.postedAt DESC",
            countQuery = "SELECT COUNT(m) " +
                    "FROM AccountMessage m " +
                    "WHERE m.account.id = :accountId"
    )
    Page<MessageSummary> findByAccountId(@Param("accountId") long accountId, Pageable pageRequest);

}
